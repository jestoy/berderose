#!/usr/bin/python
#
# MIT License
# Copyright (c) 2021 Jesus Vedasto Olazo <jestoy.olazo@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mb
from tkinter.scrolledtext import ScrolledText

import sys
import os
from datetime import datetime
import time
import json
import webbrowser
import calendar as cl
import math
import subprocess
import glob

from PIL import (Image, ImageTk)
from fpdf import FPDF

from sqlalchemy import (
                        create_engine, String, DateTime, Float,
                        Column, ForeignKey, Integer
                        )
from sqlalchemy.orm import (sessionmaker, relationship, joinedload)
from sqlalchemy.ext.declarative import declarative_base

__version__ = "1.0.0"
__author__ = "Jesus Vedasto Olazo"
__web__ = "http://jolazo.c1.biz/berderose/"
__email__ = "jestoy.olazo@gmail.com"
__license__ = "MIT"

Base = declarative_base()

class Client(Base):
    """This class provides basic client informations."""
    __tablename__ = "client"
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    address = Column(String(500))
    telephone = Column(String(100))
    fax = Column(String(100))
    email = Column(String(250))


class Status(Base):
    """This class creates the table for project status."""
    __tablename__ = "status"
    id = Column(Integer, primary_key=True)
    code = Column(String(50), nullable=False)
    description = Column(String(100), nullable=False)


class Currency(Base):
    """This class creates the table currency."""
    __tablename__ = "currency"
    id = Column(Integer, primary_key=True)
    code = Column(String(50), nullable=False)
    description = Column(String(100), nullable=False)


class Project(Base):
    """This provides basic details for the project."""
    __tablename__ = "project"
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    startdate = Column(DateTime)
    enddate = Column(DateTime)
    client_id = Column(Integer, ForeignKey('client.id'))
    client = relationship(Client)
    overhead = Column(Float)
    profit = Column(Float)
    status_id = Column(Integer, ForeignKey('status.id'))
    status = relationship(Status)
    currency_id = Column(Integer, ForeignKey('currency.id'))
    currency = relationship(Currency)


class Unit(Base):
    """Unit of measurements table."""
    __tablename__ = "unit"
    id = Column(Integer, primary_key=True)
    code = Column(String(50), unique=True)
    description = Column(String(250))


class Category(Base):
    """Main category of the estimation process."""
    __tablename__ = "category"
    id = Column(Integer, primary_key=True)
    code = Column(String(50), nullable=False)
    description = Column(String(250), nullable=False)


class SubCategory(Base):
    """Sub category class for storing sub category details."""
    __tablename__ = "subcategory"
    id = Column(Integer, primary_key=True)
    code = Column(String(50), nullable=False)
    description = Column(String(250), nullable=False)
    category_id = Column(Integer, ForeignKey('category.id'))
    category = relationship(Category)


class Material(Base):
    """The item master class for storing material related details."""
    __tablename__ = "material"
    id = Column(Integer, primary_key=True)
    itemcode = Column(String(200), nullable=False)
    description = Column(String(500), nullable=False)
    rate = Column(Float)
    unit_id = Column(Integer, ForeignKey('unit.id'))
    unit = relationship(Unit)


class Equipment(Base):
    """The equipment master class."""
    __tablename__ = "equipment"
    id = Column(Integer, primary_key=True)
    code = Column(String(200), nullable=False)
    description = Column(String(500), nullable=False)
    rate = Column(Float)
    unit_id = Column(Integer, ForeignKey('unit.id'))
    unit = relationship(Unit)


class Labor(Base):
    """This class will create the table for manpower"""
    __tablename__ = "labor"
    id = Column(Integer, primary_key=True)
    code = Column(String(200), nullable=False)
    description = Column(String(250))
    rate = Column(Float)
    unit_id = Column(Integer, ForeignKey('unit.id'))
    unit = relationship(Unit)


class SubContractor(Base):
    """This class is for the subcontractor details."""
    __tablename__ = "subcontractor"
    id = Column(Integer, primary_key=True)
    code = Column(String(100), nullable=False)
    name = Column(String(250), nullable=False)
    address = Column(String(500))
    telephone = Column(String(100))
    fax = Column(String(100))
    email = Column(String(250))


class MatTrans(Base):
    """Table for adding material used by the projects."""
    __tablename__ = "mattrans"
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    material_id = Column(Integer, ForeignKey('material.id'))
    material = relationship(Material)
    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship(Project)
    subcategory_id = Column(Integer, ForeignKey('subcategory.id'))
    subcategory = relationship(SubCategory)
    quantity = Column(Float)
    rate = Column(Float)


class LabTrans(Base):
    """This class will store any details pertaining to the manpower."""
    __tablename__ = 'labtrans'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    labor_id = Column(Integer, ForeignKey('labor.id'))
    labor = relationship(Labor)
    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship(Project)
    subcategory_id = Column(Integer, ForeignKey('subcategory.id'))
    subcategory = relationship(SubCategory)
    quantity = Column(Float)
    rate = Column(Float)


class EquiTrans(Base):
    """This class stores all the equipment sold or hired for the projects."""
    __tablename__ = 'equitrans'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    equipment_id = Column(Integer, ForeignKey('equipment.id'))
    equipment = relationship(Equipment)
    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship(Project)
    subcategory_id = Column(Integer, ForeignKey('subcategory.id'))
    subcategory = relationship(SubCategory)
    quantity = Column(Float)
    rate = Column(Float)


class SubConTrans(Base):
    """
    This class creates a table for any subcontractor
    cost of the project.
    """
    __tablename__ = 'subcontrans'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    description = Column(String)
    subcontractor_id = Column(Integer, ForeignKey('subcontractor.id'))
    subcontractor = relationship(SubContractor)
    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship(Project)
    subcategory_id = Column(Integer, ForeignKey('subcategory.id'))
    subcategory = relationship(SubCategory)
    unit_id = Column(Integer, ForeignKey('unit.id'))
    unit = relationship(Unit)
    quantity = Column(Float)
    rate = Column(Float)

ENGINE = create_engine('sqlite:///berderose.db', echo=False)

if not os.path.isfile('berderose.db'):
    Base.metadata.create_all(ENGINE)

Base.metadata.bind = ENGINE
MainSession = sessionmaker(bind=ENGINE)

def image_list(size=(16, 16)):
    """ This creates a dictionary of image file path for software icons. """
    img_lst = {}
    for img in glob.glob('images/*.png'):
        path_to_file = img
        img = os.path.basename(img)
        file_name = img.split('.')[0]
        img_lst[file_name] = ImageTk.PhotoImage(Image.open(path_to_file).resize(size))
    return img_lst

class MainWindow(ttk.Frame):
    """This is the main window of the application. Usually holds a summary
    of project details and it's cost.
    """

    def __init__(self, master=None, *args, **kwargs):
        """Initialize the graphics user interface of the program. Creates
        the connection to the database, in this instance an sqlite3 database
        but later on will be adding MySQL database support for system enhance
        ment
        """
        ttk.Frame.__init__(self, master, *args, **kwargs)
        #self.engine = create_engine("sqlite:///berderose.db", echo=False)
        title = " - ".join(["Berderose Estimation Software", __version__])
        self.master.title(title)
        self.master.protocol('WM_DELETE_WINDOW', self.close)
        self.img_list = image_list(size=(24, 24))
        self.style = ttk.Style()
        self.setup_ui()
        # This option checks the apporiate command to be used to be able
        # to maximize the window on start.
        # For Windows OS: self.master.state("zoomed") and
        # For Linux OS: self.master.attributes("-zoomed", True)
        if os.name == "nt":
            self.master.state("zoomed")
            self.master.iconbitmap("berderose.ico")
        elif os.name == "posix":
            self.master.attributes("-zoomed", True)
            self.master.tk.call('wm', 'iconphoto', self.master._w,
                                self.img_list['berderose'])
        else:
            pass

    def setup_ui(self):
        """Setup the rest of the graphical user interface."""
        self.master.event_add("<<CloseApp>>", "<Control-Q>", "<Control-q>")
        self.master.bind("<<CloseApp>>", self.closeEvent)

        menubar = tk.Menu(self)
        self.master.config(menu=menubar)

        self.sys_menu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="System", menu=self.sys_menu)

        self.proj_menu = tk.Menu(self.sys_menu, tearoff=0)
        self.sys_menu.add_cascade(label="Project", menu=self.proj_menu,
                                  image=self.img_list['project'], compound=tk.LEFT)

        self.proj_menu.add_command(label="New", command=self.newProjectWindow,
                                   accelerator="Ctrl+N", image=self.img_list['new-file'],
                                   compound=tk.LEFT)
        self.proj_menu.add_command(label="Edit",
                                   command=self.editProjectWindow,
                                   image=self.img_list['edit-file'], compound=tk.LEFT)
        self.proj_menu.add_command(label="Delete", command=self.deleteProject,
                                   image=self.img_list['delete-file'], compound=tk.LEFT)
        self.sys_menu.add_command(label="Client", command=self.clientWindow,
                                  image=self.img_list['client-management'], compound=tk.LEFT)
        self.sys_menu.add_separator()
        self.sys_menu.add_command(label="Category",
                                  command=self.categoryWindow,
                                  image=self.img_list['category'], compound=tk.LEFT)
        self.sys_menu.add_command(label="Sub-Category",
                                  command=self.subCategoryWindow,
                                  image=self.img_list['sub-category'],
                                  compound=tk.LEFT)
        self.sys_menu.add_command(label="Unit", command=self.unitWindow,
                                  image=self.img_list['measure'], compound=tk.LEFT)
        self.sys_menu.add_command(label="Currency",
                                  command=self.currencyWindow,
                                  image=self.img_list['currency'],
                                  compound=tk.LEFT)
        self.sys_menu.add_command(label="Status", command=self.statusWindow,
                                  image=self.img_list['status'], compound=tk.LEFT)
        self.sys_menu.add_separator()
        self.sys_menu.add_command(label="Material",
                                  command=self.materialWindow,
                                  image=self.img_list['material'],
                                  compound=tk.LEFT)
        self.sys_menu.add_command(label="Equipment",
                                  command=self.equipmentWindow,
                                  image=self.img_list['equipment'],
                                  compound=tk.LEFT)
        self.sys_menu.add_command(label="Labor", command=self.laborWindow,
                                  image=self.img_list['labors'], compound=tk.LEFT)
        self.sys_menu.add_command(label="Sub-Contractor",
                                  command=self.subContractorWindow,
                                  image=self.img_list['contractor'], compound=tk.LEFT)
        self.sys_menu.add_separator()
        self.sys_menu.add_command(label="Quit", command=self.close,
                                  accelerator="Ctrl+Q", image=self.img_list['quit'],
                                  compound=tk.LEFT, underline=0)

        self.trans_menu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="Transactions", menu=self.trans_menu)

        self.trans_menu.add_command(label="Material Transaction",
                                    command=self.materialTransactionWindow,
                                    image=self.img_list['mattrans'], compound=tk.LEFT)
        self.trans_menu.add_command(label="Equipment Transaction",
                                    command=self.equipmentTransactionWindow,
                                    image=self.img_list['equitrans'], compound=tk.LEFT)
        self.trans_menu.add_command(label="Labor Transaction",
                                    command=self.laborTransactionWindow,
                                    image=self.img_list['labtrans'],
                                    compound=tk.LEFT)
        self.trans_menu.add_command(label="Sub-Contractor Transaction",
                                    command=self.subConTransactionWindow,
                                    image=self.img_list['subcontrans'], compound=tk.LEFT)

        self.rept_menu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="Report", menu=self.rept_menu)

        self.rept_menu.add_command(label="Summary", command=self.summaryReport,
                                   compound=tk.LEFT, image=self.img_list['summary'])

        self.opts_menu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="Option", menu=self.opts_menu)

        self.opts_menu.add_command(label="Company",
                                   command=self.companyWindow,
                                   image=self.img_list['company'],
                                   compound=tk.LEFT)

        self.tools_menu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="Tools", menu=self.tools_menu)

        self.tools_menu.add_command(label="Calendar",
                                    command=self.calendarWindow,
                                    image=self.img_list['calendar'], compound=tk.LEFT)

        self.concrete_menu = tk.Menu(self.tools_menu, tearoff=0)
        self.tools_menu.add_cascade(label="Concrete", menu=self.concrete_menu,
                                    image=self.img_list['concrete'], compound=tk.LEFT)

        self.footing_menu = tk.Menu(self.concrete_menu, tearoff=0)
        self.column_menu = tk.Menu(self.concrete_menu, tearoff=0)
        self.wall_menu = tk.Menu(self.concrete_menu, tearoff=0)
        self.stair_menu = tk.Menu(self.concrete_menu, tearoff=0)
        self.slab_menu = tk.Menu(self.concrete_menu, tearoff=0)

        self.concrete_menu.add_cascade(label="Footing", menu=self.footing_menu)
        self.concrete_menu.add_cascade(label="Column", menu=self.column_menu)
        self.concrete_menu.add_cascade(label="Wall", menu=self.wall_menu)
        self.concrete_menu.add_cascade(label="Stairs", menu=self.stair_menu)
        self.concrete_menu.add_cascade(label="Slab", menu=self.slab_menu)

        self.footing_menu.add_command(label="Spread Footing",
                                       command=self.spreadFootingWindow)
        self.footing_menu.add_command(label="Countinous Footing",
                                       command=self.conFootingWindow)
        self.column_menu.add_command(label="Rectangular Column",
                                       command=self.recColumnWindow)
        self.column_menu.add_command(label="Round Column",
                                       command=self.roundColumnWindow)

        self.wall_menu.add_command(label="Foundation Wall",
                                       command=self.foundationWallWindow)

        self.stair_menu.add_command(label="Stairs",
                                       command=self.stairsWindow)

        self.slab_menu.add_command(label="Slab on Grade with Rebar",
                                       command=self.slabOnGradeWindow)
        self.slab_menu.add_command(label="Reinforced Slab on Grade",
                                       command=self.reSlabOnGradeWindow)
        self.slab_menu.add_command(label="Slab on Metal Deck",
                                       command=self.slabOnMetalWindow)

        self.help_menu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="Help", menu=self.help_menu)

        self.help_menu.add_command(label="Help", command=self.viewHelp,
                                   image=self.img_list['help'], compound=tk.LEFT)
        self.help_menu.add_separator()
        self.help_menu.add_command(label="About", image=self.img_list['about'],
                                   compound=tk.LEFT, command=self.aboutWindow)

        # Start of Tool Box

        self.tool_box = ttk.Frame(self)
        self.tool_box.pack(fill=tk.X, padx=5, pady=5)

        self.new_btn = ttk.Button(self.tool_box, image=self.img_list['new-file'])
        self.new_btn.pack(side=tk.LEFT)
        self.new_btn.config(command=self.newProjectWindow)
        CreateToolTip(self.new_btn, "Create new project.")

        self.edit_btn = ttk.Button(self.tool_box, image=self.img_list['edit-file'])
        self.edit_btn.pack(side=tk.LEFT)
        self.edit_btn.config(command=self.editProjectWindow)
        CreateToolTip(self.edit_btn, "Edit selected project.")

        self.delete_btn = ttk.Button(self.tool_box, image=self.img_list['delete-file'])
        self.delete_btn.pack(side=tk.LEFT)
        self.delete_btn.config(command=self.deleteProject)
        CreateToolTip(self.delete_btn, "Delete selected project.")

        group_sep1 = ttk.Separator(self.tool_box, orient=tk.VERTICAL)
        group_sep1.pack(side=tk.LEFT, fill=tk.Y, padx=4, pady=4)

        self.mattrans_btn = ttk.Button(self.tool_box, image=self.img_list['mattrans'])
        self.mattrans_btn.pack(side=tk.LEFT)
        self.mattrans_btn.config(command=self.materialTransactionWindow)
        CreateToolTip(self.mattrans_btn, "Material Transaction")

        self.equitrans_btn = ttk.Button(self.tool_box, image=self.img_list['equitrans'])
        self.equitrans_btn.pack(side=tk.LEFT)
        self.equitrans_btn.config(command=self.equipmentTransactionWindow)
        CreateToolTip(self.equitrans_btn, "Equipment Transaction")

        self.labtrans_btn = ttk.Button(self.tool_box, image=self.img_list['labtrans'])
        self.labtrans_btn.pack(side=tk.LEFT)
        self.labtrans_btn.config(command=self.laborTransactionWindow)
        CreateToolTip(self.labtrans_btn, "Labor Transaction")

        self.subcontrans_btn = ttk.Button(self.tool_box,
                                        image=self.img_list['subcontrans'])
        self.subcontrans_btn.pack(side=tk.LEFT)
        self.subcontrans_btn.config(command=self.subConTransactionWindow)
        CreateToolTip(self.subcontrans_btn, "Sub-Contractor Transaction")

        group_sep2 = ttk.Separator(self.tool_box, orient=tk.VERTICAL)
        group_sep2.pack(side=tk.LEFT, fill=tk.Y, padx=4, pady=4)

        self.cat_btn = ttk.Button(self.tool_box, image=self.img_list['category'])
        self.cat_btn.pack(side=tk.LEFT)
        self.cat_btn.config(command=self.categoryWindow)
        CreateToolTip(self.cat_btn, "Category Master")

        self.subcat_btn = ttk.Button(self.tool_box, image=self.img_list['sub-category'])
        self.subcat_btn.pack(side=tk.LEFT)
        self.subcat_btn.config(command=self.subCategoryWindow)
        CreateToolTip(self.subcat_btn, "Sub-Category Master")

        self.unit_btn = ttk.Button(self.tool_box, image=self.img_list['measure'])
        self.unit_btn.pack(side=tk.LEFT)
        self.unit_btn.config(command=self.unitWindow)
        CreateToolTip(self.unit_btn, "Unit Master")

        self.cur_btn = ttk.Button(self.tool_box, image=self.img_list['currency'])
        self.cur_btn.pack(side=tk.LEFT)
        self.cur_btn.config(command=self.currencyWindow)
        CreateToolTip(self.cur_btn, "Currency Master")

        self.stat_btn = ttk.Button(self.tool_box, image=self.img_list['status'])
        self.stat_btn.pack(side=tk.LEFT)
        self.stat_btn.config(command=self.statusWindow)
        CreateToolTip(self.stat_btn, "Status Master")

        group_sep3 = ttk.Separator(self.tool_box, orient=tk.VERTICAL)
        group_sep3.pack(side=tk.LEFT, fill=tk.Y, padx=4, pady=4)

        self.mat_btn = ttk.Button(self.tool_box, image=self.img_list['material'])
        self.mat_btn.pack(side=tk.LEFT)
        self.mat_btn.config(command=self.materialWindow)
        CreateToolTip(self.mat_btn, "Material Master")

        self.equi_btn = ttk.Button(self.tool_box, image=self.img_list['equipment'])
        self.equi_btn.pack(side=tk.LEFT)
        self.equi_btn.config(command=self.equipmentWindow)
        CreateToolTip(self.equi_btn, "Equipment Master")

        self.lab_btn = ttk.Button(self.tool_box, image=self.img_list['labors'])
        self.lab_btn.pack(side=tk.LEFT)
        self.lab_btn.config(command=self.laborWindow)
        CreateToolTip(self.lab_btn, "Labor Master")

        self.subcon_btn = ttk.Button(self.tool_box, image=self.img_list['contractor'])
        self.subcon_btn.pack(side=tk.LEFT)
        self.subcon_btn.config(command=self.subContractorWindow)
        CreateToolTip(self.subcon_btn, "Sub-Contractor Master")

        group_sep4 = ttk.Separator(self.tool_box, orient=tk.VERTICAL)
        group_sep4.pack(side=tk.LEFT, fill=tk.Y, padx=4, pady=4)

        self.summary_btn = ttk.Button(self.tool_box, image=self.img_list['summary'])
        self.summary_btn.pack(side=tk.LEFT)
        self.summary_btn.config(command=self.summaryReport)
        CreateToolTip(self.summary_btn, "View summary of the project.")

        # End of Tool Box

        self.container = ttk.PanedWindow(self, orient=tk.VERTICAL)
        self.container.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)

        self.top_frame = tk.Frame(self)
        self.bottom_frame = ttk.PanedWindow(self, orient=tk.HORIZONTAL)

        self.container.add(self.top_frame, weight=1)
        self.container.add(self.bottom_frame, weight=1)

        self.prj_tree = ttk.Treeview(self.top_frame)
        self.prj_tree.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)
        self.prj_tree.bind("<<TreeviewSelect>>", self.showDetails)
        self.prj_tree.bind("<Double-Button-1>", self.editProjectWindowEvent)

        proj_column = ("name", "client",
                       "status", "start",
                       "end", "overhead",
                       "profit")

        self.prj_tree['columns'] = proj_column

        self.prj_tree.heading("#0", text="Id")

        for col_name in proj_column:
            self.prj_tree.heading(col_name, text=col_name.title())

        self.prj_tree.column("#0", width=50, stretch=False)
        self.prj_tree.column("status", width=75)
        self.prj_tree.column("start", width=75)
        self.prj_tree.column("end", width=75)
        self.prj_tree.column("overhead", width=75)
        self.prj_tree.column("profit", width=75)

        self.prj_tree_sc = ttk.Scrollbar(self.top_frame,
                                         command=self.prj_tree.yview,
                                         orient=tk.VERTICAL)
        self.prj_tree_sc.pack(fill=tk.BOTH, side=tk.RIGHT)
        self.prj_tree['yscrollcommand'] = self.prj_tree_sc.set

        self.prj_tree.tag_configure("even", background="#ffdbca")
        self.prj_tree.tag_configure("odd", background="#ffb591")

        self.cat_list = tk.Listbox(self.bottom_frame)
        self.cat_list.config(activestyle='dotbox')
        self.cat_list.bind("<Double-Button-1>", self.showSpecificDetails)
        self.det_tree = ttk.Treeview(self.bottom_frame)

        self.det_tree.tag_configure("odd", background="#ffdbca")
        self.det_tree.tag_configure("even", background="#ffb591")

        det_column = ("subcategory", "material", "labor",
                      "equipment", "subcontractor", "total")
        self.det_tree['columns'] = det_column

        self.det_tree.heading("#0", text="Serial")

        for col_name in det_column:
            self.det_tree.heading(col_name, text=col_name.title())
            if col_name != "subcategory":
                self.det_tree.column(col_name, width=72)

        self.det_tree.column("#0", width=50, stretch=False)

        self.sum_frame = tk.LabelFrame(self.bottom_frame,
                                       relief=tk.GROOVE,
                                       text="Project Summary")

        subtotal_lbl = tk.Label(self.sum_frame, text="Sub-Total:", anchor=tk.W,
                                font=("Times", 15, "bold italic"))
        subtotal_lbl.grid(row=0, column=0, sticky="nesw")
        overhead_lbl = tk.Label(self.sum_frame, text="Overhead:", anchor=tk.W,
                                font=("Times", 15, "bold italic"))
        overhead_lbl.grid(row=1, column=0, sticky="nesw")
        profit_lbl = tk.Label(self.sum_frame, text="Profit:", anchor=tk.W,
                              font=("Times", 15, "bold italic"))
        profit_lbl.grid(row=2, column=0, sticky="nesw")
        total_lbl = tk.Label(self.sum_frame, text="Total Cost:", anchor=tk.W,
                             font=("Times", 15, "bold italic"))
        total_lbl.grid(row=3, column=0, sticky="nesw")

        self.subtotal_lbl = tk.Label(self.sum_frame, text="0.00",
                                     fg="red", font=("Times", 15, "bold"),
                                     relief=tk.RIDGE, bg='white', width=13,
                                     anchor=tk.E)
        self.subtotal_lbl.grid(row=0, column=1, sticky="nesw")
        self.overhead_lbl = tk.Label(self.sum_frame, text="0.00",
                                     fg="red", font=("Times", 15, "bold"),
                                     relief=tk.RIDGE, bg='white', width=13,
                                     anchor=tk.E)
        self.overhead_lbl.grid(row=1, column=1, sticky="nesw")
        self.profit_lbl = tk.Label(self.sum_frame, text="0.00",
                                   fg="red", font=("Times", 15, "bold"),
                                   relief=tk.RIDGE, bg='white', width=13,
                                   anchor=tk.E)
        self.profit_lbl.grid(row=2, column=1, sticky="nesw")
        self.total_lbl = tk.Label(self.sum_frame, text="0.00",
                                  fg="red", font=("Times", 15, "bold"),
                                  relief=tk.RIDGE, bg='white', width=13,
                                  anchor=tk.E)
        self.total_lbl.grid(row=3, column=1, sticky="nesw")

        self.bottom_frame.add(self.cat_list)
        self.bottom_frame.add(self.det_tree, weight=2)
        self.bottom_frame.add(self.sum_frame, weight=1)

        session = MainSession()
        cat_recs = session.query(Category).all()
        cat_lst = ['All']
        for cat_rec in cat_recs:
            cat_lst.append(cat_rec.description)
        self.cat_list.insert(tk.END, *cat_lst)
        self.cat_list.selection_set('0')
        self.cat_list.activate('0')

        subcat_recs = session.query(SubCategory).all()
        counter = 0
        for subcat_rec in subcat_recs:
            if (counter % 2) == 0:
                self.det_tree.insert('', 'end', str(subcat_rec.id),
                                     text=str(subcat_rec.id), tags='even')
            else:
                self.det_tree.insert('', 'end', str(subcat_rec.id),
                                     text=str(subcat_rec.id), tags='odd')
            self.det_tree.set(str(subcat_rec.id), 'subcategory',
                              subcat_rec.description)
            counter += 1

        session.close()

        self.updateProjectView()

    def summaryReport(self):
        """ Generate summary of the selected project in pdf format. """
        session = MainSession()
        prj_id = self.prj_tree.focus()
        if prj_id != '':
            proj_rec = session.query(Project).options(joinedload(Project.client), joinedload(Project.currency)).filter(Project.id == int(prj_id)).one()
            pdf = PDFReport('P', 'mm', 'A4', title="Summary")
            pdf.set_compression(compress=False)
            pdf.set_display_mode('fullwidth')
            pdf.alias_nb_pages()
            pdf.add_page()
            pdf.set_font('Times', '', 9)

            pdf.cell(15, 5, "Project:", 0, 0, 'L')
            pdf.set_font('Times', 'B', 9)
            pdf.cell(48, 5, proj_rec.name, 0, 1, 'L')
            pdf.set_font('Times', '', 9)
            pdf.cell(15, 5, "Client:", 0, 0, 'L')
            pdf.set_font('Times', 'B', 9)
            pdf.cell(48, 5, proj_rec.client.name, 0, 0, 'L')
            pdf.ln(10)

            pdf.set_font('Times', '', 9)
            pdf.cell(15, 5, "Code", 1, 0, 'C')
            pdf.cell(48, 5, "Description", 1, 0, 'C')
            pdf.cell(26, 5, "Materials", 1, 0, 'C')
            pdf.cell(26, 5, "Labor", 1, 0, 'C')
            pdf.cell(26, 5, "Equipment", 1, 0, 'C')
            pdf.cell(26, 5, "SubContractor", 1, 0, 'C')
            pdf.cell(26, 5, "Total", 1, 1, 'C')

            cat_recs = session.query(Category).all()
            subcat_recs = session.query(SubCategory).options(joinedload(SubCategory.category)).all()
            mat_trans_recs = session.query(MatTrans).filter(MatTrans.project_id == int(prj_id)).all()
            lab_trans_recs = session.query(LabTrans).filter(LabTrans.project_id == int(prj_id)).all()
            equip_trans_recs = session.query(EquiTrans).filter(EquiTrans.project_id == int(prj_id)).all()
            subcon_trans_recs = session.query(SubConTrans).filter(SubConTrans.project_id == int(prj_id)).all()

            materials = {}
            equipments = {}
            labors = {}
            subcons = {}

            total = 0
            for row in mat_trans_recs:
                for record in subcat_recs:
                    if row.subcategory_id == record.id:
                        total = row.rate * row.quantity
                        if str(record.id) in materials.keys():
                            materials[str(record.id)] += total
                        else:
                            materials[str(record.id)] = total

            total = 0
            for row in equip_trans_recs:
                for record in subcat_recs:
                    if row.subcategory_id == record.id:
                        total = row.rate * row.quantity
                        if str(record.id) in equipments.keys():
                            equipments[str(record.id)] += total
                        else:
                            equipments[str(record.id)] = total

            total = 0
            for row in lab_trans_recs:
                for record in subcat_recs:
                    if row.subcategory_id == record.id:
                        total = row.rate * row.quantity
                        if str(record.id) in labors.keys():
                            labors[str(record.id)] += total
                        else:
                            labors[str(record.id)] = total

            total = 0
            for row in subcon_trans_recs:
                for record in subcat_recs:
                    if row.subcategory_id == record.id:
                        total = row.rate * row.quantity
                        if str(record.id) in subcons.keys():
                            subcons[str(record.id)] += total
                        else:
                            subcons[str(record.id)] = total

            mat_total = 0
            labor_total = 0
            equip_total = 0
            subcon_total = 0

            for cat_rec in cat_recs:
                pdf.set_font('Times', 'B', 8)
                pdf.cell(15, 5, cat_rec.code, 1, 0)
                pdf.cell(48, 5, cat_rec.description.upper(), 1, 0)
                pdf.cell(26, 5, '', 1, 0)
                pdf.cell(26, 5, '', 1, 0)
                pdf.cell(26, 5, '', 1, 0)
                pdf.cell(26, 5, '', 1, 0)
                pdf.cell(26, 5, '', 1, 1)
                pdf.set_font('Times', '', 8)

                for subcat_rec in subcat_recs:
                    total = 0
                    if subcat_rec.category_id == cat_rec.id:
                        pdf.cell(15, 5, subcat_rec.code, 1, 0)
                        pdf.cell(48, 5, subcat_rec.description, 1, 0)

                        if str(subcat_rec.id) in materials:
                            pdf.cell(26, 5, "{:0,.2f}".format(materials[str(subcat_rec.id)]), 1, 0, 'R')
                            total += materials[str(subcat_rec.id)]
                            mat_total += materials[str(subcat_rec.id)]
                        else:
                            pdf.cell(26, 5, '', 1, 0)

                        if str(subcat_rec.id) in labors:
                            pdf.cell(26, 5, "{:0,.2f}".format(labors[str(subcat_rec.id)]), 1, 0, 'R')
                            total += labors[str(subcat_rec.id)]
                            labor_total += labors[str(subcat_rec.id)]
                        else:
                            pdf.cell(26, 5, '', 1, 0)

                        if str(subcat_rec.id) in equipments:
                            pdf.cell(26, 5, "{:0,.2f}".format(equipments[str(subcat_rec.id)]), 1, 0, 'R')
                            total += equipments[str(subcat_rec.id)]
                            equip_total += equipments[str(subcat_rec.id)]
                        else:
                            pdf.cell(26, 5, '', 1, 0)

                        if str(subcat_rec.id) in subcons:
                            pdf.cell(26, 5, "{:0,.2f}".format(subcons[str(subcat_rec.id)]), 1, 0, 'R')
                            total += subcons[str(subcat_rec.id)]
                            subcon_total += subcons[str(subcat_rec.id)]
                        else:
                            pdf.cell(26, 5, '', 1, 0)

                        if total > 0:
                            pdf.cell(26, 5, "{:0,.2f}".format(total), 1, 1, 'R')
                        else:
                            pdf.cell(26, 5, '', 1, 1)

                pdf.cell(15, 1, '', 1, 0)
                pdf.cell(48, 1, '', 1, 0)
                pdf.cell(26, 1, '', 1, 0)
                pdf.cell(26, 1, '', 1, 0)
                pdf.cell(26, 1, '', 1, 0)
                pdf.cell(26, 1, '', 1, 0)
                pdf.cell(26, 1, '', 1, 1)

            sub_total = mat_total + labor_total + equip_total + subcon_total

            pdf.set_font('Times', 'B', 8)
            pdf.cell(15, 5, '', 1, 0)
            pdf.cell(48, 5, "SUBTOTAL", 1, 0)
            pdf.cell(26, 5, "{:0,.2f}".format(mat_total), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(labor_total), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(equip_total), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(subcon_total), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(sub_total), 1, 1, 'R')

            pdf.set_font('Times', '', 8)
            pdf.cell(15, 5, '', 1, 0)
            pdf.cell(48, 5, "Overhead", 1, 0)
            pdf.cell(26, 5, "{:0,.2f}".format(mat_total * (proj_rec.overhead/100)), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(labor_total * (proj_rec.overhead/100)), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(equip_total * (proj_rec.overhead/100)), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(subcon_total * (proj_rec.overhead/100)), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(sub_total * (proj_rec.overhead/100)), 1, 1, 'R')

            pdf.cell(15, 5, '', 1, 0)
            pdf.cell(48, 5, "Profit", 1, 0)
            pdf.cell(26, 5, "{:0,.2f}".format(mat_total * (proj_rec.profit/100)), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(labor_total * (proj_rec.profit/100)), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(equip_total * (proj_rec.profit/100)), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(subcon_total * (proj_rec.profit/100)), 1, 0, 'R')
            pdf.cell(26, 5, "{:0,.2f}".format(sub_total * (proj_rec.profit/100)), 1, 1, 'R')

            mat_total = mat_total + (mat_total * (proj_rec.overhead/100)) + (mat_total * (proj_rec.profit/100))
            labor_total = labor_total + (labor_total * (proj_rec.overhead/100)) + (labor_total * (proj_rec.profit/100))
            equip_total = equip_total + (equip_total * (proj_rec.overhead/100)) + (equip_total * (proj_rec.profit/100))
            subcon_total = subcon_total + (subcon_total * (proj_rec.overhead/100)) + (subcon_total * (proj_rec.profit/100))
            sub_total = mat_total + labor_total + equip_total + subcon_total

            pdf.set_font('Times', 'B', 8)
            pdf.cell(15, 5, '', 1, 0)
            pdf.cell(48, 5, "TOTAL", 1, 0)
            pdf.cell(26, 5, "{} {:0,.2f}".format(proj_rec.currency.code, mat_total), 1, 0, 'R')
            pdf.cell(26, 5, "{} {:0,.2f}".format(proj_rec.currency.code, labor_total), 1, 0, 'R')
            pdf.cell(26, 5, "{} {:0,.2f}".format(proj_rec.currency.code, equip_total), 1, 0, 'R')
            pdf.cell(26, 5, "{} {:0,.2f}".format(proj_rec.currency.code, subcon_total), 1, 0, 'R')
            pdf.cell(26, 5, "{} {:0,.2f}".format(proj_rec.currency.code, sub_total), 1, 1, 'R')

            report_path = os.path.join(os.getcwd(), "reports", "summary.pdf")
            pdf.output(report_path, 'F')
            if os.path.isfile(report_path):
                CREATE_NO_WINDOW = 0x08000000 # This will hide the cmd window on report launch in windows.
                if os.name == 'nt':
                    subprocess.call(["start", report_path], creationflags=CREATE_NO_WINDOW, shell=True)
                else:
                    # This will activate in Linux but not sure with Mac OS.
                    os.system("xdg-open %s"% (report_path,))
        else:
            message = 'Unable to show project summary.\nPlease select the project and try again.'
            mb.showwarning('No Record', message, parent=self)

        session.close()

    def showSpecificDetails(self, event):
        """ Update the details of the selected project. """
        item_selected = int(event.widget.curselection()[0])
        prj_id = self.prj_tree.focus()
        if prj_id == '':
            return
        if item_selected == 0:
            self.showAllDetails()
        else:
            session = MainSession()
            cat_rec = session.query(Category).filter(
                Category.description == event.widget.get(item_selected)).one()
            prj_id = int(prj_id)
            category_id = cat_rec.id
            subcat_rec = session.query(SubCategory).filter(
                SubCategory.category_id == category_id).all()
            mat_tran_rec = session.query(MatTrans).filter(
                MatTrans.project_id == prj_id).all()
            equip_tran_rec = session.query(EquiTrans).filter(
                EquiTrans.project_id == prj_id).all()
            labor_tran_rec = session.query(LabTrans).filter(
                LabTrans.project_id == prj_id).all()
            subcon_tran_rec = session.query(SubConTrans).filter(
                SubConTrans.project_id == prj_id).all()

            materials = {}
            equipments = {}
            labors = {}
            subcons = {}

            for row in mat_tran_rec:
                for record in subcat_rec:
                    if row.subcategory_id == record.id:
                        total = row.rate * row.quantity
                        if str(record.id) in materials.keys():
                            materials[str(record.id)] += total
                        else:
                            materials[str(record.id)] = total

            for row in equip_tran_rec:
                for record in subcat_rec:
                    if row.subcategory_id == record.id:
                        total = row.rate * row.quantity
                        if str(record.id) in equipments.keys():
                            equipments[str(record.id)] += total
                        else:
                            equipments[str(record.id)] = total

            for row in labor_tran_rec:
                for record in subcat_rec:
                    if row.subcategory_id == record.id:
                        total = row.rate * row.quantity
                        if str(record.id) in labors.keys():
                            labors[str(record.id)] += total
                        else:
                            labors[str(record.id)] = total

            for row in subcon_tran_rec:
                for record in subcat_rec:
                    if row.subcategory_id == record.id:
                        total = row.rate * row.quantity
                        if str(record.id) in subcons.keys():
                            subcons[str(record.id)] += total
                        else:
                            subcons[str(record.id)] = total

            children = self.det_tree.get_children()
            if len(children) != 0:
                for child in children:
                    self.det_tree.delete(child)

            counter = 0
            for sc_rec in subcat_rec:
                if (counter % 2) == 0:
                    self.det_tree.insert('', 'end', str(sc_rec.id),
                                         text=str(sc_rec.id), tags='even')
                else:
                    self.det_tree.insert('', 'end', str(sc_rec.id),
                                         text=str(sc_rec.id), tags='odd')
                self.det_tree.set(str(sc_rec.id), 'subcategory',
                                  sc_rec.description)
                counter += 1

            for k, v in materials.items():
                self.det_tree.set(k, 'material', "{:0,.2f}".format(v))

            for k, v in equipments.items():
                self.det_tree.set(k, 'equipment', "{:0,.2f}".format(v))

            for k, v in labors.items():
                self.det_tree.set(k, 'labor', "{:0,.2f}".format(v))

            for k, v in subcons.items():
                self.det_tree.set(k, 'subcontractor', "{:0,.2f}".format(v))

            children = self.det_tree.get_children()
            subtotal = 0
            for child in children:
                total = 0
                if child in materials.keys():
                    total += materials[child]
                if child in equipments.keys():
                    total += equipments[child]
                if child in labors.keys():
                    total += labors[child]
                if child in subcons.keys():
                    total += subcons[child]

                if total != 0:
                    self.det_tree.set(child, 'total', "{:0,.2f}".format(total))
                    subtotal += total

            project = session.query(Project).options(joinedload(Project.currency)).filter(Project.id == prj_id).one()
            self.subtotal_lbl.config(text="{} {:0,.2f}".format(project.currency.code, subtotal))
            overhead = (project.overhead/100) * subtotal
            profit = (project.profit/100) * subtotal
            total_all = subtotal + overhead + profit

            self.overhead_lbl.config(text="{} {:0,.2f}".format(project.currency.code, overhead))
            self.profit_lbl.config(text="{} {:0,.2f}".format(project.currency.code, profit))
            self.total_lbl.config(text="{} {:0,.2f}".format(project.currency.code, total_all))
            session.close()

    def showDetails(self, event):
        """
        This method is event type method where it will activate only if
        you select a project so that all the details will be shown.
        """
        self.showAllDetails()

    def showAllDetails(self):
        """
        This method shows all the details pertaining to the currently
        selected project.
        """
        session = MainSession()
        prj_id = int(self.prj_tree.focus())
        self.cat_list.selection_set('0')
        self.cat_list.activate('0')

        subcat_rec = session.query(SubCategory).all()
        mat_tran_rec = session.query(MatTrans).filter(
            MatTrans.project_id == prj_id).all()
        equip_tran_rec = session.query(EquiTrans).filter(
            EquiTrans.project_id == prj_id).all()
        labor_tran_rec = session.query(LabTrans).filter(
            LabTrans.project_id == prj_id).all()
        subcon_tran_rec = session.query(SubConTrans).filter(
            SubConTrans.project_id == prj_id).all()

        materials = {}
        equipments = {}
        labors = {}
        subcons = {}

        for row in mat_tran_rec:
            for record in subcat_rec:
                if row.subcategory_id == record.id:
                    total = row.rate * row.quantity
                    if str(record.id) in materials.keys():
                        materials[str(record.id)] += total
                    else:
                        materials[str(record.id)] = total

        for row in equip_tran_rec:
            for record in subcat_rec:
                if row.subcategory_id == record.id:
                    total = row.rate * row.quantity
                    if str(record.id) in equipments.keys():
                        equipments[str(record.id)] += total
                    else:
                        equipments[str(record.id)] = total

        for row in labor_tran_rec:
            for record in subcat_rec:
                if row.subcategory_id == record.id:
                    total = row.rate * row.quantity
                    if str(record.id) in labors.keys():
                        labors[str(record.id)] += total
                    else:
                        labors[str(record.id)] = total

        for row in subcon_tran_rec:
            for record in subcat_rec:
                if row.subcategory_id == record.id:
                    total = row.rate * row.quantity
                    if str(record.id) in subcons.keys():
                        subcons[str(record.id)] += total
                    else:
                        subcons[str(record.id)] = total

        children = self.det_tree.get_children()
        if len(children) != 0:
            for child in children:
                self.det_tree.delete(child)

        counter = 0
        for sc_rec in subcat_rec:
            if (counter % 2) == 0:
                self.det_tree.insert('', 'end', str(sc_rec.id),
                                     text=str(sc_rec.id), tags='even')
            else:
                self.det_tree.insert('', 'end', str(sc_rec.id),
                                     text=str(sc_rec.id), tags='odd')
            self.det_tree.set(str(sc_rec.id), 'subcategory',
                              sc_rec.description)
            counter += 1

        for k, v in materials.items():
            self.det_tree.set(k, 'material', "{:0,.2f}".format(v))

        for k, v in equipments.items():
            self.det_tree.set(k, 'equipment', "{:0,.2f}".format(v))

        for k, v in labors.items():
            self.det_tree.set(k, 'labor', "{:0,.2f}".format(v))

        for k, v in subcons.items():
            self.det_tree.set(k, 'subcontractor', "{:0,.2f}".format(v))

        children = self.det_tree.get_children()
        subtotal = 0
        for child in children:
            total = 0
            if child in materials.keys():
                total += materials[child]
            if child in equipments.keys():
                total += equipments[child]
            if child in labors.keys():
                total += labors[child]
            if child in subcons.keys():
                total += subcons[child]

            if total != 0:
                self.det_tree.set(child, 'total', "{:0,.2f}".format(total))
                subtotal += total

        project = session.query(Project).options(joinedload(Project.currency)).filter(Project.id == prj_id).one()
        self.subtotal_lbl.config(text="{} {:0,.2f}".format(project.currency.code, subtotal))
        overhead = (project.overhead/100) * subtotal
        profit = (project.profit/100) * subtotal
        total_all = subtotal + overhead + profit

        self.overhead_lbl.config(text="{} {:0,.2f}".format(project.currency.code, overhead))
        self.profit_lbl.config(text="{} {:0,.2f}".format(project.currency.code, profit))
        self.total_lbl.config(text="{} {:0,.2f}".format(project.currency.code, total_all))
        session.close()

    def updateProjectView(self):
        """
        Insert the records in the table like widget if there is any and
        skip if there is none.
        """
        session = MainSession()
        records = session.query(Project).options(
            joinedload(Project.client), joinedload(Project.status)
            ).all()
        if len(records) != 0:
            children = self.prj_tree.get_children()
            if len(children) != 0:
                for child in children:
                    self.prj_tree.delete(child)

            counter = 1
            for record in records:
                if counter % 2 == 0:
                    self.prj_tree.insert("", "end", str(record.id),
                                         text=str(record.id), tags="even")
                else:
                    self.prj_tree.insert("", "end", str(record.id),
                                         text=str(record.id), tags="odd")
                self.prj_tree.set(str(record.id), "name", str(record.name))
                self.prj_tree.set(str(record.id), "client",
                                  str(record.client.name))
                self.prj_tree.set(str(record.id), "status",
                                  str(record.status.description))
                self.prj_tree.set(str(record.id), "start",
                                  record.startdate.strftime("%d/%m/%Y"))
                self.prj_tree.set(str(record.id), "end",
                                  record.enddate.strftime("%d/%m/%Y"))
                self.prj_tree.set(str(record.id), "overhead",
                                  "{:0.2f} %".format(record.overhead))
                self.prj_tree.set(str(record.id), "profit",
                                  "{:0.2f} %".format(record.profit))
                counter += 1
        session.close()

    def spreadFootingWindow(self):
        """ Show the footing window """
        footing_tp = tk.Toplevel(self)
        footing_tp.title("Spread Footing")
        win = SpreadFootingWindow(footing_tp)
        win.pack(expand=True, fill=tk.BOTH)

    def recColumnWindow(self):
        """ Show rectangular column window """
        rec_column_tp = tk.Toplevel(self)
        rec_column_tp.title("Rectangular Column")
        win = RecColumnWindow(rec_column_tp)
        win.pack(expand=True, fill=tk.BOTH)

    def roundColumnWindow(self):
        """ Show rounded column window """
        round_column_tp = tk.Toplevel(self)
        round_column_tp.title("Round Column")
        win = RoundColumnWindow(round_column_tp)
        win.pack(expand=True, fill=tk.BOTH)

    def conFootingWindow(self):
        """ Show continous footing  window """
        con_footing_tp = tk.Toplevel(self)
        con_footing_tp.title("Continous Footing")
        win = ConFootingWindow(con_footing_tp)
        win.pack(expand=True, fill=tk.BOTH)

    def foundationWallWindow(self):
        """ Show foundation wall window """
        foundation_wall_tp = tk.Toplevel(self)
        foundation_wall_tp.title("Foundation Wall")
        win = FoundationWallWindow(foundation_wall_tp)
        win.pack(expand=True, fill=tk.BOTH)

    def slabOnGradeWindow(self):
        """ Show slab on grade with rebar window """
        slab_on_grade_tp = tk.Toplevel(self)
        slab_on_grade_tp.title("Slab on Grade with Rebar")
        win = SlabOnGradeWindow(slab_on_grade_tp)
        win.pack(expand=True, fill=tk.BOTH)

    def reSlabOnGradeWindow(self):
        """ Show reinforced slab on grade """
        re_slab_on_grade_tp = tk.Toplevel(self)
        re_slab_on_grade_tp.title("Wire Mesh - Reinforced Slab on Grade")
        win = ReSlabOnGradeWindow(re_slab_on_grade_tp)
        win.pack(expand=True, fill=tk.BOTH)

    def slabOnMetalWindow(self):
        """ Show concrete slab on metal deck window """
        slab_on_metal_tp = tk.Toplevel(self)
        slab_on_metal_tp.title("Concrete Slab on Metal Deck")
        win = SlabOnMetalWindow(slab_on_metal_tp)
        win.pack(expand=True, fill=tk.BOTH)

    def stairsWindow(self):
        """ Show the calculations of stairs """
        stairs_tp = tk.Toplevel(self)
        stairs_tp.title("Stairs")
        win = StairsWindow(stairs_tp)
        win.pack(expand=True, fill=tk.BOTH)

    def clientWindow(self):
        """ Show client management window """
        client_tp = tk.Toplevel(self)
        win = ClientWindow(client_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def companyWindow(self):
        """ Show/edit company information """
        company_tp = tk.Toplevel(self)
        win = CompanyWindow(company_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def calendarWindow(self):
        """ Calendar tools """
        cal_tp = tk.Toplevel(self)
        date_now = datetime.today()
        win = CalendarWidget(date_now.year, date_now.month, cal_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def categoryWindow(self):
        """ Show category window """
        cat_tp = tk.Toplevel(self)
        win = CategoryWindow(cat_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def unitWindow(self):
        """ Show unit of measure management window """
        unit_tp = tk.Toplevel(self)
        win = UnitWindow(unit_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def statusWindow(self):
        """ Show status management window """
        status_tp = tk.Toplevel(self)
        win = StatusWindow(status_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def currencyWindow(self):
        """ Show currency management window """
        currency_tp = tk.Toplevel(self)
        win = CurrencyWindow(currency_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def subCategoryWindow(self):
        """ Show sub category management window """
        subcat_tp = tk.Toplevel(self)
        win = SubCategoryWindow(subcat_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def viewHelp(self):
        """ Show help information about the software """
        local_url = os.path.join(os.getcwd(), "help", "index.html")
        if os.path.isfile(local_url):
            webbrowser.open_new(local_url)

    def newProjectWindow(self):
        """ Show the window for creating new project """
        new_prj_tp = tk.Toplevel(self)
        win = ProjectWindow(new_prj_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()
        self.wait_window(new_prj_tp)
        self.updateProjectView()

    def editProjectWindowEvent(self, event):
        """ Show edit project window """
        record = event.widget.focus()
        if record != '':
            edit_prj_tp = tk.Toplevel(self)
            win = ProjectWindow(edit_prj_tp, False, int(record))
            win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
            win.focus_set()
            self.wait_window(edit_prj_tp)
            self.updateProjectView()

    def editProjectWindow(self):
        """ Show edit project window """
        record = self.prj_tree.focus()
        if record != '':
            edit_prj_tp = tk.Toplevel(self)
            win = ProjectWindow(edit_prj_tp, False, int(record))
            win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
            win.focus_set()
            self.wait_window(edit_prj_tp)
            self.updateProjectView()

    def materialWindow(self):
        """ Show material management window """
        mat_tp = tk.Toplevel(self)
        win = MaterialWindow(mat_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def equipmentWindow(self):
        """ Show equipment management window """
        equip_tp = tk.Toplevel(self)
        win = EquipmentWindow(equip_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def laborWindow(self):
        """ Show labor/manpower management window """
        labor_tp = tk.Toplevel(self)
        win = LaborWindow(labor_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def subContractorWindow(self):
        """ Show subcontractor management window """
        subcon_tp = tk.Toplevel(self)
        win = SubContractorWindow(subcon_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def aboutWindow(self):
        """ Show about dialog for the software """
        about_tp = tk.Toplevel(self)
        win = AboutWindow(about_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def materialTransactionWindow(self):
        """ Window for entering material transaction """
        item = self.prj_tree.focus()
        if item != '':
            mattrans_tp = tk.Toplevel(self)
            win = MaterialTransactionWindow(mattrans_tp, int(item))
            win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
            win.focus_set()
            self.wait_window(mattrans_tp)
            self.updateProjectView()

    def equipmentTransactionWindow(self):
        """ This window is used to adding/updating equipment transaction"""
        item = self.prj_tree.focus()
        if item != '':
            equiptrans_tp = tk.Toplevel(self)
            win = EquipmentTransactionWindow(equiptrans_tp, int(item))
            win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
            win.focus_set()
            self.wait_window(equiptrans_tp)
            self.updateProjectView()

    def laborTransactionWindow(self):
        """ This window will be used to add/update details for labor. """
        item = self.prj_tree.focus()
        if item != '':
            labortrans_tp = tk.Toplevel(self)
            win = LaborTransactionWindow(labortrans_tp, int(item))
            win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
            win.focus_set()
            self.wait_window(labortrans_tp)
            self.updateProjectView()

    def subConTransactionWindow(self):
        """ This is used for adding/updadting subcon transaction."""
        item = self.prj_tree.focus()
        if item != '':
            subcontrans_tp = tk.Toplevel(self)
            win = SubConTransactionWindow(subcontrans_tp, int(item))
            win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
            win.focus_set()
            self.wait_window(subcontrans_tp)
            self.updateProjectView()

    def deleteProject(self):
        """ This method is for deleting existing project record."""
        pass

    def closeEvent(self, event):
        """ Event method bind when closing the application. """
        self.close()

    def close(self):
        """ This method used to close the application. """
        self.master.destroy()


class ProjectWindow(tk.Frame):

    def __init__(self, master=None, mode=True, prj_id=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.mode = mode
        self.prj_id = prj_id
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.img_list = image_list()
        if self.mode:
            self.master.title("New Project")
        else:
            self.master.title("Edit Project")
        # The command below will set the window icon to 'berderose.ico'
        # on windows machine.
        if os.name == 'nt':
            self.master.iconbitmap("berderose.ico")
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        """ This method will initialize all other widgets. """
        session = MainSession()
        top_frame = tk.Frame(self)
        top_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill=tk.X)

        name_lbl = tk.Label(top_frame, text="Project Name:")
        name_lbl.grid(row=0, column=0, sticky=tk.W)
        self.name_entry = tk.Entry(top_frame, width=40)
        self.name_entry.grid(row=0, column=1, columnspan=5, sticky=tk.W)

        start_lbl = tk.Label(top_frame, text="Start Date:")
        start_lbl.grid(row=1, column=0, sticky=tk.W)
        self.start_entry = tk.Entry(top_frame)
        self.start_entry.grid(row=1, column=1, sticky=tk.EW)

        self.add_start_btn = tk.Button(top_frame, image=self.img_list['plus'])
        self.add_start_btn.grid(row=1, column=2)
        self.add_start_btn.config(command=self.startDate)

        end_lbl = tk.Label(top_frame, text="End Date:")
        end_lbl.grid(row=1, column=3, sticky=tk.W)
        self.end_entry = tk.Entry(top_frame)
        self.end_entry.grid(row=1, column=4)

        self.add_end_btn = tk.Button(top_frame, image=self.img_list['plus'])
        self.add_end_btn.grid(row=1, column=5)
        self.add_end_btn.config(command=self.endDate)

        client_lbl = tk.Label(top_frame, text="Client:")
        client_lbl.grid(row=2, column=0, sticky=tk.W)

        self.client_var = tk.StringVar()
        cl_values = []
        client_rec = session.query(Client.name).all()
        for val in client_rec:
            cl_values.append(val[0])
        if len(cl_values) != 0:
            self.client_var.set(cl_values[0])

        self.client_cb = ttk.Combobox(top_frame, textvariable=self.client_var)
        self.client_cb.grid(row=2, column=1, columnspan=2, sticky=tk.EW)
        self.client_cb.config(values=cl_values)

        profit_lbl = tk.Label(top_frame, text="Profit(%):")
        profit_lbl.grid(row=3, column=0, sticky=tk.W)
        self.profit_entry = tk.Entry(top_frame, width=10)
        self.profit_entry.grid(row=3, column=1, sticky=tk.W)
        overhead_lbl = tk.Label(top_frame, text="Overhead(%):")
        overhead_lbl.grid(row=3, column=3, sticky=tk.W)
        self.overhead_entry = tk.Entry(top_frame, width=10)
        self.overhead_entry.grid(row=3, column=4, sticky=tk.W)

        cur_lbl = tk.Label(top_frame, text="Currency:")
        cur_lbl.grid(row=4, column=0, sticky=tk.W)

        self.cur_var = tk.StringVar()
        cur_values = []
        cur_rec = session.query(Currency.code).all()
        for val in cur_rec:
            cur_values.append(val[0])
        if len(cur_values) != 0:
            self.cur_var.set(cur_values[0])

        self.cur_cb = ttk.Combobox(top_frame,
                                   textvariable=self.cur_var, width=10)
        self.cur_cb.grid(row=4, column=1, sticky=tk.W)
        self.cur_cb.config(values=cur_values)

        status_lbl = tk.Label(top_frame, text="Status:")
        status_lbl.grid(row=5, column=0, sticky=tk.W)

        self.status_var = tk.StringVar()
        stat_values = []
        stat_rec = session.query(Status.description).all()
        for val in stat_rec:
            stat_values.append(val[0])
        if len(stat_values) != 0:
            self.status_var.set(stat_values[0])
        self.status_cb = ttk.Combobox(top_frame, textvariable=self.status_var)
        self.status_cb.grid(row=5, column=1)
        self.status_cb.config(values=stat_values)

        self.save_btn = ttk.Button(bottom_frame, text="Save",
                                   image=self.img_list['save'])
        self.save_btn.pack(side=tk.RIGHT)
        self.save_btn.config(command=self.saveRecord, compound=tk.LEFT)

        self.close_btn = ttk.Button(bottom_frame, text="Cancel",
                                    image=self.img_list['cancel'])
        self.close_btn.config(command=self.close, compound=tk.LEFT)
        self.close_btn.pack(side=tk.RIGHT)

        session.close()

        if not self.mode:
            self.loadRecord()

    def loadRecord(self):
        if self.prj_id is not None:
            session = MainSession()
            record = session.query(Project).options(
                joinedload(Project.client), joinedload(Project.status), joinedload(Project.currency)).filter(
                    Project.id == self.prj_id).one()

            # Get all the required details.
            name = record.name
            startdate = record.startdate.strftime("%d/%m/%Y")
            enddate = record.enddate.strftime("%d/%m/%Y")
            client_name = record.client.name
            profit = "{:0.2f}".format(record.profit)
            overhead = "{:0.2f}".format(record.overhead)
            status_descp = record.status.description
            currency_code = record.currency.code

            # Make sure all entries has been deleted to avoid error
            self.name_entry.delete('0', tk.END)
            self.start_entry.delete('0', tk.END)
            self.end_entry.delete('0', tk.END)
            self.profit_entry.delete('0', tk.END)
            self.overhead_entry.delete('0', tk.END)

            # Insert values to the entries.
            self.name_entry.insert(tk.END, name)
            self.start_entry.insert(tk.END, startdate)
            self.end_entry.insert(tk.END, enddate)
            self.profit_entry.insert(tk.END, profit)
            self.overhead_entry.insert(tk.END, overhead)
            self.client_var.set(client_name)
            self.status_var.set(status_descp)
            self.cur_var.set(currency_code)

            session.close()

    def saveRecord(self):
        if self.prj_id is None:
            session = MainSession()

            name = self.name_entry.get()
            startdate = datetime.strptime(self.start_entry.get(), "%d/%m/%Y")
            enddate = datetime.strptime(self.end_entry.get(), "%d/%m/%Y")

            cl_rec = session.query(Client).filter(
                Client.name == self.client_var.get()).one()
            client_id = cl_rec.id

            profit = self.profit_entry.get()
            overhead = self.overhead_entry.get()

            st_rec = session.query(Status).filter(
                Status.description == self.status_var.get()).one()
            status_id = st_rec.id

            cur_rec = session.query(Currency).filter(
                Currency.code == self.cur_var.get()).one()
            currency_id = cur_rec.id

            data = {'name': name, 'startdate': startdate,
                    'enddate': enddate, 'client_id': client_id,
                    'overhead': overhead, 'profit': profit,
                    'status_id': status_id, 'currency_id': currency_id}

            record = Project(**data)
            session.add(record)
            session.commit()
            session.close()
        else:
            session = MainSession()

            name = self.name_entry.get()
            startdate = datetime.strptime(self.start_entry.get(), "%d/%m/%Y")
            enddate = datetime.strptime(self.end_entry.get(), "%d/%m/%Y")

            cl_rec = session.query(Client).filter(
                Client.name == self.client_var.get()).one()
            client_id = cl_rec.id

            profit = self.profit_entry.get()
            overhead = self.overhead_entry.get()

            st_rec = session.query(Status).filter(
                Status.description == self.status_var.get()).one()
            status_id = st_rec.id

            cur_rec = session.query(Currency).filter(
                Currency.code == self.cur_var.get()).one()
            currency_id = cur_rec.id

            record = session.query(Project).filter(
                Project.id == self.prj_id).one()

            record.name = name
            record.startdate = startdate
            record.enddate = enddate
            record.client_id = client_id
            record.profit = profit
            record.overhead = overhead
            record.status_id = status_id
            record.currency_id = currency_id

            session.commit()
            session.close()
        self.close()

    def startDate(self):
        cal_tp = tk.Toplevel(self)
        date_now = datetime.today()
        win = CalendarWidget(date_now.year, date_now.month, cal_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        self.wait_window(cal_tp)
        if win.date is not None:
            self.start_entry.delete('0', tk.END)
            self.start_entry.insert(tk.END, win.date)

    def endDate(self):
        cal_tp = tk.Toplevel(self)
        date_now = datetime.today()
        win = CalendarWidget(date_now.year, date_now.month, cal_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        self.wait_window(cal_tp)
        if win.date is not None:
            self.end_entry.delete('0', tk.END)
            self.end_entry.insert(tk.END, win.date)

    def close(self):
        print("Closing Project Window.")
        self.master.destroy()


class ClientWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = ''
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        # self.master.geometry("800x600+20+20")
        self.master.title("Client")
        self.img_list = image_list()
        if os.name == 'nt':
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        name_lbl = tk.Label(top_frame, text="Name:")
        name_lbl.grid(row=0, column=0, sticky="w")
        self.name_entry = tk.Entry(top_frame, width=35)
        self.name_entry.grid(row=0, column=1, sticky="w")
        email_lbl = tk.Label(top_frame, text="E-mail:")
        email_lbl.grid(row=1, column=0, sticky="w")
        self.email_entry = tk.Entry(top_frame, width=50)
        self.email_entry.grid(row=1, column=1, sticky="w")

        tel_lbl = tk.Label(top_frame, text="Telephone:")
        tel_lbl.grid(row=0, column=2)
        self.tel_entry = tk.Entry(top_frame)
        self.tel_entry.grid(row=0, column=3, sticky="w")
        fax_lbl = tk.Label(top_frame, text="Fax:")
        fax_lbl.grid(row=1, column=2)
        self.fax_entry = tk.Entry(top_frame)
        self.fax_entry.grid(row=1, column=3, sticky="w")

        addr_lbl = tk.Label(top_frame, text="Address:")
        addr_lbl.grid(row=2, column=0, sticky="w")
        self.addr_text = tk.Text(top_frame, height=5, width=30)
        self.addr_text.grid(row=3, column=0, columnspan=2,
                            sticky="nesw", pady=5)

        self.search_btn = ttk.Button(top_frame, text="Search")
        self.search_btn.grid(row=0, column=4)
        self.search_btn.config(image=self.img_list['search'], compound=tk.LEFT)
        self.add_btn = ttk.Button(top_frame, text="Add/Save")
        self.add_btn.grid(row=1, column=4)
        self.add_btn.config(command=self.addRecord,
                            image=self.img_list['save'], compound=tk.LEFT)

        self.client_view = ttk.Treeview(mid_frame)
        self.client_view.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

        self.client_view.tag_configure("even", background="#ffdbca")
        self.client_view.tag_configure("odd", background="#ffb591")

        self.client_view['columns'] = ("name", "telephone", "fax",
                                       "email", "address")
        self.client_view.heading("#0", text="ID")
        self.client_view.heading("name", text="Name")
        self.client_view.heading("telephone", text="Telephone")
        self.client_view.heading("fax", text="Fax")
        self.client_view.heading("email", text="Email")
        self.client_view.heading("address", text="Address")

        self.client_view.column("#0", width=75, stretch=False)
        self.client_view.column("telephone", width=100, stretch=False)
        self.client_view.column("fax", width=100, stretch=False)
        self.client_view.column("email", width=120, stretch=False)
        self.client_view.column("name", width=150, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill="y")

        self.vert_scroll.config(command=self.client_view.yview)
        self.client_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit")
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(image=self.img_list['edit'], compound=tk.LEFT)
        self.edit_btn.config(command=self.editRecord)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete")
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(image=self.img_list['minus'], compound=tk.LEFT)
        self.delete_btn.config(command=self.deleteRecord)
        self.export_btn = ttk.Button(bottom_frame, text="Export")
        self.export_btn.grid(row=0, column=2)
        self.export_btn.config(image=self.img_list['export'], compound=tk.LEFT)
        self.close_btn = ttk.Button(bottom_frame, text="Close")
        self.close_btn.grid(row=0, column=3)

        self.close_btn.config(command=self.close,
                              image=self.img_list['cancel'], compound=tk.LEFT)

        self.updateView()
        self.name_entry.focus_set()

    def updateView(self):
        session = MainSession()
        records = session.query(Client).all()
        if len(records) != 0:
            children = self.client_view.get_children()
            if len(children) != 0:
                for child in children:
                    self.client_view.delete(child)

            counter = 1
            for record in records:
                if counter % 2 == 0:
                    self.client_view.insert("", "end", str(record.id),
                                            text=str(record.id), tags="even")
                else:
                    self.client_view.insert("", "end", str(record.id),
                                            text=str(record.id), tags="odd")
                self.client_view.set(str(record.id), "name", str(record.name))
                self.client_view.set(str(record.id), "telephone",
                                     str(record.telephone))
                self.client_view.set(str(record.id), "fax", str(record.fax))
                self.client_view.set(str(record.id), "email",
                                     str(record.email))
                self.client_view.set(str(record.id), "address",
                                     str(record.address))
                counter += 1

        session.close()

    def addRecord(self):
        session = MainSession()
        name = self.name_entry.get()
        telephone = self.tel_entry.get()
        fax = self.fax_entry.get()
        email = self.email_entry.get()
        address = self.addr_text.get("1.0", tk.END)
        if self.rec_id == "":
            data = {'name': name,
                    'address': address,
                    'telephone': telephone,
                    'fax': fax,
                    'email': email}
            record = Client(**data)
            session.add(record)
            session.commit()
        else:
            record = session.query(Client).filter(
                Client.id == self.rec_id).one()
            record.name = name
            record.address = address
            record.telephone = telephone
            record.fax = fax
            record.email = email
            session.commit()
            self.rec_id = ""

        session.close()

        self.name_entry.delete("0", tk.END)
        self.tel_entry.delete("0", tk.END)
        self.fax_entry.delete("0", tk.END)
        self.email_entry.delete("0", tk.END)
        self.addr_text.delete("1.0", tk.END)

        self.updateView()
        self.name_entry.focus_set()

    def editRecord(self):
        record = self.client_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.name_entry.delete("0", tk.END)
            self.tel_entry.delete("0", tk.END)
            self.fax_entry.delete("0", tk.END)
            self.email_entry.delete("0", tk.END)
            self.addr_text.delete("1.0", tk.END)

            item = self.client_view.item(record, "values")
            name = item[0]
            telephone = item[1]
            fax = item[2]
            email = item[3]
            address = item[4]
            self.name_entry.insert(tk.END, name)
            self.tel_entry.insert(tk.END, telephone)
            self.fax_entry.insert(tk.END, fax)
            self.email_entry.insert(tk.END, email)
            self.addr_text.insert('1.0', address)
        else:
            mb.showwarning(
                "No Record", "Please select a record and try again.",
                parent=self)

    def deleteRecord(self):
        session = MainSession()
        record = self.client_view.focus()
        if record != "":
            check = mb.showwarning(
                "Delete Record?",
                "Are you sure you want to\n delete the record?",
                parent=self)
            if check:
                rec_id = int(record)
                record = session.query(Client).filter(
                    Client.id == rec_id).one()
                session.delete(record)
                session.commit()
        session.close()

        self.updateView()

    def searchRecord(self):
        pass

    def exportRecord(self):
        pass

    def close(self):
        print("Closing Client Window.")
        self.master.destroy()


class CategoryWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = None
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        # self.master.geometry("640x480+20+20")
        self.master.title("Category")
        self.img_list = image_list()
        if os.name == "nt":
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        code_lbl = tk.Label(top_frame, text="Code:")
        code_lbl.grid(row=0, column=0, sticky="w")
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=0, column=1, sticky="w")
        descp_lbl = tk.Label(top_frame, text="Description:")
        descp_lbl.grid(row=1, column=0, sticky="w")
        self.descp_entry = tk.Entry(top_frame, width=50)
        self.descp_entry.grid(row=1, column=1)
        self.search_btn = ttk.Button(top_frame, text="Search")
        self.search_btn.grid(row=1, column=2)
        self.search_btn.config(image=self.img_list['search'], compound=tk.LEFT)
        self.add_btn = ttk.Button(top_frame, text="Add/Save")
        self.add_btn.grid(row=1, column=3)
        self.add_btn.config(command=self.addRecord, image=self.img_list['save'])
        self.add_btn.config(compound=tk.LEFT)

        self.cat_view = ttk.Treeview(mid_frame)
        self.cat_view.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

        self.cat_view.tag_configure("even", background="#ffdbca")
        self.cat_view.tag_configure("odd", background="#ffb591")

        self.cat_view['columns'] = ("code", "description")
        self.cat_view.heading("#0", text="ID")
        self.cat_view.heading("code", text="Code")
        self.cat_view.heading("description", text="Description")
        self.cat_view.column("#0", width=75, stretch=False)
        self.cat_view.column("code", width=100, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill="y")

        self.vert_scroll.config(command=self.cat_view.yview)
        self.cat_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list['edit'])
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord, compound=tk.LEFT)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list['minus'])
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord, compound=tk.LEFT)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list['export'])
        self.export_btn.grid(row=0, column=2)
        self.export_btn.config(compound=tk.LEFT)
        self.close_btn = ttk.Button(bottom_frame, text="Close",
                                    image=self.img_list['cancel'])
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(compound=tk.LEFT)

        self.close_btn.config(command=self.close)

        self.updateView()
        self.code_entry.focus_set()

    def updateView(self):
        session = MainSession()
        records = session.query(Category).all()
        if len(records) != 0:
            children = self.cat_view.get_children()
            if len(children) != 0:
                for child in children:
                    self.cat_view.delete(child)

            counter = 1
            for record in records:
                if counter % 2 == 0:
                    self.cat_view.insert("", "end", str(record.id),
                                         text=str(record.id), tags="even")
                else:
                    self.cat_view.insert("", "end", str(record.id),
                                         text=str(record.id), tags="odd")
                self.cat_view.set(str(record.id), "code", str(record.code))
                self.cat_view.set(str(record.id), "description",
                                  str(record.description))
                counter += 1

        session.close()

    def addRecord(self):
        session = MainSession()
        code = self.code_entry.get()
        description = self.descp_entry.get()
        cat_recs = session.query(Category).all()
        if self.rec_id is None:
            for cat_rec in cat_recs:
                if cat_rec.code == code:
                    message = "Please avoid using duplicate code."
                    mb.showwarning("Duplicate Code", message, parent=self)
                    self.code_entry.focus_set()
                    self.code_entry.select_range("0", tk.END)
                    return

            record = Category(code=code, description=description)
            session.add(record)
            session.commit()
        else:
            record = session.query(Category).filter(
                Category.id == self.rec_id).one()
            record.code = code
            record.description = description
            session.commit()
            self.rec_id = None

        session.close()

        self.code_entry.delete("0", tk.END)
        self.descp_entry.delete("0", tk.END)

        self.updateView()
        self.code_entry.focus_set()

    def editRecord(self):
        record = self.cat_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.code_entry.delete("0", tk.END)
            self.descp_entry.delete("0", tk.END)
            item = self.cat_view.item(record, "values")
            code = item[0]
            description = item[1]
            self.code_entry.insert(tk.END, code)
            self.descp_entry.insert(tk.END, description)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

    def deleteRecord(self):
        session = MainSession()
        record = self.cat_view.focus()
        if record != "":
            rec_id = int(record)
            record = session.query(Category).filter(
                Category.id == rec_id).one()
            session.delete(record)
            session.commit()
        session.close()
        self.updateView()

    def searchRecord(self):
        pass

    def exportRecord(self):
        pass

    def close(self):
        print("Closing Category Window")
        self.master.destroy()


class UnitWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = None
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        # self.master.geometry("640x480+20+20")
        self.master.title("Unit of Measurements")
        self.img_list = image_list()
        if os.name == "nt":
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        code_lbl = tk.Label(top_frame, text="Code:")
        code_lbl.grid(row=0, column=0, sticky="w")
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=0, column=1, sticky="w")
        descp_lbl = tk.Label(top_frame, text="Description:")
        descp_lbl.grid(row=1, column=0, sticky="w")
        self.descp_entry = tk.Entry(top_frame, width=50)
        self.descp_entry.grid(row=1, column=1)
        self.search_btn = ttk.Button(top_frame, text="Search",
                                     image=self.img_list['search'])
        self.search_btn.grid(row=1, column=2)
        self.search_btn.config(compound=tk.LEFT)
        self.add_btn = ttk.Button(top_frame, text="Add/Save",
                                  image=self.img_list['save'])
        self.add_btn.grid(row=1, column=3)
        self.add_btn.config(command=self.addRecord, compound=tk.LEFT)

        self.unit_view = ttk.Treeview(mid_frame)
        self.unit_view.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

        self.unit_view.tag_configure("even", background="#ffdbca")
        self.unit_view.tag_configure("odd", background="#ffb591")

        self.unit_view['columns'] = ("code", "description")
        self.unit_view.heading("#0", text="ID")
        self.unit_view.heading("code", text="Code")
        self.unit_view.heading("description", text="Description")
        self.unit_view.column("#0", width=75, stretch=False)
        self.unit_view.column("code", width=100, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill="y")

        self.vert_scroll.config(command=self.unit_view.yview)
        self.unit_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list['edit'])
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord, compound=tk.LEFT)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list['minus'])
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord, compound=tk.LEFT)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list['export'])
        self.export_btn.grid(row=0, column=2)
        self.export_btn.config(compound=tk.LEFT)
        self.close_btn = ttk.Button(bottom_frame, text="Close",
                                    image=self.img_list['cancel'])
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(command=self.close, compound=tk.LEFT)

        self.updateView()

    def updateView(self):
        session = MainSession()
        records = session.query(Unit).all()
        if len(records) != 0:
            children = self.unit_view.get_children()
            if len(children) != 0:
                for child in children:
                    self.unit_view.delete(child)

            counter = 1
            for record in records:
                if counter % 2 == 0:
                    self.unit_view.insert("", "end", str(record.id),
                                          text=str(record.id), tags="even")
                else:
                    self.unit_view.insert("", "end", str(record.id),
                                          text=str(record.id), tags="odd")
                self.unit_view.set(str(record.id), "code", str(record.code))
                self.unit_view.set(str(record.id),
                                   "description", str(record.description))
                counter += 1

        session.close()

    def addRecord(self):
        session = MainSession()
        code = self.code_entry.get()
        description = self.descp_entry.get()
        unit_recs = session.query(Unit).all()
        if self.rec_id is None:
            for unit_rec in unit_recs:
                if unit_rec.code == code:
                    message = "Please avoid using duplicate code."
                    mb.showwarning("Duplicate Code", message, parent=self)
                    self.code_entry.focus_set()
                    self.code_entry.select_range('0', tk.END)
                    return

            record = Unit(code=code, description=description)
            session.add(record)
            session.commit()
        else:
            record = session.query(Unit).filter(
                Unit.id == self.rec_id).one()
            record.code = code
            record.description = description
            session.commit()
            self.rec_id = None

        session.close()

        self.code_entry.delete("0", tk.END)
        self.descp_entry.delete("0", tk.END)

        self.updateView()
        self.code_entry.focus_set()

    def editRecord(self):
        record = self.unit_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.code_entry.delete("0", tk.END)
            self.descp_entry.delete("0", tk.END)
            item = self.unit_view.item(record, "values")
            code = item[0]
            description = item[1]
            self.code_entry.insert(tk.END, code)
            self.descp_entry.insert(tk.END, description)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

    def deleteRecord(self):
        session = MainSession()
        record = self.unit_view.focus()
        if record != "":
            rec_id = int(record)
            record = session.query(Unit).filter(Unit.id == rec_id).one()
            session.delete(record)
            session.commit()
        session.close()

        self.updateView()

    def searchRecord(self):
        pass

    def exportRecord(self):
        pass

    def close(self):
        print("Closing The Unit Window")
        self.master.destroy()


class CurrencyWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = None
        self.img_list = image_list(size=(16, 16))
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        # self.master.geometry("640x480+20+20")
        self.master.title("Currency")
        if os.name == 'nt':
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        code_lbl = tk.Label(top_frame, text="Code:")
        code_lbl.grid(row=0, column=0, sticky="w")
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=0, column=1, sticky="w")
        descp_lbl = tk.Label(top_frame, text="Description:")
        descp_lbl.grid(row=1, column=0, sticky="w")
        self.descp_entry = tk.Entry(top_frame, width=50)
        self.descp_entry.grid(row=1, column=1)
        self.search_btn = ttk.Button(top_frame,
                                     text="Search", image=self.img_list['search'])
        self.search_btn.grid(row=1, column=2)
        self.search_btn.config(compound=tk.LEFT)
        self.add_btn = ttk.Button(top_frame, text="Add/Save",
                                  image=self.img_list['save'])
        self.add_btn.grid(row=1, column=3)
        self.add_btn.config(command=self.addRecord, compound=tk.LEFT)

        self.currency_view = ttk.Treeview(mid_frame)
        self.currency_view.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

        self.currency_view.tag_configure("even", background="#ffdbca")
        self.currency_view.tag_configure("odd", background="#ffb591")

        self.currency_view['columns'] = ("code", "description")
        self.currency_view.heading("#0", text="ID")
        self.currency_view.heading("code", text="Code")
        self.currency_view.heading("description", text="Description")
        self.currency_view.column("#0", width=75, stretch=False)
        self.currency_view.column("code", width=100, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill="y")

        self.vert_scroll.config(command=self.currency_view.yview)
        self.currency_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list["edit"])
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord, compound=tk.LEFT)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list["minus"])
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord, compound=tk.LEFT)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list["export"])
        self.export_btn.grid(row=0, column=2)
        self.export_btn.config(compound=tk.LEFT)
        self.close_btn = ttk.Button(bottom_frame, text="Close",
                                    image=self.img_list["cancel"])
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(command=self.close, compound=tk.LEFT)

        self.updateView()

    def updateView(self):
        session = MainSession()
        records = session.query(Currency).all()
        if len(records) != 0:
            children = self.currency_view.get_children()
            if len(children) != 0:
                for child in children:
                    self.currency_view.delete(child)

            counter = 1
            for record in records:
                if counter % 2 == 0:
                    self.currency_view.insert("", "end", str(record.id),
                                              text=str(record.id),
                                              tags="even")
                else:
                    self.currency_view.insert("", "end", str(record.id),
                                              text=str(record.id), tags="odd")
                self.currency_view.set(str(record.id), "code",
                                       str(record.code))
                self.currency_view.set(str(record.id), "description",
                                       str(record.description))
                counter += 1

        session.close()

    def addRecord(self):
        session = MainSession()
        code = self.code_entry.get()
        description = self.descp_entry.get()
        cur_recs = session.query(Currency).all()
        if self.rec_id is None:
            for cur_rec in cur_recs:
                if cur_rec.code == code:
                    message = "Please avoid using duplicate code."
                    mb.showwarning("Duplicate Code", message, parent=self)
                    self.code_entry.focus_set()
                    self.code_entry.select_range('0', tk.END)
                    return

            record = Currency(code=code, description=description)
            session.add(record)
            session.commit()
        else:
            record = session.query(Currency).filter(
                Currency.id == self.rec_id).one()
            record.code = code
            record.description = description
            session.commit()
            self.rec_id = None

        session.close()

        self.code_entry.delete("0", tk.END)
        self.descp_entry.delete("0", tk.END)

        self.updateView()
        self.code_entry.focus_set()

    def editRecord(self):
        record = self.currency_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.code_entry.delete("0", tk.END)
            self.descp_entry.delete("0", tk.END)
            item = self.currency_view.item(record, "values")
            code = item[0]
            description = item[1]
            self.code_entry.insert(tk.END, code)
            self.descp_entry.insert(tk.END, description)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

    def deleteRecord(self):
        session = MainSession()
        record = self.currency_view.focus()
        if record != "":
            rec_id = int(record)
            record = session.query(Currency).filter(
                Currency.id == rec_id).one()
            session.delete(record)
            session.commit()
        session.close()

        self.updateView()

    def searchRecord(self):
        pass

    def exportRecord(self):
        pass

    def close(self):
        print("Closing Currency Window")
        self.master.destroy()


class StatusWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = ""
        self.img_list = image_list()
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        # self.master.geometry("640x480+20+20")
        self.master.title("Status")
        if os.name == 'nt':
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        code_lbl = tk.Label(top_frame, text="Code:")
        code_lbl.grid(row=0, column=0, sticky="w")
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=0, column=1, sticky="w")
        descp_lbl = tk.Label(top_frame, text="Description:")
        descp_lbl.grid(row=1, column=0, sticky="w")
        self.descp_entry = tk.Entry(top_frame, width=50)
        self.descp_entry.grid(row=1, column=1)
        self.search_btn = ttk.Button(top_frame, text="Search",
                                     image=self.img_list['search'])
        self.search_btn.grid(row=1, column=2)
        self.search_btn.config(compound=tk.LEFT)
        self.add_btn = ttk.Button(top_frame, text="Add/Save",
                                  image=self.img_list['save'])
        self.add_btn.grid(row=1, column=3)
        self.add_btn.config(command=self.addRecord, compound=tk.LEFT)

        self.status_view = ttk.Treeview(mid_frame)
        self.status_view.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

        self.status_view.tag_configure("even", background="#ffdbca")
        self.status_view.tag_configure("odd", background="#ffb591")

        self.status_view['columns'] = ("code", "description")
        self.status_view.heading("#0", text="ID")
        self.status_view.heading("code", text="Code")
        self.status_view.heading("description", text="Description")
        self.status_view.column("#0", width=75, stretch=False)
        self.status_view.column("code", width=100, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill="y")

        self.vert_scroll.config(command=self.status_view.yview)
        self.status_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list['edit'])
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord, compound=tk.LEFT)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list['minus'])
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord, compound=tk.LEFT)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list['export'])
        self.export_btn.grid(row=0, column=2)
        self.export_btn.config(compound=tk.LEFT)
        self.close_btn = ttk.Button(bottom_frame, text="Close",
                                    image=self.img_list['cancel'])
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(command=self.close, compound=tk.LEFT)

        self.updateView()

    def updateView(self):
        session = MainSession()
        records = session.query(Status).all()
        if len(records) != 0:
            children = self.status_view.get_children()
            if len(children) != 0:
                for child in children:
                    self.status_view.delete(child)

            counter = 1
            for record in records:
                if counter % 2 == 0:
                    self.status_view.insert("", "end",
                                            str(record.id),
                                            text=str(record.id), tags="even")
                else:
                    self.status_view.insert("", "end",
                                            str(record.id),
                                            text=str(record.id), tags="odd")
                self.status_view.set(str(record.id), "code", str(record.code))
                self.status_view.set(str(record.id), "description",
                                     str(record.description))
                counter += 1

        session.close()

    def addRecord(self):
        session = MainSession()
        code = self.code_entry.get()
        description = self.descp_entry.get()
        try:
            if self.rec_id == "":
                record = Status(code=code, description=description)
                session.add(record)
                session.commit()
            else:
                record = session.query(Status).filter(
                    Status.id == self.rec_id).one()
                record.code = code
                record.description = description
                session.commit()
                self.rec_id = ""

            self.code_entry.delete("0", tk.END)
            self.descp_entry.delete("0", tk.END)

            self.updateView()
            self.code_entry.focus_set()
        except IntegrityError:
            mb.showwarning("Duplicate Code",
                           "Please avoid using duplicate code.", parent=self)
            self.code_entry.focus_set()
            self.code_entry.select_range('0', tk.END)

        session.close()

    def editRecord(self):
        record = self.status_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.code_entry.delete("0", tk.END)
            self.descp_entry.delete("0", tk.END)
            item = self.status_view.item(record, "values")
            code = item[0]
            description = item[1]
            self.code_entry.insert(tk.END, code)
            self.descp_entry.insert(tk.END, description)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

    def deleteRecord(self):
        session = MainSession()
        record = self.status_view.focus()
        if record != "":
            rec_id = int(record)
            record = session.query(Status).filter(Status.id == rec_id).one()
            session.delete(record)
            session.commit()
        session.close()

        self.updateView()

    def searchRecord(self):
        pass

    def exportRecord(self):
        pass

    def close(self):
        print("Closing Status Window")
        self.master.destroy()


class SubCategoryWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = None
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        # self.master.geometry("640x480+20+20")
        self.master.title("Sub-Category")
        self.img_list = image_list()
        if os.name == 'nt':
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        code_lbl = tk.Label(top_frame, text="Code:")
        code_lbl.grid(row=0, column=0, sticky="w")
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=0, column=1, sticky="w")
        descp_lbl = tk.Label(top_frame, text="Description:")
        descp_lbl.grid(row=1, column=0, sticky="w")
        self.descp_entry = tk.Entry(top_frame, width=40)
        self.descp_entry.grid(row=1, column=1)
        cat_lbl = tk.Label(top_frame, text="Category:")
        cat_lbl.grid(row=2, column=0, sticky='w')

        session = MainSession()
        records = session.query(Category.description).all()
        cat_values = []
        for record in records:
            cat_values.append(record[0])
        session.close()

        self.cb_var = tk.StringVar()
        if len(cat_values) > 0:
            self.cb_var.set(cat_values[0])

        self.cat_cb = ttk.Combobox(top_frame, textvariable=self.cb_var,
                                   values=cat_values)
        self.cat_cb.grid(row=2, column=1, sticky='we')

        self.search_btn = ttk.Button(top_frame, text="Search",
                                     image=self.img_list['search'])
        self.search_btn.grid(row=2, column=2)
        self.search_btn.config(compound=tk.LEFT)
        self.add_btn = ttk.Button(top_frame, text="Add/Save",
                                  image=self.img_list['save'])
        self.add_btn.grid(row=2, column=3)
        self.add_btn.config(command=self.addRecord, compound=tk.LEFT)

        self.subcat_view = ttk.Treeview(mid_frame)
        self.subcat_view.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

        self.subcat_view.tag_configure("even", background="#ffdbca")
        self.subcat_view.tag_configure("odd", background="#ffb591")

        self.subcat_view['columns'] = ("code", "description", "category")
        self.subcat_view.heading("#0", text="ID")
        self.subcat_view.heading("code", text="Code")
        self.subcat_view.heading("description", text="Description")
        self.subcat_view.heading("category", text="Category")
        self.subcat_view.column("#0", width=75, stretch=False)
        self.subcat_view.column("code", width=100, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill="y")

        self.vert_scroll.config(command=self.subcat_view.yview)
        self.subcat_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list['edit'])
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord, compound=tk.LEFT)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list['minus'])
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord, compound=tk.LEFT)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list['export'])
        self.export_btn.grid(row=0, column=2)
        self.export_btn.config(compound=tk.LEFT)
        self.close_btn = ttk.Button(bottom_frame, text="Close",
                                    image=self.img_list['cancel'])
        self.close_btn.grid(row=0, column=3)

        self.close_btn.config(command=self.close, compound=tk.LEFT)

        self.updateView()

    def updateView(self):
        session = MainSession()
        records = session.query(SubCategory).options(joinedload(SubCategory.category)).all()
        if len(records) != 0:
            children = self.subcat_view.get_children()
            if len(children) != 0:
                for child in children:
                    self.subcat_view.delete(child)

            counter = 1
            for record in records:
                if counter % 2 == 0:
                    self.subcat_view.insert("", "end", str(record.id),
                                            text=str(record.id), tags="even")
                else:
                    self.subcat_view.insert("", "end", str(record.id),
                                            text=str(record.id), tags="odd")
                self.subcat_view.set(str(record.id), "code", str(record.code))
                self.subcat_view.set(str(record.id), "description",
                                     str(record.description))
                self.subcat_view.set(str(record.id), "category",
                                     record.category.description)
                counter += 1

        session.close()

    def addRecord(self):
        session = MainSession()
        cat_rec = session.query(Category).filter(
            Category.description == self.cb_var.get()).one()
        code = self.code_entry.get()
        description = self.descp_entry.get()
        if self.rec_id is None:
            subcat_recs = session.query(SubCategory).all()
            for subcat_rec in subcat_recs:
                if subcat_rec.code == code:
                    message = "Please avoid using duplicate code."
                    mb.showwarning("Duplicate Code", message, parent=self)
                    self.code_entry.focus_set()
                    self.code_entry.select_range('0', tk.END)
                    return

            record = SubCategory(code=code, description=description,
                                 category_id=cat_rec.id)
            session.add(record)
            session.commit()
        else:
            record = session.query(SubCategory).filter(
                SubCategory.id == self.rec_id).one()
            record.code = code
            record.description = description
            session.commit()
            self.rec_id = None

        session.close()

        self.code_entry.delete("0", tk.END)
        self.descp_entry.delete("0", tk.END)

        self.updateView()
        self.code_entry.focus_set()

    def editRecord(self):
        record = self.subcat_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.code_entry.delete("0", tk.END)
            self.descp_entry.delete("0", tk.END)
            item = self.subcat_view.item(record, "values")
            code = item[0]
            description = item[1]
            category = item[2]
            self.code_entry.insert(tk.END, code)
            self.descp_entry.insert(tk.END, description)
            self.cb_var.set(category)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

    def deleteRecord(self):
        session = MainSession()
        record = self.subcat_view.focus()
        if record != "":
            rec_id = int(record)
            record = session.query(SubCategory).filter(
                SubCategory.id == rec_id).one()
            session.delete(record)
            session.commit()
        session.close()

        self.updateView()
        self.code_entry.focus_set()

    def searchRecord(self):
        pass

    def exportRecord(self):
        pass

    def close(self):
        print("Closing Sub-Category Window")
        self.master.destroy()


class CalendarWidget(tk.Frame):

    def __init__(self, year, month, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.year = year
        self.month = month
        self.date = None
        self.cal = cl.Calendar(6)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.title("Calendar")
        self.master.resizable(False, False)
        if os.name == "nt":
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()

    def setup_ui(self):
        container = tk.Frame(self)
        container.pack(expand=True, fill=tk.BOTH)

        self.prev_btn = tk.Button(container, text="<",
                                  font="Times 12 bold", fg="blue", bg="white")
        self.prev_btn.pack(side=tk.LEFT, fill="y")
        self.prev_btn.bind("<Button-1>", self.btnHandler)

        self.month_var = tk.StringVar()
        self.month_lbl = tk.Label(container, textvariable=self.month_var,
                                  font="Times 14 bold", fg="blue", bg="white")
        self.month_lbl.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        self.next_btn = tk.Button(container, text=">",
                                  font="Times 12 bold", fg="blue", bg="white")
        self.next_btn.pack(side=tk.LEFT, fill="y")
        self.next_btn.bind("<Button-1>", self.btnHandler)

        self.days_frame = tk.Frame(self)
        self.days_frame.pack(expand=True, fill=tk.BOTH)

        self.updateCalendar()

    def updateCalendar(self):
        children = self.days_frame.winfo_children()
        if len(children) != 0:
            for child in children:
                child.destroy()

        days = ["Sun", "Mon", "Tue",
                "Wed", "Thu", "Fri",
                "Sat"]
        month = ["January", "February", "March",
                 "April", "May", "June",
                 "July", "August", "September",
                 "October", "November", "December"]

        self.month_var.set(month[self.month-1]+" "+str(self.year))
        list_of_days = self.cal.monthdayscalendar(self.year, self.month)
        list_of_days.insert(0, days)

        for idx, week in enumerate(list_of_days):
            for idx1, day in enumerate(week):
                if day == 0:
                    self.day_lbl = tk.Label(self.days_frame, text=str(""),
                                            relief=tk.RAISED,
                                            state="disabled",
                                            font="Times 12 normal",
                                            bg="white")
                    self.day_lbl.grid(row=idx, column=idx1, sticky="nesw")
                elif day in days:
                    self.day_lbl = tk.Label(self.days_frame,
                                            text=str(day), relief=tk.RAISED,
                                            font="Times 12 bold")
                    self.day_lbl.grid(row=idx, column=idx1, sticky="nesw")
                    if (day == "Sun") or (day == "Sat"):
                        self.day_lbl.config(fg="red")
                else:
                    self.day_btn = tk.Button(self.days_frame, text=str(day),
                                             width=3, font="Times 12 normal",
                                             bg="white")
                    self.day_btn.grid(row=idx, column=idx1, sticky="nesw")
                    self.day_btn.bind("<Button-1>", self.printEvent)
                    if (idx1 == 0) or (idx1 == 6):
                        self.day_btn.config(fg='red')

    def printEvent(self, event):
        if event.widget.winfo_class() == "Button":
            day = event.widget.cget("text")
            self.date = "%s/%s/%s" % ("{:02}".format(int(day)),
                                      "{:02}".format(self.month),
                                      str(self.year))
            self.close()

    def btnHandler(self, event):
        if event.widget.cget('text') == "<":
            if (self.month - 1) == 0:
                self.month = 12
                self.year = self.year - 1
                self.updateCalendar()
            else:
                self.month = self.month - 1
                self.updateCalendar()
        elif event.widget.cget('text') == ">":
            if (self.month + 1) > 12:
                self.month = 1
                self.year = self.year + 1
                self.updateCalendar()
            else:
                self.month = self.month + 1
                self.updateCalendar()

    def close(self):
        self.master.destroy()


class MaterialWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = None
        self.img_list = image_list()
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.title("Materials")
        # self.master.geometry("640x480+20+20")
        if os.name == 'nt':
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill=tk.X)
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bot_frame = tk.Frame(self)
        bot_frame.pack(fill=tk.X)

        code_lbl = tk.Label(top_frame, text="Item Code:")
        code_lbl.grid(row=0, column=0, sticky=tk.W)
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=0, column=1, sticky=tk.W)
        descp_lbl = tk.Label(top_frame, text="Description:")
        descp_lbl.grid(row=1, column=0, sticky=tk.W)
        self.descp_entry = tk.Entry(top_frame, width=40)
        self.descp_entry.grid(row=1, column=1, sticky=tk.W)
        rate_lbl = tk.Label(top_frame, text="Rate:")
        rate_lbl.grid(row=2, column=0, sticky=tk.W)
        self.rate_entry = tk.Entry(top_frame, width=10)
        self.rate_entry.grid(row=2, column=1, sticky=tk.W)
        unit_lbl = tk.Label(top_frame, text="Unit:")
        unit_lbl.grid(row=3, column=0, sticky=tk.W)

        self.unit_var = tk.StringVar()
        unit_val = []

        session = MainSession()
        record = session.query(Unit.code).all()
        for val in record:
            unit_val.append(val[0])
        session.close()

        self.unit_cb = ttk.Combobox(top_frame, width=5,
                                    textvariable=self.unit_var,
                                    values=unit_val)
        self.unit_cb.grid(row=3, column=1, sticky=tk.W)

        if len(unit_val) != 0:
            self.unit_var.set(unit_val[0])

        self.search_btn = ttk.Button(top_frame, text="Search",
                                     image=self.img_list['search'])
        self.search_btn.grid(row=0, column=2)
        self.search_btn.config(compound=tk.LEFT)
        self.save_btn = ttk.Button(top_frame, text="Add/Save",
                                   image=self.img_list['save'])
        self.save_btn.grid(row=1, column=2)
        self.save_btn.config(command=self.saveRecord, compound=tk.LEFT)

        mat_col = ["itemcode", "description", "unit", "rate"]

        self.mat_view = ttk.Treeview(mid_frame)
        self.mat_view.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)
        self.mat_view['columns'] = mat_col

        self.mat_view.tag_configure("even", background="#ffdbca")
        self.mat_view.tag_configure("odd", background="#ffb591")

        self.mat_view.heading("#0", text="ID")
        self.mat_view.column("#0", width=50, stretch=False)

        for col in mat_col:
            self.mat_view.heading(col, text=col.title())

        self.mat_view.column("itemcode", width=120, stretch=False)
        self.mat_view.column("rate", width=75, stretch=False)
        self.mat_view.column("unit", width=75, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.vert_scroll.config(command=self.mat_view.yview)

        self.mat_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bot_frame, text="Edit",
                                   image=self.img_list['edit'])
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord, compound=tk.LEFT)
        self.delete_btn = ttk.Button(bot_frame, text="Delete",
                                     image=self.img_list['minus'])
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord, compound=tk.LEFT)
        self.export_btn = ttk.Button(bot_frame, text="Export",
                                     image=self.img_list['export'])
        self.export_btn.grid(row=0, column=2)
        self.export_btn.config(compound=tk.LEFT)
        self.close_btn = ttk.Button(bot_frame, text="Close",
                                    image=self.img_list['cancel'])
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(command=self.close, compound=tk.LEFT)

        self.updateView()
        self.code_entry.focus_set()

    def updateView(self):
        session = MainSession()
        records = session.query(Material).options(joinedload(Material.unit)).all()

        children = self.mat_view.get_children()
        if len(children) != 0:
            for child in children:
                self.mat_view.delete(child)

        counter = 0
        for record in records:
            if (counter % 2) == 0:
                self.mat_view.insert('', 'end', str(record.id),
                                     text=str(record.id), tags='even')
            else:
                self.mat_view.insert('', 'end', str(record.id),
                                     text=str(record.id), tags='odd')

            self.mat_view.set(str(record.id), 'itemcode', record.itemcode)
            self.mat_view.set(str(record.id), 'description',
                              record.description)
            self.mat_view.set(str(record.id), 'unit', record.unit.code)
            self.mat_view.set(str(record.id), 'rate',
                              "{:0.2f}".format(record.rate))
            counter += 1
        session.close()

    def saveRecord(self):
        session = MainSession()
        if self.rec_id is None:
            record = session.query(Unit).filter(
                Unit.code == self.unit_var.get()).one()
            itemcode = self.code_entry.get()
            description = self.descp_entry.get()
            rate = float(self.rate_entry.get())
            unit_id = record.id

            data = {"itemcode": itemcode, 'description': description,
                    'rate': rate, 'unit_id': unit_id}

            item = Material(**data)
            session.add(item)
            session.commit()

            self.code_entry.delete('0', tk.END)
            self.descp_entry.delete('0', tk.END)
            self.rate_entry.delete('0', tk.END)
            self.code_entry.focus_set()
        else:
            record = session.query(Material).filter(
                Material.id == self.rec_id).one()
            unit_rec = session.query(Unit).filter(
                Unit.code == self.unit_var.get()).one()
            itemcode = self.code_entry.get()
            description = self.descp_entry.get()
            rate = float(self.rate_entry.get())
            unit_id = unit_rec.id

            record.itemcode = itemcode
            record.description = description
            record.rate = rate
            record.unit_id = unit_id
            session.commit()

            self.code_entry.delete('0', tk.END)
            self.descp_entry.delete('0', tk.END)
            self.rate_entry.delete('0', tk.END)
            self.code_entry.focus_set()

        session.close()

        self.updateView()

    def deleteRecord(self):
        pass

    def editRecord(self):
        record = self.mat_view.focus()
        if record != '':
            self.rec_id = int(record)
            self.code_entry.delete('0', tk.END)
            self.descp_entry.delete('0', tk.END)
            self.rate_entry.delete('0', tk.END)

            item = self.mat_view.item(record, 'values')
            itemcode = item[0]
            description = item[1]
            unit = item[2]
            rate = item[3]

            self.code_entry.insert(tk.END, itemcode)
            self.descp_entry.insert(tk.END, description)
            self.unit_var.set(unit)
            self.rate_entry.insert(tk.END, rate)

    def close(self):
        print("Closing Material Window")
        self.master.destroy()


class EquipmentWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = None
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.title("Equipments")
        # self.master.geometry("640x480+20+20")
        self.img_list = image_list()
        if os.name == 'nt':
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill=tk.X)
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bot_frame = tk.Frame(self)
        bot_frame.pack(fill=tk.X)

        code_lbl = tk.Label(top_frame, text="Item Code:")
        code_lbl.grid(row=0, column=0, sticky=tk.W)
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=0, column=1, sticky=tk.W)
        descp_lbl = tk.Label(top_frame, text="Description:")
        descp_lbl.grid(row=1, column=0, sticky=tk.W)
        self.descp_entry = tk.Entry(top_frame, width=40)
        self.descp_entry.grid(row=1, column=1, sticky=tk.W)
        rate_lbl = tk.Label(top_frame, text="Rate:")
        rate_lbl.grid(row=2, column=0, sticky=tk.W)
        self.rate_entry = tk.Entry(top_frame, width=10)
        self.rate_entry.grid(row=2, column=1, sticky=tk.W)
        unit_lbl = tk.Label(top_frame, text="Unit:")
        unit_lbl.grid(row=3, column=0, sticky=tk.W)

        self.unit_var = tk.StringVar()
        unit_val = []

        session = MainSession()
        record = session.query(Unit.code).all()
        for val in record:
            unit_val.append(val[0])
        session.close()

        self.unit_cb = ttk.Combobox(top_frame, width=5,
                                    textvariable=self.unit_var,
                                    values=unit_val)
        self.unit_cb.grid(row=3, column=1, sticky=tk.W)

        if len(unit_val) != 0:
            self.unit_var.set(unit_val[0])

        self.search_btn = ttk.Button(top_frame, text="Search",
                                     image=self.img_list['search'])
        self.search_btn.grid(row=0, column=2)
        self.search_btn.config(compound=tk.LEFT)
        self.save_btn = ttk.Button(top_frame, text="Add/Save",
                                   image=self.img_list['save'])
        self.save_btn.grid(row=1, column=2)
        self.save_btn.config(command=self.saveRecord, compound=tk.LEFT)

        equip_col = ["code", "description", "unit", "rate"]

        self.equip_view = ttk.Treeview(mid_frame)
        self.equip_view.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)
        self.equip_view['columns'] = equip_col

        self.equip_view.tag_configure("even", background="#ffdbca")
        self.equip_view.tag_configure("odd", background="#ffb591")

        self.equip_view.heading("#0", text="ID")
        self.equip_view.column("#0", width=50, stretch=False)

        for col in equip_col:
            self.equip_view.heading(col, text=col.title())

        self.equip_view.column("code", width=120, stretch=False)
        self.equip_view.column("rate", width=75, stretch=False)
        self.equip_view.column("unit", width=75, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.vert_scroll.config(command=self.equip_view.yview)

        self.equip_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bot_frame, text="Edit",
                                   image=self.img_list['edit'])
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord, compound=tk.LEFT)
        self.delete_btn = ttk.Button(bot_frame, text="Delete",
                                     image=self.img_list['minus'])
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord, compound=tk.LEFT)
        self.export_btn = ttk.Button(bot_frame, text="Export",
                                     image=self.img_list['export'])
        self.export_btn.grid(row=0, column=2)
        self.export_btn.config(compound=tk.LEFT)
        self.close_btn = ttk.Button(bot_frame, text="Close",
                                    image=self.img_list['cancel'])
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(command=self.close, compound=tk.LEFT)

        self.updateView()
        self.code_entry.focus_set()

    def updateView(self):
        session = MainSession()
        records = session.query(Equipment).options(joinedload(Equipment.unit)).all()

        children = self.equip_view.get_children()
        if len(children) != 0:
            for child in children:
                self.equip_view.delete(child)

        counter = 0
        for record in records:
            if (counter % 2) == 0:
                self.equip_view.insert('', 'end', str(record.id),
                                       text=str(record.id), tags='even')
            else:
                self.equip_view.insert('', 'end', str(record.id),
                                       text=str(record.id), tags='odd')

            self.equip_view.set(str(record.id), 'code', record.code)
            self.equip_view.set(str(record.id), 'description',
                                record.description)
            self.equip_view.set(str(record.id), 'unit', record.unit.code)
            self.equip_view.set(str(record.id), 'rate',
                                "{:0.2f}".format(record.rate))
            counter += 1

        session.close()

    def saveRecord(self):
        session = MainSession()
        if self.rec_id is None:
            record = session.query(Unit).filter(
                Unit.code == self.unit_var.get()).one()
            code = self.code_entry.get()
            description = self.descp_entry.get()
            rate = float(self.rate_entry.get())
            unit_id = record.id

            data = {"code": code, 'description': description,
                    'rate': rate, 'unit_id': unit_id}

            item = Equipment(**data)
            session.add(item)
            session.commit()

            self.code_entry.delete('0', tk.END)
            self.descp_entry.delete('0', tk.END)
            self.rate_entry.delete('0', tk.END)
            self.code_entry.focus_set()
        else:
            record = session.query(Equipment).filter(
                Equipment.id == self.rec_id).one()
            unit_rec = session.query(Unit).filter(
                Unit.code == self.unit_var.get()).one()
            code = self.code_entry.get()
            description = self.descp_entry.get()
            rate = float(self.rate_entry.get())
            unit_id = unit_rec.id

            record.code = code
            record.description = description
            record.rate = rate
            record.unit_id = unit_id
            session.commit()

            self.code_entry.delete('0', tk.END)
            self.descp_entry.delete('0', tk.END)
            self.rate_entry.delete('0', tk.END)
            self.code_entry.focus_set()

        session.close()

        self.updateView()

    def deleteRecord(self):
        pass

    def editRecord(self):
        record = self.equip_view.focus()
        if record != '':
            self.rec_id = int(record)
            self.code_entry.delete('0', tk.END)
            self.descp_entry.delete('0', tk.END)
            self.rate_entry.delete('0', tk.END)

            item = self.equip_view.item(record, 'values')
            code = item[0]
            description = item[1]
            unit = item[2]
            rate = item[3]

            self.code_entry.insert(tk.END, code)
            self.descp_entry.insert(tk.END, description)
            self.unit_var.set(unit)
            self.rate_entry.insert(tk.END, rate)

    def close(self):
        print("Closing Equipment Window")
        self.master.destroy()


class LaborWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = None
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.title("Labors")
        # self.master.geometry("640x480+20+20")
        self.img_list = image_list()
        if os.name == 'nt':
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill=tk.X)
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bot_frame = tk.Frame(self)
        bot_frame.pack(fill=tk.X)

        code_lbl = tk.Label(top_frame, text="Item Code:")
        code_lbl.grid(row=0, column=0, sticky=tk.W)
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=0, column=1, sticky=tk.W)
        descp_lbl = tk.Label(top_frame, text="Description:")
        descp_lbl.grid(row=1, column=0, sticky=tk.W)
        self.descp_entry = tk.Entry(top_frame, width=40)
        self.descp_entry.grid(row=1, column=1, sticky=tk.W)
        rate_lbl = tk.Label(top_frame, text="Rate:")
        rate_lbl.grid(row=2, column=0, sticky=tk.W)
        self.rate_entry = tk.Entry(top_frame, width=10)
        self.rate_entry.grid(row=2, column=1, sticky=tk.W)
        unit_lbl = tk.Label(top_frame, text="Unit:")
        unit_lbl.grid(row=3, column=0, sticky=tk.W)

        self.unit_var = tk.StringVar()
        unit_val = []

        session = MainSession()
        record = session.query(Unit.code).all()
        for val in record:
            unit_val.append(val[0])
        session.close()

        self.unit_cb = ttk.Combobox(top_frame, width=5,
                                    textvariable=self.unit_var,
                                    values=unit_val)
        self.unit_cb.grid(row=3, column=1, sticky=tk.W)

        if len(unit_val) != 0:
            self.unit_var.set(unit_val[0])

        self.search_btn = ttk.Button(top_frame, text="Search",
                                     image=self.img_list['search'])
        self.search_btn.grid(row=0, column=2)
        self.search_btn.config(compound=tk.LEFT)
        self.save_btn = ttk.Button(top_frame, text="Add/Save",
                                   image=self.img_list['save'])
        self.save_btn.grid(row=1, column=2)
        self.save_btn.config(command=self.saveRecord, compound=tk.LEFT)

        lab_col = ["code", "description", "unit", "rate"]

        self.labor_view = ttk.Treeview(mid_frame)
        self.labor_view.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)
        self.labor_view['columns'] = lab_col

        self.labor_view.tag_configure("even", background="#ffdbca")
        self.labor_view.tag_configure("odd", background="#ffb591")

        self.labor_view.heading("#0", text="ID")
        self.labor_view.column("#0", width=50, stretch=False)

        for col in lab_col:
            self.labor_view.heading(col, text=col.title())

        self.labor_view.column("code", width=120, stretch=False)
        self.labor_view.column("rate", width=75, stretch=False)
        self.labor_view.column("unit", width=75, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.vert_scroll.config(command=self.labor_view.yview)

        self.labor_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bot_frame, text="Edit",
                                   image=self.img_list['edit'])
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord, compound=tk.LEFT)
        self.delete_btn = ttk.Button(bot_frame, text="Delete",
                                     image=self.img_list['minus'])
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord, compound=tk.LEFT)
        self.export_btn = ttk.Button(bot_frame, text="Export",
                                     image=self.img_list['export'])
        self.export_btn.grid(row=0, column=2)
        self.export_btn.config(compound=tk.LEFT)
        self.close_btn = ttk.Button(bot_frame, text="Close",
                                    command=self.close)
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(image=self.img_list['cancel'], compound=tk.LEFT)

        self.updateView()
        self.code_entry.focus_set()

    def updateView(self):
        session = MainSession()
        records = session.query(Labor).options(joinedload(Labor.unit)).all()

        children = self.labor_view.get_children()
        if len(children) != 0:
            for child in children:
                self.labor_view.delete(child)

        counter = 0
        for record in records:
            if (counter % 2) == 0:
                self.labor_view.insert('', 'end', str(record.id),
                                       text=str(record.id), tags='even')
            else:
                self.labor_view.insert('', 'end', str(record.id),
                                       text=str(record.id), tags='odd')

            self.labor_view.set(str(record.id), 'code', record.code)
            self.labor_view.set(str(record.id), 'description',
                                record.description)
            self.labor_view.set(str(record.id), 'unit', record.unit.code)
            self.labor_view.set(str(record.id), 'rate',
                                "{:0.2f}".format(record.rate))
            counter += 1

        session.close()

    def saveRecord(self):
        session = MainSession()
        if self.rec_id is None:
            record = session.query(Unit).filter(
                Unit.code == self.unit_var.get()).one()
            code = self.code_entry.get()
            description = self.descp_entry.get()
            rate = float(self.rate_entry.get())
            unit_id = record.id

            data = {"code": code, 'description': description,
                    'rate': rate, 'unit_id': unit_id}

            item = Labor(**data)
            session.add(item)
            session.commit()

            self.code_entry.delete('0', tk.END)
            self.descp_entry.delete('0', tk.END)
            self.rate_entry.delete('0', tk.END)
            self.code_entry.focus_set()
        else:
            record = session.query(Labor).filter(
                Labor.id == self.rec_id).one()
            unit_rec = session.query(Unit).filter(
                Unit.code == self.unit_var.get()).one()
            code = self.code_entry.get()
            description = self.descp_entry.get()
            rate = float(self.rate_entry.get())
            unit_id = unit_rec.id

            record.code = code
            record.description = description
            record.rate = rate
            record.unit_id = unit_id
            session.commit()

            self.code_entry.delete('0', tk.END)
            self.descp_entry.delete('0', tk.END)
            self.rate_entry.delete('0', tk.END)
            self.code_entry.focus_set()
        session.close()

        self.updateView()

    def deleteRecord(self):
        pass

    def editRecord(self):
        record = self.labor_view.focus()
        if record != '':
            self.rec_id = int(record)
            self.code_entry.delete('0', tk.END)
            self.descp_entry.delete('0', tk.END)
            self.rate_entry.delete('0', tk.END)

            item = self.labor_view.item(record, 'values')
            code = item[0]
            description = item[1]
            unit = item[2]
            rate = item[3]

            self.code_entry.insert(tk.END, code)
            self.descp_entry.insert(tk.END, description)
            self.unit_var.set(unit)
            self.rate_entry.insert(tk.END, rate)

    def close(self):
        print("Closing Labor Window")
        self.master.destroy()


class SubContractorWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.rec_id = None
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        # self.master.geometry("+20+20")
        self.master.title("SubContractor")
        self.img_list = image_list()
        if os.name == 'nt':
            self.master.iconbitmap('berderose.ico')
        self.setup_ui()
        self.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        code_lbl = tk.Label(top_frame, text="Code:")
        code_lbl.grid(row=0, column=0, sticky=tk.W)
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=0, column=1, sticky=tk.W)
        name_lbl = tk.Label(top_frame, text="Name:")
        name_lbl.grid(row=1, column=0, sticky=tk.W)
        self.name_entry = tk.Entry(top_frame, width=30)
        self.name_entry.grid(row=1, column=1, sticky="w")
        email_lbl = tk.Label(top_frame, text="E-mail:")
        email_lbl.grid(row=2, column=0, sticky="w")
        self.email_entry = tk.Entry(top_frame, width=50)
        self.email_entry.grid(row=2, column=1, sticky="w")

        tel_lbl = tk.Label(top_frame, text="Telephone:")
        tel_lbl.grid(row=0, column=2)
        self.tel_entry = tk.Entry(top_frame)
        self.tel_entry.grid(row=0, column=3, sticky="w")
        fax_lbl = tk.Label(top_frame, text="Fax:")
        fax_lbl.grid(row=1, column=2)
        self.fax_entry = tk.Entry(top_frame)
        self.fax_entry.grid(row=1, column=3, sticky="w")

        addr_lbl = tk.Label(top_frame, text="Address:")
        addr_lbl.grid(row=3, column=0, sticky="w")
        self.addr_text = tk.Text(top_frame, height=5, width=30)
        self.addr_text.grid(row=4, column=0, columnspan=2,
                            sticky="nesw", pady=5)

        self.search_btn = ttk.Button(top_frame, text="Search")
        self.search_btn.grid(row=0, column=4)
        self.search_btn.config(image=self.img_list['search'], compound=tk.LEFT)
        self.add_btn = ttk.Button(top_frame, text="Add/Save")
        self.add_btn.grid(row=1, column=4)
        self.add_btn.config(command=self.addRecord, image=self.img_list['save'],
                            compound=tk.LEFT)

        self.subcon_view = ttk.Treeview(mid_frame)
        self.subcon_view.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

        self.subcon_view.tag_configure("even", background="#ffdbca")
        self.subcon_view.tag_configure("odd", background="#ffb591")

        self.subcon_view['columns'] = ("code", "name", "telephone",
                                       "fax", "email", "address")
        self.subcon_view.heading("#0", text="ID")
        self.subcon_view.heading("code", text="Code")
        self.subcon_view.heading("name", text="Name")
        self.subcon_view.heading("telephone", text="Telephone")
        self.subcon_view.heading("fax", text="Fax")
        self.subcon_view.heading("email", text="Email")
        self.subcon_view.heading("address", text="Address")

        self.subcon_view.column("#0", width=75, stretch=False)
        self.subcon_view.column("code", width=75, stretch=False)
        self.subcon_view.column("telephone", width=100, stretch=False)
        self.subcon_view.column("fax", width=100, stretch=False)
        self.subcon_view.column("email", width=120, stretch=False)
        self.subcon_view.column("name", width=150, stretch=False)

        self.vert_scroll = tk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill="y")

        self.vert_scroll.config(command=self.subcon_view.yview)
        self.subcon_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list['edit'],
                                   compound=tk.LEFT)
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list['minus'],
                                     compound=tk.LEFT)
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list['export'],
                                     compound=tk.LEFT)
        self.export_btn.grid(row=0, column=2)
        self.close_btn = ttk.Button(bottom_frame, text="Cancel",
                                    image=self.img_list['cancel'],
                                    compound=tk.LEFT)
        self.close_btn.grid(row=0, column=3)

        self.close_btn.config(command=self.close)

        self.updateView()
        self.code_entry.focus_set()

    def updateView(self):
        session = MainSession()
        records = session.query(SubContractor).all()
        if len(records) != 0:
            children = self.subcon_view.get_children()
            if len(children) != 0:
                for child in children:
                    self.subcon_view.delete(child)

            counter = 1
            for record in records:
                if counter % 2 == 0:
                    self.subcon_view.insert("", "end", str(record.id),
                                            text=str(record.id), tags="even")
                else:
                    self.subcon_view.insert("", "end", str(record.id),
                                            text=str(record.id), tags="odd")
                self.subcon_view.set(str(record.id), "code", str(record.code))
                self.subcon_view.set(str(record.id), "name", str(record.name))
                self.subcon_view.set(str(record.id), "telephone",
                                     str(record.telephone))
                self.subcon_view.set(str(record.id), "fax", str(record.fax))
                self.subcon_view.set(str(record.id), "email",
                                     str(record.email))
                self.subcon_view.set(str(record.id), "address",
                                     str(record.address))
                counter += 1
        session.close()

    def addRecord(self):
        session = MainSession()
        code = self.code_entry.get()
        name = self.name_entry.get()
        telephone = self.tel_entry.get()
        fax = self.fax_entry.get()
        email = self.email_entry.get()
        address = self.addr_text.get("1.0", tk.END)
        if self.rec_id is None:
            data = {'code': code,
                    'name': name,
                    'address': address,
                    'telephone': telephone,
                    'fax': fax,
                    'email': email}
            record = SubContractor(**data)
            session.add(record)
            session.commit()
        else:
            record = session.query(SubContractor).filter(
                SubContractor.id == self.rec_id).one()
            record.code = code
            record.name = name
            record.address = address
            record.telephone = telephone
            record.fax = fax
            record.email = email
            session.commit()
            self.rec_id = None

        session.close()

        self.name_entry.delete("0", tk.END)
        self.code_entry.delete("0", tk.END)
        self.tel_entry.delete("0", tk.END)
        self.fax_entry.delete("0", tk.END)
        self.email_entry.delete("0", tk.END)
        self.addr_text.delete("1.0", tk.END)

        self.updateView()
        self.code_entry.focus_set()

    def editRecord(self):
        record = self.subcon_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.name_entry.delete("0", tk.END)
            self.tel_entry.delete("0", tk.END)
            self.fax_entry.delete("0", tk.END)
            self.email_entry.delete("0", tk.END)
            self.addr_text.delete("1.0", tk.END)

            item = self.subcon_view.item(record, "values")
            code = item[0]
            name = item[1]
            telephone = item[2]
            fax = item[3]
            email = item[4]
            address = item[5]

            self.code_entry.insert(tk.END, code)
            self.name_entry.insert(tk.END, name)
            self.tel_entry.insert(tk.END, telephone)
            self.fax_entry.insert(tk.END, fax)
            self.email_entry.insert(tk.END, email)
            self.addr_text.insert('1.0', address)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

    def deleteRecord(self):
        session = MainSession()
        record = self.subcon_view.focus()
        if record != "":
            message = "Are you sure you want to\n delete the record?"
            check = mb.showwarning("Delete Record?", message, parent=self)
            if check:
                rec_id = int(record)
                record = session.query(SubContractor).filter(
                    SubContractor.id == rec_id).one()
                session.delete(record)
                session.commit()
        session.close()
        self.updateView()

    def searchRecord(self):
        pass

    def exportRecord(self):
        pass

    def close(self):
        print("Closing SubContractor Window.")
        self.master.destroy()


class AboutWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.title("About")
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.img_list = image_list()
        self.icon_only = image_list((50,50))
        if os.name == 'nt':
            self.master.iconbitmap('berderose.ico')
        self.master.geometry("+20+20")
        self.setup_ui()
        self.master.grab_set()

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill=tk.X)
        mid_frame = tk.Frame(self)
        mid_frame.pack(fill=tk.X)
        bot_frame = tk.LabelFrame(self, text="MIT License", labelanchor=tk.N)
        bot_frame.pack(expand=True, fill=tk.BOTH)

        logo_lbl = tk.Label(top_frame, image=self.icon_only['berderose'])
        logo_lbl.pack(side=tk.LEFT)

        text = " ".join(["Berderose Estimation Software", "v"+__version__])

        app_name_lbl = tk.Label(top_frame, text=text,
                                font="Times 11 bold", fg='green')
        app_name_lbl.pack(side=tk.LEFT)

        author_lbl = tk.Label(mid_frame, text="Author:")
        author_lbl.grid(row=0, column=0, sticky=tk.W)

        author_name_lbl = tk.Label(mid_frame, text=__author__)
        author_name_lbl.grid(row=0, column=1, sticky=tk.W)

        web_lbl = tk.Label(mid_frame, text="Home Page:")
        web_lbl.grid(row=1, column=0, sticky=tk.W)

        web_name_lbl = tk.Label(mid_frame, text=__web__, fg="blue")
        web_name_lbl.grid(row=1, column=1, sticky=tk.W)

        icons8_lbl = tk.Label(mid_frame, text="Free Icons:")
        icons8_lbl.grid(row=2, column=0, sticky=tk.W)

        icons8_name_lbl = tk.Label(mid_frame, text="https://icons8.com", fg='blue')
        icons8_name_lbl.grid(row=2, column=1, sticky=tk.W)

        self.sc_text = ScrolledText(bot_frame, bd=1)
        self.sc_text.pack(expand=True, fill=tk.BOTH, padx=20, pady=20)

        with open("LICENSE.txt", 'r') as lic_file:
            data = lic_file.read()

        self.sc_text.insert('1.0', data)
        self.sc_text.config(state="disabled")

        self.okay_btn = ttk.Button(self, text="Okay", command=self.close,
                                   image=self.img_list['okay'], compound=tk.LEFT)
        self.okay_btn.pack()

    def close(self):
        print("Closing About Window")
        self.master.destroy()


class MaterialTransactionWindow(ttk.Frame):

    def __init__(self, master=None, prj_id=None, **kws):
        ttk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.title("Material Transaction")
        self.prj_id = prj_id
        self.rec_id = None
        self.img_list = image_list()
        self.setup_ui()
        if os.name == "nt":
            self.master.state("zoomed")
            self.master.iconbitmap("berderose.ico")
        elif os.name == "posix":
            self.master.attributes("-zoomed", True)
        else:
            pass

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        date_lbl = tk.Label(top_frame, text="Date:")
        date_lbl.grid(row=0, column=0, sticky=tk.W)
        self.date_entry = tk.Entry(top_frame)
        self.date_entry.grid(row=0, column=1, sticky=tk.W)
        date_now = datetime.today().strftime("%d/%m/%Y")
        self.date_entry.insert(tk.END, date_now)
        self.add_date_btn = tk.Button(top_frame, image=self.img_list['plus'])
        self.add_date_btn.grid(row=0, column=2)
        self.add_date_btn.config(command=self.addDateWindow)

        code_lbl = tk.Label(top_frame, text="Itemcode:")
        code_lbl.grid(row=1, column=0, sticky=tk.W)
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=1, column=1, sticky=tk.W)
        self.view_item_btn = tk.Button(top_frame, image=self.img_list['material'])
        self.view_item_btn.grid(row=1, column=2)
        self.view_item_btn.config(command=self.viewItemWindow)

        prj_lbl = tk.Label(top_frame, text="Project:")
        prj_lbl.grid(row=1, column=3, sticky=tk.W)
        self.prj_entry = tk.Entry(top_frame, width=40)
        self.prj_entry.grid(row=1, column=4, sticky=tk.W)

        subcat_lbl = tk.Label(top_frame, text="Sub-Category:")
        subcat_lbl.grid(row=0, column=3, sticky=tk.W)
        self.subcat_var = tk.StringVar()
        subcat_col = []
        self.subcat_cb = ttk.Combobox(top_frame, textvariable=self.subcat_var)
        self.subcat_cb.grid(row=0, column=4, sticky=tk.W)
        self.subcat_cb.config(width=30)

        session = MainSession()
        subcat_column = session.query(SubCategory.description).all()
        for col in subcat_column:
            subcat_col.append(col[0])
        self.subcat_cb.config(values=subcat_col)
        if len(subcat_col) != 0:
            self.subcat_var.set(subcat_col[0])

        prj = session.query(Project).filter(Project.id == self.prj_id).one()
        self.prj_entry.delete('0', tk.END)
        self.prj_entry.insert(tk.END, prj.name)

        qty_lbl = tk.Label(top_frame, text="Quantity:")
        qty_lbl.grid(row=3, column=0, sticky=tk.W)
        self.qty_entry = tk.Entry(top_frame, width=10)
        self.qty_entry.grid(row=3, column=1, sticky=tk.W)

        rate_lbl = tk.Label(top_frame, text="Rate:")
        rate_lbl.grid(row=4, column=0, sticky=tk.W)
        self.rate_entry = tk.Entry(top_frame, width=10)
        self.rate_entry.grid(row=4, column=1, sticky=tk.W)

        self.save_btn = ttk.Button(top_frame, text="Save", image=self.img_list['save'])
        self.save_btn.grid(row=0, column=5)
        self.save_btn.config(compound=tk.LEFT, command=self.saveRecord)

        self.mat_trans_view = ttk.Treeview(mid_frame)
        self.mat_trans_view.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)

        column = ["date", "itemcode", "description", "unit",
                  "rate", "quantity", "total"]
        self.mat_trans_view['columns'] = column

        self.mat_trans_view.heading('#0', text='ID')
        self.mat_trans_view.column('#0', width=75, stretch=False)
        for col in column:
            self.mat_trans_view.heading(col, text=col.title())

            if (col == "date") or (col == "itemcode") or (col == 'quantity'):
                self.mat_trans_view.column(col, width=120, stretch=False)
            elif (col == 'unit') or (col == 'rate'):
                self.mat_trans_view.column(col, width=100, stretch=False)
        self.mat_trans_view.tag_configure("even", background="#ffdbca")
        self.mat_trans_view.tag_configure("odd", background="#ffb591")

        self.vert_scroll = ttk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.vert_scroll.config(command=self.mat_trans_view.yview)

        self.mat_trans_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list['edit'],
                                   compound=tk.LEFT)
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list['minus'],
                                     compound=tk.LEFT)
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list['export'],
                                     compound=tk.LEFT)
        self.export_btn.grid(row=0, column=2)
        self.close_btn = ttk.Button(bottom_frame, text="Cancel",
                                    image=self.img_list['cancel'],
                                    compound=tk.LEFT)
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(command=self.close)

        session.close()

        self.updateView()

    def viewItemWindow(self):
        mat_tp = tk.Toplevel(self)
        win = MaterialWindow(mat_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def addDateWindow(self):
        date_now = datetime.today()
        cal_tp = tk.Toplevel(self)
        win = CalendarWidget(date_now.year, date_now.month, cal_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        self.wait_window(win)
        if len(win.date) != 0:
            self.date_entry.delete('0', tk.END)
            self.date_entry.insert(tk.END, win.date)

    def updateView(self):
        session = MainSession()
        records = session.query(MatTrans).options(joinedload(MatTrans.material)).filter(MatTrans.project_id == self.prj_id).all()
        children = self.mat_trans_view.get_children()
        if len(children) != 0:
            for child in children:
                self.mat_trans_view.delete(child)

        if len(records) != 0:
            counter = 0
            for record in records:
                if (counter % 2) == 0:
                    self.mat_trans_view.insert("", "end", str(record.id),
                                               text=str(record.id),
                                               tags='even')
                else:
                    self.mat_trans_view.insert("", "end", str(record.id),
                                               text=str(record.id),
                                               tags='odd')
                self.mat_trans_view.set(str(record.id), "date",
                                        record.date.strftime("%d/%m/%Y"))
                self.mat_trans_view.set(str(record.id), "itemcode",
                                        record.material.itemcode)
                self.mat_trans_view.set(str(record.id), "description",
                                        record.material.description)
                unit_rec = session.query(Unit).filter(Unit.id == record.material.unit_id).first()
                self.mat_trans_view.set(str(record.id), "unit",
                                        unit_rec.code)
                self.mat_trans_view.set(str(record.id), "rate",
                                        "{:0.2f}".format(record.rate))
                self.mat_trans_view.set(str(record.id), "quantity",
                                        "{:0.2f}".format(record.quantity))
                total = record.rate * record.quantity
                self.mat_trans_view.set(str(record.id), "total",
                                        "{:0.2f}".format(total))
                counter += 1

        session.close()

    def saveRecord(self):
        session = MainSession()
        date = datetime.strptime(self.date_entry.get(), "%d/%m/%Y")
        mat_rec = session.query(Material).filter(
            Material.itemcode == self.code_entry.get()).one()
        mat_id = mat_rec.id
        project_id = self.prj_id
        subcat_rec = session.query(SubCategory).filter(
            SubCategory.description == self.subcat_var.get()).one()
        subcat_id = subcat_rec.id
        quantity = float(self.qty_entry.get())
        rate = float(self.rate_entry.get())
        if self.rec_id is None:
            data = {'date': date, 'material_id': mat_id,
                    'project_id': project_id, 'subcategory_id': subcat_id,
                    'quantity': quantity, 'rate': rate}
            item = MatTrans(**data)
            session.add(item)
            session.commit()

            self.code_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)
        else:
            record = session.query(MatTrans).filter(
                MatTrans.id == self.rec_id).one()
            record.date = date
            record.material_id = mat_id
            record.project_id = self.prj_id
            record.subcategory_id = subcat_id
            record.quantity = quantity
            record.rate = rate
            session.commit()
            self.rec_id = None

            self.code_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)
        session.close()

        self.code_entry.focus_set()
        self.updateView()

    def deleteRecord(self):
        session = MainSession()
        item = self.mat_trans_view.focus()
        if item != '':
            rec_id = int(item)
            record = session.query(MatTrans).filter(
                MatTrans.id == rec_id).one()
            session.delete(record)
            session.commit()
        session.close()
        self.updateView()

    def editRecord(self):
        session = MainSession()
        record = self.mat_trans_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.code_entry.delete("0", tk.END)
            self.date_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)

            item = self.mat_trans_view.item(record, "values")
            date = item[0]
            code = item[1]
            quantity = item[5]
            rate = item[4]
            subcat_rec = session.query(MatTrans).options(
                joinedload(MatTrans.subcategory)).filter(MatTrans.id == self.rec_id).one()
            subcat_description = subcat_rec.subcategory.description

            self.date_entry.insert(tk.END, date)
            self.code_entry.insert(tk.END, code)
            self.subcat_var.set(subcat_description)
            self.qty_entry.insert(tk.END, quantity)
            self.rate_entry.insert(tk.END, rate)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

        session.close()

    def exportRecord(self):
        pass

    def close(self):
        print("Material Transaction Window Closing.")
        self.master.destroy()


class EquipmentTransactionWindow(tk.Frame):

    def __init__(self, master=None, prj_id=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.title("Material Transaction")
        self.prj_id = prj_id
        self.rec_id = None
        self.img_list = image_list()
        self.setup_ui()
        if os.name == "nt":
            self.master.state("zoomed")
            self.master.iconbitmap("berderose.ico")
        elif os.name == "posix":
            self.master.attributes("-zoomed", True)
        else:
            pass

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        date_lbl = tk.Label(top_frame, text="Date:")
        date_lbl.grid(row=0, column=0, sticky=tk.W)
        self.date_entry = tk.Entry(top_frame)
        self.date_entry.grid(row=0, column=1, sticky=tk.W)
        date_now = datetime.today().strftime("%d/%m/%Y")
        self.date_entry.insert(tk.END, date_now)
        self.add_date_btn = tk.Button(top_frame, image=self.img_list['plus'])
        self.add_date_btn.grid(row=0, column=2)
        self.add_date_btn.config(command=self.addDateWindow)

        code_lbl = tk.Label(top_frame, text="Equipment Code:")
        code_lbl.grid(row=1, column=0, sticky=tk.W)
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=1, column=1, sticky=tk.W)
        self.view_equip_btn = tk.Button(top_frame, image=self.img_list['equipment'])
        self.view_equip_btn.grid(row=1, column=2)
        self.view_equip_btn.config(command=self.viewEquipmentWindow)

        prj_lbl = tk.Label(top_frame, text="Project:")
        prj_lbl.grid(row=1, column=3, sticky=tk.W)
        self.prj_entry = tk.Entry(top_frame, width=40)
        self.prj_entry.grid(row=1, column=4, sticky=tk.W)

        subcat_lbl = tk.Label(top_frame, text="Sub-Category:")
        subcat_lbl.grid(row=0, column=3, sticky=tk.W)
        self.subcat_var = tk.StringVar()
        subcat_col = []
        self.subcat_cb = ttk.Combobox(top_frame, textvariable=self.subcat_var)
        self.subcat_cb.grid(row=0, column=4, sticky=tk.W)
        self.subcat_cb.config(width=30)
        session = MainSession()
        subcat_column = session.query(SubCategory.description).all()
        for col in subcat_column:
            subcat_col.append(col[0])
        self.subcat_cb.config(values=subcat_col)
        if len(subcat_col) != 0:
            self.subcat_var.set(subcat_col[0])

        prj = session.query(Project).filter(Project.id == self.prj_id).one()
        self.prj_entry.delete('0', tk.END)
        self.prj_entry.insert(tk.END, prj.name)

        qty_lbl = tk.Label(top_frame, text="Quantity:")
        qty_lbl.grid(row=3, column=0, sticky=tk.W)
        self.qty_entry = tk.Entry(top_frame, width=10)
        self.qty_entry.grid(row=3, column=1, sticky=tk.W)

        rate_lbl = tk.Label(top_frame, text="Rate:")
        rate_lbl.grid(row=4, column=0, sticky=tk.W)
        self.rate_entry = tk.Entry(top_frame, width=10)
        self.rate_entry.grid(row=4, column=1, sticky=tk.W)

        self.save_btn = ttk.Button(top_frame, text="Save", image=self.img_list['save'])
        self.save_btn.grid(row=0, column=5)
        self.save_btn.config(compound=tk.LEFT, command=self.saveRecord)

        self.equip_trans_view = ttk.Treeview(mid_frame)
        self.equip_trans_view.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)

        column = ["date", "code", "description", "unit",
                  "rate", "quantity", "total"]
        self.equip_trans_view['columns'] = column

        self.equip_trans_view.heading('#0', text='ID')
        self.equip_trans_view.column('#0', width=75, stretch=False)
        for col in column:
            self.equip_trans_view.heading(col, text=col.title())

            if (col == "date") or (col == "code") or (col == 'quantity'):
                self.equip_trans_view.column(col, width=120, stretch=False)
            elif (col == 'unit') or (col == 'rate'):
                self.equip_trans_view.column(col, width=100, stretch=False)
        self.equip_trans_view.tag_configure("even", background="#ffdbca")
        self.equip_trans_view.tag_configure("odd", background="#ffb591")

        self.vert_scroll = ttk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.vert_scroll.config(command=self.equip_trans_view.yview)

        self.equip_trans_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list['edit'],
                                   compound=tk.LEFT)
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list['minus'],
                                     compound=tk.LEFT)
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list['export'],
                                     compound=tk.LEFT)
        self.export_btn.grid(row=0, column=2)
        self.close_btn = ttk.Button(bottom_frame, text="Cancel",
                                    image=self.img_list['cancel'],
                                    compound=tk.LEFT)
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(command=self.close)

        session.close()

        self.updateView()

    def viewEquipmentWindow(self):
        equip_tp = tk.Toplevel(self)
        win = EquipmentWindow(equip_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def addDateWindow(self):
        date_now = datetime.today()
        cal_tp = tk.Toplevel(self)
        win = CalendarWidget(date_now.year, date_now.month, cal_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        self.wait_window(cal_tp)
        if len(win.date) != 0:
            self.date_entry.delete('0', tk.END)
            self.date_entry.insert(tk.END, win.date)

    def updateView(self):
        session = MainSession()
        records = session.query(EquiTrans).options(joinedload(EquiTrans.equipment)).filter(EquiTrans.project_id == self.prj_id).all()
        children = self.equip_trans_view.get_children()
        if len(children) != 0:
            for child in children:
                self.equip_trans_view.delete(child)

        if len(records) != 0:
            counter = 0
            for record in records:
                if (counter % 2) == 0:
                    self.equip_trans_view.insert("", "end",
                                                 str(record.id),
                                                 text=str(record.id),
                                                 tags='even')
                else:
                    self.equip_trans_view.insert("", "end", str(record.id),
                                                 text=str(record.id),
                                                 tags='odd')
                self.equip_trans_view.set(str(record.id), "date",
                                          record.date.strftime("%d/%m/%Y"))
                self.equip_trans_view.set(str(record.id), "code",
                                          record.equipment.code)
                self.equip_trans_view.set(str(record.id), "description",
                                          record.equipment.description)
                unit_rec = session.query(Unit).filter(Unit.id == record.equipment.unit_id).first()
                self.equip_trans_view.set(str(record.id), "unit",
                                          unit_rec.code)
                self.equip_trans_view.set(str(record.id), "rate",
                                          "{:0.2f}".format(record.rate))
                self.equip_trans_view.set(str(record.id), "quantity",
                                          "{:0.2f}".format(record.quantity))
                total = record.rate * record.quantity
                self.equip_trans_view.set(str(record.id), "total",
                                          "{:0.2f}".format(total))
                counter += 1

        session.close()

    def saveRecord(self):
        session = MainSession()
        date = datetime.strptime(self.date_entry.get(), "%d/%m/%Y")
        equip_rec = session.query(Equipment).filter(
            Equipment.code == self.code_entry.get()).one()
        equipment_id = equip_rec.id
        project_id = self.prj_id
        subcat_rec = session.query(SubCategory).filter(
            SubCategory.description == self.subcat_var.get()).one()
        subcat_id = subcat_rec.id
        quantity = float(self.qty_entry.get())
        rate = float(self.rate_entry.get())
        if self.rec_id is None:
            data = {'date': date, 'equipment_id': equipment_id,
                    'project_id': project_id, 'subcategory_id': subcat_id,
                    'quantity': quantity, 'rate': rate}
            item = EquiTrans(**data)
            session.add(item)
            session.commit()

            self.code_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)
        else:
            record = session.query(EquiTrans).filter(
                EquiTrans.id == self.rec_id).one()
            record.date = date
            record.equipment_id = equipment_id
            record.project_id = self.prj_id
            record.subcategory_id = subcat_id
            record.quantity = quantity
            record.rate = rate
            session.commit()
            self.rec_id = None

            self.code_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)
        session.close()
        self.code_entry.focus_set()
        self.updateView()

    def deleteRecord(self):
        session = MainSession()
        item = self.equip_trans_view.focus()
        if item != '':
            rec_id = int(item)
            record = session.query(EquiTrans).filter(
                EquiTrans.id == rec_id).one()
            message = "Do you really want to\n delete the record?"
            check = mb.showwarning("Delete Record?", message, parent=self)
            if check:
                session.delete(record)
                session.commit()
        session.close()
        self.updateView()

    def editRecord(self):
        session = MainSession()
        record = self.equip_trans_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.code_entry.delete("0", tk.END)
            self.date_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)

            item = self.equip_trans_view.item(record, "values")
            date = item[0]
            code = item[1]
            quantity = item[5]
            rate = item[4]
            subcat_rec = session.query(EquiTrans).options(
                joinedload(EquiTrans.subcategory)).filter(
                    EquiTrans.id == self.rec_id).one()
            subcat_description = subcat_rec.subcategory.description

            self.date_entry.insert(tk.END, date)
            self.code_entry.insert(tk.END, code)
            self.subcat_var.set(subcat_description)
            self.qty_entry.insert(tk.END, quantity)
            self.rate_entry.insert(tk.END, rate)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

        session.close()

    def exportRecord(self):
        pass

    def close(self):
        print("Equipment Transaction Window Closing.")
        self.master.destroy()


class LaborTransactionWindow(tk.Frame):

    def __init__(self, master=None, prj_id=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.title("Labor Transaction")
        self.img_list = image_list()
        self.prj_id = prj_id
        self.rec_id = None
        self.setup_ui()
        if os.name == "nt":
            self.master.state("zoomed")
            self.master.iconbitmap("berderose.ico")
        elif os.name == "posix":
            self.master.attributes("-zoomed", True)
        else:
            pass

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        date_lbl = tk.Label(top_frame, text="Date:")
        date_lbl.grid(row=0, column=0, sticky=tk.W)
        self.date_entry = tk.Entry(top_frame)
        self.date_entry.grid(row=0, column=1, sticky=tk.W)
        date_now = datetime.today().strftime("%d/%m/%Y")
        self.date_entry.insert(tk.END, date_now)
        self.add_date_btn = tk.Button(top_frame, image=self.img_list['plus'])
        self.add_date_btn.grid(row=0, column=2)
        self.add_date_btn.config(command=self.addDateWindow)

        code_lbl = tk.Label(top_frame, text="Labor Code:")
        code_lbl.grid(row=1, column=0, sticky=tk.W)
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=1, column=1, sticky=tk.W)
        self.view_lab_btn = tk.Button(top_frame, image=self.img_list['labors'])
        self.view_lab_btn.grid(row=1, column=2)
        self.view_lab_btn.config(command=self.viewLaborWindow)

        prj_lbl = tk.Label(top_frame, text="Project:")
        prj_lbl.grid(row=1, column=3, sticky=tk.W)
        self.prj_entry = tk.Entry(top_frame, width=40)
        self.prj_entry.grid(row=1, column=4, sticky=tk.W)

        subcat_lbl = tk.Label(top_frame, text="Sub-Category:")
        subcat_lbl.grid(row=0, column=3, sticky=tk.W)
        self.subcat_var = tk.StringVar()
        subcat_col = []
        self.subcat_cb = ttk.Combobox(top_frame, textvariable=self.subcat_var)
        self.subcat_cb.grid(row=0, column=4, sticky=tk.W)
        self.subcat_cb.config(width=30)
        session = MainSession()
        subcat_column = session.query(SubCategory.description).all()
        for col in subcat_column:
            subcat_col.append(col[0])
        self.subcat_cb.config(values=subcat_col)
        if len(subcat_col) != 0:
            self.subcat_var.set(subcat_col[0])

        prj = session.query(Project).filter(Project.id == self.prj_id).one()
        self.prj_entry.delete('0', tk.END)
        self.prj_entry.insert(tk.END, prj.name)

        qty_lbl = tk.Label(top_frame, text="Quantity:")
        qty_lbl.grid(row=3, column=0, sticky=tk.W)
        self.qty_entry = tk.Entry(top_frame, width=10)
        self.qty_entry.grid(row=3, column=1, sticky=tk.W)

        rate_lbl = tk.Label(top_frame, text="Rate:")
        rate_lbl.grid(row=4, column=0, sticky=tk.W)
        self.rate_entry = tk.Entry(top_frame, width=10)
        self.rate_entry.grid(row=4, column=1, sticky=tk.W)

        self.save_btn = ttk.Button(top_frame, text="Save", image=self.img_list['save'])
        self.save_btn.grid(row=0, column=5)
        self.save_btn.config(compound=tk.LEFT, command=self.saveRecord)

        self.labor_trans_view = ttk.Treeview(mid_frame)
        self.labor_trans_view.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)

        column = ["date", "code", "description", "unit",
                  "rate", "quantity", "total"]
        self.labor_trans_view['columns'] = column

        self.labor_trans_view.heading('#0', text='ID')
        self.labor_trans_view.column('#0', width=75, stretch=False)
        for col in column:
            self.labor_trans_view.heading(col, text=col.title())

            if (col == "date") or (col == "code") or (col == 'quantity'):
                self.labor_trans_view.column(col, width=120, stretch=False)
            elif (col == 'unit') or (col == 'rate'):
                self.labor_trans_view.column(col, width=100, stretch=False)
        self.labor_trans_view.tag_configure("even", background="#ffdbca")
        self.labor_trans_view.tag_configure("odd", background="#ffb591")

        self.vert_scroll = ttk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.vert_scroll.config(command=self.labor_trans_view.yview)

        self.labor_trans_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list['edit'],
                                   compound=tk.LEFT)
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list['minus'],
                                     compound=tk.LEFT)
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list['export'],
                                     compound=tk.LEFT)
        self.export_btn.grid(row=0, column=2)
        self.close_btn = ttk.Button(bottom_frame, text="Cancel",
                                    image=self.img_list['cancel'],
                                    compound=tk.LEFT)
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(command=self.close)

        session.close()

        self.updateView()

    def viewLaborWindow(self):
        labor_tp = tk.Toplevel(self)
        win = LaborWindow(labor_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def addDateWindow(self):
        date_now = datetime.today()
        cal_tp = tk.Toplevel(self)
        win = CalendarWidget(date_now.year, date_now.month, cal_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        self.wait_window(cal_tp)
        if len(win.date) != 0:
            self.date_entry.delete('0', tk.END)
            self.date_entry.insert(tk.END, win.date)

    def updateView(self):
        session = MainSession()
        records = session.query(LabTrans).options(joinedload(LabTrans.labor)).filter(LabTrans.project_id == self.prj_id).all()
        children = self.labor_trans_view.get_children()
        if len(children) != 0:
            for child in children:
                self.labor_trans_view.delete(child)

        if len(records) != 0:
            counter = 0
            for record in records:
                if (counter % 2) == 0:
                    self.labor_trans_view.insert("", "end",
                                                 str(record.id),
                                                 text=str(record.id),
                                                 tags='even')
                else:
                    self.labor_trans_view.insert("", "end", str(record.id),
                                                 text=str(record.id),
                                                 tags='odd')
                self.labor_trans_view.set(str(record.id), "date",
                                          record.date.strftime("%d/%m/%Y"))
                self.labor_trans_view.set(str(record.id), "code",
                                          record.labor.code)
                self.labor_trans_view.set(str(record.id), "description",
                                          record.labor.description)
                unit_rec = session.query(Unit).filter(Unit.id == record.labor.unit_id).first()
                self.labor_trans_view.set(str(record.id), "unit",
                                          unit_rec.code)
                self.labor_trans_view.set(str(record.id), "rate",
                                          "{:0.2f}".format(record.rate))
                self.labor_trans_view.set(str(record.id), "quantity",
                                          "{:0.2f}".format(record.quantity))
                total = record.rate * record.quantity
                self.labor_trans_view.set(str(record.id), "total",
                                          "{:0.2f}".format(total))
                counter += 1
        session.close()

    def saveRecord(self):
        session = MainSession()
        date = datetime.strptime(self.date_entry.get(), "%d/%m/%Y")
        labor_rec = session.query(Labor).filter(
            Labor.code == self.code_entry.get()).one()
        labor_id = labor_rec.id
        project_id = self.prj_id
        subcat_rec = session.query(SubCategory).filter(
            SubCategory.description == self.subcat_var.get()).one()
        subcat_id = subcat_rec.id
        quantity = float(self.qty_entry.get())
        rate = float(self.rate_entry.get())
        if self.rec_id is None:
            data = {'date': date, 'labor_id': labor_id,
                    'project_id': project_id, 'subcategory_id': subcat_id,
                    'quantity': quantity, 'rate': rate}
            item = LabTrans(**data)
            session.add(item)
            session.commit()

            self.code_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)
        else:
            record = session.query(LabTrans).filter(
                LabTrans.id == self.rec_id).one()
            record.date = date
            record.labor_id = labor_id
            record.project_id = self.prj_id
            record.subcategory_id = subcat_id
            record.quantity = quantity
            record.rate = rate
            session.commit()
            self.rec_id = None

            self.code_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)
        session.close()
        self.code_entry.focus_set()
        self.updateView()

    def deleteRecord(self):
        session = MainSession()
        item = self.labor_trans_view.focus()
        if item != '':
            rec_id = int(item)
            record = session.query(LabTrans).filter(
                LabTrans.id == rec_id).one()
            message = "Do you really want to\n delete the record?"
            check = mb.showwarning("Delete Record?", message, parent=self)
            if check:
                session.delete(record)
                session.commit()
        session.close()
        self.updateView()

    def editRecord(self):
        session = MainSession()
        record = self.labor_trans_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.code_entry.delete("0", tk.END)
            self.date_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)

            item = self.labor_trans_view.item(record, "values")
            date = item[0]
            code = item[1]
            quantity = item[5]
            rate = item[4]
            subcat_rec = session.query(LabTrans).options(
                joinedload(LabTrans.subcategory)).filter(
                    LabTrans.id == self.rec_id).one()
            subcat_description = subcat_rec.subcategory.description

            self.date_entry.insert(tk.END, date)
            self.code_entry.insert(tk.END, code)
            self.subcat_var.set(subcat_description)
            self.qty_entry.insert(tk.END, quantity)
            self.rate_entry.insert(tk.END, rate)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

        session.close()

    def exportRecord(self):
        pass

    def close(self):
        print("Labor Transaction Window Closing.")
        self.master.destroy()


class SubConTransactionWindow(tk.Frame):

    def __init__(self, master=None, prj_id=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.title("Sub-Contractor Transaction")
        self.prj_id = prj_id
        self.rec_id = None
        self.img_list = image_list()
        self.setup_ui()
        if os.name == "nt":
            self.master.state("zoomed")
            self.master.iconbitmap("berderose.ico")
        elif os.name == "posix":
            self.master.attributes("-zoomed", True)
        else:
            pass

    def setup_ui(self):
        top_frame = tk.Frame(self)
        top_frame.pack(fill="x")
        mid_frame = tk.Frame(self)
        mid_frame.pack(expand=True, fill=tk.BOTH)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(fill="x")

        date_lbl = tk.Label(top_frame, text="Date:")
        date_lbl.grid(row=0, column=0, sticky=tk.W)
        self.date_entry = tk.Entry(top_frame)
        self.date_entry.grid(row=0, column=1, sticky=tk.W)
        date_now = datetime.today().strftime("%d/%m/%Y")
        self.date_entry.insert(tk.END, date_now)
        self.add_date_btn = tk.Button(top_frame, image=self.img_list['plus'])
        self.add_date_btn.grid(row=0, column=2)
        self.add_date_btn.config(command=self.addDateWindow)

        code_lbl = tk.Label(top_frame, text="Sub-Contractor:")
        code_lbl.grid(row=1, column=0, sticky=tk.W)
        self.code_entry = tk.Entry(top_frame)
        self.code_entry.grid(row=1, column=1, sticky=tk.W)
        descp_lbl = tk.Label(top_frame, text="Description:")
        descp_lbl.grid(row=2, column=0, sticky=tk.W)
        self.descp_entry = tk.Entry(top_frame, width=60)
        self.descp_entry.grid(row=2, column=1, columnspan=5, sticky=tk.W)
        self.view_subcon_btn = tk.Button(top_frame, image=self.img_list['contractor'])
        self.view_subcon_btn.grid(row=1, column=2)
        self.view_subcon_btn.config(command=self.viewSubConWindow)

        prj_lbl = tk.Label(top_frame, text="Project:")
        prj_lbl.grid(row=1, column=3, sticky=tk.W)
        self.prj_entry = tk.Entry(top_frame, width=40)
        self.prj_entry.grid(row=1, column=4, sticky=tk.W)

        subcat_lbl = tk.Label(top_frame, text="Sub-Category:")
        subcat_lbl.grid(row=0, column=3, sticky=tk.W)
        self.subcat_var = tk.StringVar()
        subcat_col = []
        self.subcat_cb = ttk.Combobox(top_frame, textvariable=self.subcat_var)
        self.subcat_cb.grid(row=0, column=4, sticky=tk.W)
        self.subcat_cb.config(width=30)
        session = MainSession()
        subcat_column = session.query(SubCategory.description).all()
        for col in subcat_column:
            subcat_col.append(col[0])
        self.subcat_cb.config(values=subcat_col)
        if len(subcat_col) != 0:
            self.subcat_var.set(subcat_col[0])

        prj = session.query(Project).filter(Project.id == self.prj_id).one()
        self.prj_entry.delete('0', tk.END)
        self.prj_entry.insert(tk.END, prj.name)

        qty_lbl = tk.Label(top_frame, text="Quantity:")
        qty_lbl.grid(row=3, column=0, sticky=tk.W)
        self.qty_entry = tk.Entry(top_frame, width=10)
        self.qty_entry.grid(row=3, column=1, sticky=tk.W)

        rate_lbl = tk.Label(top_frame, text="Rate:")
        rate_lbl.grid(row=4, column=0, sticky=tk.W)
        self.rate_entry = tk.Entry(top_frame, width=10)
        self.rate_entry.grid(row=4, column=1, sticky=tk.W)
        unit_lbl = tk.Label(top_frame, text="Unit:")
        unit_lbl.grid(row=5, column=0, sticky=tk.W)

        self.unit_var = tk.StringVar()
        unit_col = []

        unit_column = session.query(Unit.code).all()
        for col in unit_column:
            unit_col.append(col[0])

        self.unit_cb = ttk.Combobox(top_frame, width=10,
                                    textvariable=self.unit_var)
        self.unit_cb.grid(row=5, column=1, sticky=tk.W)

        self.unit_cb.config(values=unit_col)
        if len(unit_col) != 0:
            self.unit_var.set(unit_col[0])

        self.save_btn = ttk.Button(top_frame, text="Save", image=self.img_list['save'])
        self.save_btn.grid(row=0, column=5)
        self.save_btn.config(compound=tk.LEFT, command=self.saveRecord)

        self.subcon_trans_view = ttk.Treeview(mid_frame)
        self.subcon_trans_view.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)

        column = ["date", "code", "description", "unit",
                  "rate", "quantity", "total"]
        self.subcon_trans_view['columns'] = column

        self.subcon_trans_view.heading('#0', text='ID')
        self.subcon_trans_view.column('#0', width=75, stretch=False)
        for col in column:
            self.subcon_trans_view.heading(col, text=col.title())

            if (col == "date") or (col == "code") or (col == 'quantity'):
                self.subcon_trans_view.column(col, width=120, stretch=False)
            elif (col == 'unit') or (col == 'rate'):
                self.subcon_trans_view.column(col, width=100, stretch=False)
        self.subcon_trans_view.tag_configure("even", background="#ffdbca")
        self.subcon_trans_view.tag_configure("odd", background="#ffb591")

        self.vert_scroll = ttk.Scrollbar(mid_frame, orient=tk.VERTICAL)
        self.vert_scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.vert_scroll.config(command=self.subcon_trans_view.yview)

        self.subcon_trans_view['yscrollcommand'] = self.vert_scroll.set

        self.edit_btn = ttk.Button(bottom_frame, text="Edit",
                                   image=self.img_list['edit'],
                                   compound=tk.LEFT)
        self.edit_btn.grid(row=0, column=0)
        self.edit_btn.config(command=self.editRecord)
        self.delete_btn = ttk.Button(bottom_frame, text="Delete",
                                     image=self.img_list['minus'],
                                     compound=tk.LEFT)
        self.delete_btn.grid(row=0, column=1)
        self.delete_btn.config(command=self.deleteRecord)
        self.export_btn = ttk.Button(bottom_frame, text="Export",
                                     image=self.img_list['export'],
                                     compound=tk.LEFT)
        self.export_btn.grid(row=0, column=2)
        self.close_btn = ttk.Button(bottom_frame, text="Cancel",
                                    image=self.img_list['cancel'],
                                    compound=tk.LEFT)
        self.close_btn.grid(row=0, column=3)
        self.close_btn.config(command=self.close)

        session.close()

        self.updateView()

    def viewSubConWindow(self):
        subcon_tp = tk.Toplevel(self)
        win = SubContractorWindow(subcon_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        win.focus_set()

    def addDateWindow(self):
        date_now = datetime.today()
        cal_tp = tk.Toplevel(self)
        win = CalendarWidget(date_now.year, date_now.month, cal_tp)
        win.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        self.wait_window(cal_tp)
        if len(win.date) != 0:
            self.date_entry.delete('0', tk.END)
            self.date_entry.insert(tk.END, win.date)

    def updateView(self):
        session = MainSession()
        records = session.query(SubConTrans).options(
            joinedload(SubConTrans.subcontractor), joinedload(SubConTrans.unit)).filter(SubConTrans.project_id == self.prj_id).all()
        children = self.subcon_trans_view.get_children()
        if len(children) != 0:
            for child in children:
                self.subcon_trans_view.delete(child)

        if len(records) != 0:
            counter = 0
            for record in records:
                if (counter % 2) == 0:
                    self.subcon_trans_view.insert("", "end",
                                                  str(record.id),
                                                  text=str(record.id),
                                                  tags='even')
                else:
                    self.subcon_trans_view.insert("", "end", str(record.id),
                                                  text=str(record.id),
                                                  tags='odd')
                self.subcon_trans_view.set(str(record.id), "date",
                                           record.date.strftime("%d/%m/%Y"))
                self.subcon_trans_view.set(str(record.id), "code",
                                           record.subcontractor.code)
                self.subcon_trans_view.set(str(record.id), "description",
                                           record.description)
                self.subcon_trans_view.set(str(record.id), "unit",
                                           record.unit.code)
                self.subcon_trans_view.set(str(record.id), "rate",
                                           "{:0.2f}".format(record.rate))
                self.subcon_trans_view.set(str(record.id), "quantity",
                                           "{:0.2f}".format(record.quantity))
                total = record.rate * record.quantity
                self.subcon_trans_view.set(str(record.id), "total",
                                           "{:0.2f}".format(total))
                counter += 1
        session.close()

    def saveRecord(self):
        session = MainSession()
        description = self.descp_entry.get()
        date = datetime.strptime(self.date_entry.get(), "%d/%m/%Y")
        subcon_rec = session.query(SubContractor).filter(
            SubContractor.code == self.code_entry.get()).one()
        subcontractor_id = subcon_rec.id
        unit_rec = session.query(Unit).filter(
            Unit.code == self.unit_var.get()).one()
        unit_id = unit_rec.id
        project_id = self.prj_id
        subcat_rec = session.query(SubCategory).filter(
            SubCategory.description == self.subcat_var.get()).one()
        subcat_id = subcat_rec.id
        quantity = float(self.qty_entry.get())
        rate = float(self.rate_entry.get())
        if self.rec_id is None:
            data = {'date': date, 'description': description,
                    'subcontractor_id': subcontractor_id,
                    'project_id': project_id,
                    'subcategory_id': subcat_id, 'unit_id': unit_id,
                    'quantity': quantity, 'rate': rate}
            item = SubConTrans(**data)
            session.add(item)
            session.commit()

            self.code_entry.delete("0", tk.END)
            self.descp_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)
        else:
            record = session.query(SubConTrans).filter(
                SubConTrans.id == self.rec_id).one()
            record.date = date
            record.subcontractor_id = subcontractor_id
            record.description = description
            record.project_id = self.prj_id
            record.subcategory_id = subcat_id
            record.unit_id = unit_id
            record.quantity = quantity
            record.rate = rate
            session.commit()
            self.rec_id = None

            self.code_entry.delete("0", tk.END)
            self.descp_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)
        session.close()
        self.code_entry.focus_set()
        self.updateView()

    def deleteRecord(self):
        session = MainSession()
        item = self.subcon_trans_view.focus()
        if item != '':
            rec_id = int(item)
            record = session.query(SubConTrans).filter(
                SubConTrans.id == rec_id).one()
            message = "Do you really want to\n delete the record?"
            check = mb.showwarning("Delete Record?", message, parent=self)
            if check:
                session.delete(record)
                session.commit()
        session.close()
        self.updateView()

    def editRecord(self):
        session = MainSession()
        record = self.subcon_trans_view.focus()
        if record != "":
            self.rec_id = int(record)
            self.code_entry.delete("0", tk.END)
            self.descp_entry.delete("0", tk.END)
            self.date_entry.delete("0", tk.END)
            self.qty_entry.delete("0", tk.END)
            self.rate_entry.delete("0", tk.END)

            item = self.subcon_trans_view.item(record, "values")
            date = item[0]
            code = item[1]
            description = item[2]
            unit = item[3]
            quantity = item[5]
            rate = item[4]
            subcat_rec = session.query(SubConTrans).options(
                joinedload(SubConTrans.subcategory)).filter(
                    SubConTrans.id == self.rec_id).one()
            subcat_description = subcat_rec.subcategory.description

            self.date_entry.insert(tk.END, date)
            self.code_entry.insert(tk.END, code)
            self.descp_entry.insert(tk.END, description)
            self.subcat_var.set(subcat_description)
            self.unit_var.set(unit)
            self.qty_entry.insert(tk.END, quantity)
            self.rate_entry.insert(tk.END, rate)
        else:
            mb.showwarning("No Record",
                           "Please select a record and try again.",
                           parent=self)

        session.close()

    def exportRecord(self):
        pass

    def close(self):
        print("SubCon Transaction Window Closing.")
        self.master.destroy()


class CompanyWindow(ttk.Frame):

    def __init__(self, master=None, **kws):
        ttk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.closeWindow)
        self.master.title("Company")
        self.img_list = image_list()
        if os.name == "nt":
            self.master.iconbitmap("berderose.ico")
        self.setup_ui()
        self.master.grab_set()

    def setup_ui(self):
        self.com_lbl = tk.Label(self, text="Company Details",
                                font=("Times", 20, "italic"), fg="blue")
        self.com_lbl.pack(fill=tk.X)

        container = tk.Frame(self)
        container.pack(expand=True, fill=tk.BOTH)

        name_lbl = tk.Label(container, text="Name:", anchor=tk.E)
        name_lbl.grid(row=0, column=0, sticky="we")
        address_lbl = tk.Label(container, text="Address:", anchor=tk.E)
        address_lbl.grid(row=1, column=0, sticky="we")
        tel_lbl = tk.Label(container, text="Telephone:", anchor=tk.E)
        tel_lbl.grid(row=3, column=0, sticky="we")
        fax_lbl = tk.Label(container, text="Fax:", anchor=tk.E)
        fax_lbl.grid(row=5, column=0, sticky="we")
        email_lbl = tk.Label(container, text="Email:", anchor=tk.E)
        email_lbl.grid(row=4, column=0, sticky="we")

        self.name_entry = tk.Entry(container, width=40)
        self.name_entry.grid(row=0, column=1, sticky="we")
        self.address_entry = tk.Text(container, width=20, height=4,
                                     wrap=tk.WORD)
        self.address_entry.grid(row=2, column=1, sticky="we")
        self.tel_entry = tk.Entry(container)
        self.tel_entry.grid(row=3, column=1, sticky="we")
        self.fax_entry = tk.Entry(container)
        self.fax_entry.grid(row=5, column=1, sticky="we")
        self.email_entry = tk.Entry(container, width=40)
        self.email_entry.grid(row=4, column=1, sticky="we")

        self.cancel_btn = ttk.Button(self, text="Cancel",
                                     command=self.closeWindow)
        self.cancel_btn.pack(side=tk.RIGHT)
        self.save_btn = ttk.Button(self, text="Save", command=self.saveDetails)
        self.save_btn.pack(side=tk.RIGHT)

        self.company_path = os.path.join(os.getcwd(), 'config', 'company.json')

        if os.path.isfile(self.company_path):
            self.loadDetails(self.company_path)

    def loadDetails(self, path):
        with open(path, 'r') as cofile:
            details = json.load(cofile)

        self.name_entry.insert('end', details['name'])
        self.address_entry.insert('end', details['address'])
        self.tel_entry.insert('end', details['telephone'])
        self.fax_entry.insert('end', details['fax'])
        self.email_entry.insert('end', details['email'])

    def saveDetails(self):
        name = self.name_entry.get()
        address = self.address_entry.get('1.0', 'end')
        telephone = self.tel_entry.get()
        fax = self.fax_entry.get()
        email = self.email_entry.get()

        details = {'name': name, 'address': address, 'telephone': telephone,
                   'fax': fax, 'email': email}

        with open(self.company_path, 'w') as cofile:
            json.dump(details, cofile)

        self.closeWindow()

    def closeWindow(self):
        print("Company WIndow Closing")
        self.master.destroy()


class SpreadFootingWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        #self.master.geometry("480x320+20+20")
        self.master.title("Spread Footing")
        self.master.resizable(False, False)
        if os.name == 'nt':
            self.master.iconbitmap("berderose.ico")
        self.setup_ui()
        self.grab_set()
        self.focus_set()

    def setup_ui(self):
        self.config(padx=10, pady=10)

        style = ttk.Style()
        style.configure("CB.TCombobox", foreground="blue")

        unit_system_frame = tk.LabelFrame(self, text="System Unit",
                                          padx=10, pady=10)
        unit_system_frame.pack(fill=tk.X)

        unit_system_lbl = tk.Label(unit_system_frame, text="System Unit",
                                   fg='blue', font="Times 12 bold")

        unit_system_frame.config(labelwidget=unit_system_lbl)

        self.unitvar_system = tk.StringVar()
        self.unitvar_system.set("FPS")

        self.unit_system_cb = ttk.Combobox(unit_system_frame, values=("MKS", "FPS"),
                                           textvariable=self.unitvar_system,
                                           style="CB.TCombobox")
        self.unit_system_cb.grid(row=0, column=0)
        self.unit_system_cb.bind("<<ComboboxSelected>>", self.setUnitEvent)

        foot_qty_frame = tk.LabelFrame(self, text="No. of Footings",
                                       padx=10, pady=10)
        foot_qty_frame.pack(fill=tk.X)

        foot_qty_lbl = tk.Label(foot_qty_frame, text="No. of Footings",
                                font="Times 12 bold", fg="blue")
        foot_qty_frame.config(labelwidget=foot_qty_lbl)

        self.footing_sbox = tk.Spinbox(foot_qty_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")
        self.footing_sbox.grid(row=0, column=0)

        self.foot_qty_unit = tk.Label(foot_qty_frame, text="ea",
                                 font="Times 11 bold", fg="blue",
                                 anchor=tk.W)
        self.foot_qty_unit.grid(row=0, column=1)

        foot_length_frame = tk.LabelFrame(self, text="Length",
                                       padx=10, pady=10)
        foot_length_frame.pack(fill=tk.X)

        foot_length_lbl = tk.Label(foot_length_frame, text="Length",
                                   font="Times 12 bold", fg="blue")
        foot_length_frame.config(labelwidget=foot_length_lbl)

        self.length_sbox1 = tk.Spinbox(foot_length_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.length_sbox1.grid(row=0, column=0)

        self.length_unit1 = tk.Label(foot_length_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.length_unit1.grid(row=0, column=1)

        self.length_sbox2 = tk.Spinbox(foot_length_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.length_sbox2.grid(row=0, column=2)

        self.length_unit2 = tk.Label(foot_length_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.length_unit2.grid(row=0, column=3)

        foot_width_frame = tk.LabelFrame(self, text="Width",
                                       padx=10, pady=10)
        foot_width_frame.pack(fill=tk.X)

        foot_width_lbl = tk.Label(foot_width_frame, text="Width",
                                   font="Times 12 bold", fg="blue")
        foot_width_frame.config(labelwidget=foot_width_lbl)

        self.width_sbox1 = tk.Spinbox(foot_width_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.width_sbox1.grid(row=0, column=0)

        self.width_unit1 = tk.Label(foot_width_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.width_unit1.grid(row=0, column=1)

        self.width_sbox2 = tk.Spinbox(foot_width_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.width_sbox2.grid(row=0, column=2)

        self.width_unit2 = tk.Label(foot_width_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.width_unit2.grid(row=0, column=3)

        foot_thick_frame = tk.LabelFrame(self, text="Thickness",
                                       padx=10, pady=10)
        foot_thick_frame.pack(fill=tk.X)

        foot_thick_lbl = tk.Label(foot_thick_frame, text="Thickness",
                                   font="Times 12 bold", fg="blue")
        foot_thick_frame.config(labelwidget=foot_thick_lbl)

        self.thick_sbox1 = tk.Spinbox(foot_thick_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.thick_sbox1.grid(row=0, column=0)

        self.thick_unit1 = tk.Label(foot_thick_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.thick_unit1.grid(row=0, column=1)

        self.thick_sbox2 = tk.Spinbox(foot_thick_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.thick_sbox2.grid(row=0, column=2)

        self.thick_unit2 = tk.Label(foot_thick_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.thick_unit2.grid(row=0, column=3)

        waste_frame = tk.LabelFrame(self, text="Concrete Waste",
                                       padx=10, pady=10)
        waste_frame.pack(fill=tk.X)

        waste_lbl = tk.Label(waste_frame, text="Concrete Waste",
                                   font="Times 12 bold", fg="blue")
        waste_frame.config(labelwidget=waste_lbl)

        self.waste_sbox = tk.Spinbox(waste_frame, increment=0.50,
                                     from_=0.00, to=100.00, fg="blue",
                                     font="Times 12 normal",
                                     format="%.2f")

        self.waste_sbox.grid(row=0, column=0)

        waste_unit = tk.Label(waste_frame, text="%",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        waste_unit.grid(row=0, column=1)

        result_frame = tk.LabelFrame(self, text="Result",
                                     padx=10, pady=10)
        result_frame.pack(fill=tk.X)

        result_lbl = tk.Label(result_frame, text="Result",
                              font="Times 12 bold", fg="blue")
        result_frame.config(labelwidget=result_lbl)

        forms_lbl = tk.Label(result_frame, text="Forms: ", fg='blue',
                             font="Times 11 bold")
        forms_lbl.grid(row=0, column=0)

        self.forms_entry = tk.Entry(result_frame, font="Times 12 normal",
                                    fg='blue')
        self.forms_entry.grid(row=0, column=1)

        self.unitvar = tk.StringVar()

        forms_unit = tk.Label(result_frame, textvariable=self.unitvar,
                              width=6, anchor=tk.W, fg='blue',
                              font="Times 12 bold")
        forms_unit.grid(row=0, column=2)

        volume_lbl = tk.Label(result_frame, text="Volume: ", fg='blue',
                              font="Times 11 bold")
        volume_lbl.grid(row=1, column=0)
        
        self.volume_entry = tk.Entry(result_frame, font="Times 12 normal",
                                     fg='blue')
        self.volume_entry.grid(row=1, column=1)
        
        self.volume_lbl1 = tk.Label(result_frame, text="cyd", fg="blue",
                               font="Times 12 bold", width=6, anchor=tk.W)
        self.volume_lbl1.grid(row=1, column=2)

        self.calc_btn = tk.Button(self, text="Calculate", width=11,
                                  command=self.calculate, fg='white',
                                  font="Times 12 bold", bg='green')
        self.calc_btn.pack(padx=5, pady=5)

        # Hackfix for an error in Spinbox widget
        # displaying the max value instead of minimum value
        
        for child in self.winfo_children():
            for widget in child.winfo_children():
                if widget.winfo_class() == "Spinbox":
                    widget.delete('0', 'end')
                    widget.insert('end', "0")
                    widget.config(width=10)

        self.setUnit(self.unitvar_system.get())

    def setUnit(self, unit_system):
        if unit_system == "MKS":
            self.unitvar.set("mtr")
            self.length_unit1.config(text="mtr")
            self.length_unit2.config(text="cm")
            self.width_unit1.config(text="mtr")
            self.width_unit2.config(text="cm")
            self.thick_unit1.config(text="mtr")
            self.thick_unit2.config(text="cm")
            self.volume_lbl1.config(text="cum")
        else:
            self.unitvar.set("ft")
            self.length_unit1.config(text="ft")
            self.length_unit2.config(text="in")
            self.width_unit1.config(text="ft")
            self.width_unit2.config(text="in")
            self.thick_unit1.config(text="ft")
            self.thick_unit2.config(text="in")
            self.volume_lbl1.config(text="cyd")

    def setUnitEvent(self, event):
        unit = self.unitvar_system.get()
        self.setUnit(unit)

    def calculate(self):
        unit = self.unitvar_system.get()
        if unit == "FPS":
            length_ft = float(self.length_sbox1.get())
            length_in = float(self.length_sbox2.get())

            width_ft = float(self.width_sbox1.get())
            width_in = float(self.width_sbox2.get())

            thick_ft = float(self.thick_sbox1.get())
            thick_in = float(self.thick_sbox2.get())

            waste = float(self.waste_sbox.get())
            footing = float(self.footing_sbox.get())

            volume = (footing * (length_ft + (length_in/12)) *
                      (width_ft + (width_in/12)) * (thick_ft + (thick_in/12)))/27
            volume = volume * (1 + waste/100)
            forms = (footing *
                     (2 * ((length_ft + length_in/12) + (width_ft + width_in/12))))
            unit = ((thick_ft * 12) + (thick_in)) > 12

            if unit:
                self.unitvar.set("sft")
            else:
                self.unitvar.set("ft")
        else:
            length_mtr = float(self.length_sbox1.get())
            length_cm = float(self.length_sbox2.get())

            width_mtr = float(self.width_sbox1.get())
            width_cm = float(self.width_sbox2.get())

            thick_mtr = float(self.thick_sbox1.get())
            thick_cm = float(self.thick_sbox2.get())

            waste = float(self.waste_sbox.get())
            footing = float(self.footing_sbox.get())

            volume = (footing * (length_mtr + (length_cm/100)) *
                      (width_mtr + (width_cm/100)) * (thick_mtr + (thick_cm/100)))
            volume = volume * (1 + waste/100)
            forms = (footing *
                     (2 * ((length_mtr + length_cm/100) + (width_mtr + width_cm/100))))
            unit = ((thick_mtr * 100) + (thick_cm)) > 30.48

            if unit:
                self.unitvar.set("sqm")
            else:
                self.unitvar.set("mtr")

        self.volume_entry.delete('0', 'end')
        self.volume_entry.insert('end', format(volume, ".2f"))

        self.forms_entry.delete('0', 'end')
        self.forms_entry.insert('end', format(forms, ".2f"))

    def close(self):
        self.master.destroy()


class RecColumnWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        #self.master.geometry("480x320+20+20")
        self.master.title("Rectangular Column")
        self.master.resizable(False, False)
        if os.name == 'nt':
            self.master.iconbitmap("berderose.ico")
        self.setup_ui()
        self.grab_set()
        self.focus_set()

    def setup_ui(self):
        self.config(padx=10, pady=10)

        style = ttk.Style()
        style.configure("CB.TCombobox", foreground="blue")

        unit_system_frame = tk.LabelFrame(self, text="System Unit",
                                          padx=10, pady=10)
        unit_system_frame.pack(fill=tk.X)

        unit_system_lbl = tk.Label(unit_system_frame, text="System Unit",
                                   fg='blue', font="Times 12 bold")

        unit_system_frame.config(labelwidget=unit_system_lbl)

        self.unitvar_system = tk.StringVar()
        self.unitvar_system.set("FPS")

        self.unit_system_cb = ttk.Combobox(unit_system_frame, values=("MKS", "FPS"),
                                           textvariable=self.unitvar_system,
                                           style="CB.TCombobox")
        self.unit_system_cb.grid(row=0, column=0)
        self.unit_system_cb.bind("<<ComboboxSelected>>", self.setUnitEvent)

        column_qty_frame = tk.LabelFrame(self, text="No. of Column",
                                       padx=10, pady=10)
        column_qty_frame.pack(fill=tk.X)

        column_qty_lbl = tk.Label(column_qty_frame, text="No. of Column",
                                font="Times 12 bold", fg="blue")
        column_qty_frame.config(labelwidget=column_qty_lbl)

        self.column_sbox = tk.Spinbox(column_qty_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")
        self.column_sbox.grid(row=0, column=0)

        self.column_qty_unit = tk.Label(column_qty_frame, text="ea",
                                 font="Times 11 bold", fg="blue",
                                 anchor=tk.W)
        self.column_qty_unit.grid(row=0, column=1)

        column_length_frame = tk.LabelFrame(self, text="Length",
                                       padx=10, pady=10)
        column_length_frame.pack(fill=tk.X)

        column_length_lbl = tk.Label(column_length_frame, text="Length",
                                   font="Times 12 bold", fg="blue")
        column_length_frame.config(labelwidget=column_length_lbl)

        self.length_sbox1 = tk.Spinbox(column_length_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.length_sbox1.grid(row=0, column=0)

        self.length_unit1 = tk.Label(column_length_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.length_unit1.grid(row=0, column=1)

        self.length_sbox2 = tk.Spinbox(column_length_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.length_sbox2.grid(row=0, column=2)

        self.length_unit2 = tk.Label(column_length_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.length_unit2.grid(row=0, column=3)

        column_width_frame = tk.LabelFrame(self, text="Width",
                                       padx=10, pady=10)
        column_width_frame.pack(fill=tk.X)

        column_width_lbl = tk.Label(column_width_frame, text="Width",
                                   font="Times 12 bold", fg="blue")
        column_width_frame.config(labelwidget=column_width_lbl)

        self.width_sbox1 = tk.Spinbox(column_width_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.width_sbox1.grid(row=0, column=0)

        self.width_unit1 = tk.Label(column_width_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.width_unit1.grid(row=0, column=1)

        self.width_sbox2 = tk.Spinbox(column_width_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.width_sbox2.grid(row=0, column=2)

        self.width_unit2 = tk.Label(column_width_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.width_unit2.grid(row=0, column=3)

        column_thick_frame = tk.LabelFrame(self, text="Thickness",
                                       padx=10, pady=10)
        column_thick_frame.pack(fill=tk.X)

        column_thick_lbl = tk.Label(column_thick_frame, text="Height",
                                   font="Times 12 bold", fg="blue")
        column_thick_frame.config(labelwidget=column_thick_lbl)

        self.thick_sbox1 = tk.Spinbox(column_thick_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.thick_sbox1.grid(row=0, column=0)

        self.thick_unit1 = tk.Label(column_thick_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.thick_unit1.grid(row=0, column=1)

        self.thick_sbox2 = tk.Spinbox(column_thick_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.thick_sbox2.grid(row=0, column=2)

        self.thick_unit2 = tk.Label(column_thick_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.thick_unit2.grid(row=0, column=3)

        waste_frame = tk.LabelFrame(self, text="Concrete Waste",
                                       padx=10, pady=10)
        waste_frame.pack(fill=tk.X)

        waste_lbl = tk.Label(waste_frame, text="Concrete Waste",
                                   font="Times 12 bold", fg="blue")
        waste_frame.config(labelwidget=waste_lbl)

        self.waste_sbox = tk.Spinbox(waste_frame, increment=0.50,
                                     from_=0.00, to=100.00, fg="blue",
                                     font="Times 12 normal",
                                     format="%.2f")

        self.waste_sbox.grid(row=0, column=0)

        waste_unit = tk.Label(waste_frame, text="%",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        waste_unit.grid(row=0, column=1)

        result_frame = tk.LabelFrame(self, text="Result",
                                     padx=10, pady=10)
        result_frame.pack(fill=tk.X)

        result_lbl = tk.Label(result_frame, text="Result",
                              font="Times 12 bold", fg="blue")
        result_frame.config(labelwidget=result_lbl)

        forms_lbl = tk.Label(result_frame, text="Forms: ", fg='blue',
                             font="Times 11 bold")
        forms_lbl.grid(row=0, column=0)

        self.forms_entry = tk.Entry(result_frame, font="Times 12 normal",
                                    fg='blue')
        self.forms_entry.grid(row=0, column=1)

        self.unitvar = tk.StringVar()

        forms_unit = tk.Label(result_frame, textvariable=self.unitvar,
                              width=6, anchor=tk.W, fg='blue',
                              font="Times 12 bold")
        forms_unit.grid(row=0, column=2)

        volume_lbl = tk.Label(result_frame, text="Volume: ", fg='blue',
                              font="Times 11 bold")
        volume_lbl.grid(row=1, column=0)
        
        self.volume_entry = tk.Entry(result_frame, font="Times 12 normal",
                                     fg='blue')
        self.volume_entry.grid(row=1, column=1)
        
        self.volume_lbl1 = tk.Label(result_frame, text="cyd", fg="blue",
                               font="Times 12 bold", width=6, anchor=tk.W)
        self.volume_lbl1.grid(row=1, column=2)

        self.calc_btn = tk.Button(self, text="Calculate", width=11,
                                  command=self.calculate, fg='white',
                                  font="Times 12 bold", bg='green')
        self.calc_btn.pack(padx=5, pady=5)

        # Hackfix for an error in Spinbox widget
        # displaying the max value instead of minimum value
        
        for child in self.winfo_children():
            for widget in child.winfo_children():
                if widget.winfo_class() == "Spinbox":
                    widget.delete('0', 'end')
                    widget.insert('end', "0")
                    widget.config(width=10)

        self.setUnit(self.unitvar_system.get())

    def setUnit(self, unit_system):
        if unit_system == "MKS":
            self.unitvar.set("sqm")
            self.length_unit1.config(text="mtr")
            self.length_unit2.config(text="cm")
            self.width_unit1.config(text="mtr")
            self.width_unit2.config(text="cm")
            self.thick_unit1.config(text="mtr")
            self.thick_unit2.config(text="cm")
            self.volume_lbl1.config(text="cum")
        else:
            self.unitvar.set("sft")
            self.length_unit1.config(text="ft")
            self.length_unit2.config(text="in")
            self.width_unit1.config(text="ft")
            self.width_unit2.config(text="in")
            self.thick_unit1.config(text="ft")
            self.thick_unit2.config(text="in")
            self.volume_lbl1.config(text="cyd")

    def setUnitEvent(self, event):
        unit = self.unitvar_system.get()
        self.setUnit(unit)

    def calculate(self):
        unit = self.unitvar_system.get()
        if unit == "FPS":
            length_ft = float(self.length_sbox1.get())
            length_in = float(self.length_sbox2.get())

            width_ft = float(self.width_sbox1.get())
            width_in = float(self.width_sbox2.get())

            thick_ft = float(self.thick_sbox1.get())
            thick_in = float(self.thick_sbox2.get())

            waste = float(self.waste_sbox.get())
            column = float(self.column_sbox.get())

            perimeter = length_ft + (length_in/12) + width_ft + (width_in/12)

            volume = (column * (length_ft + (length_in/12)) *
                      (width_ft + (width_in/12)) * (thick_ft + (thick_in/12)))/27
            volume = volume * (1 + waste/100)
            forms = column * perimeter * (thick_ft + (thick_in/12)) * 2
        else:
            length_mtr = float(self.length_sbox1.get())
            length_cm = float(self.length_sbox2.get())

            width_mtr = float(self.width_sbox1.get())
            width_cm = float(self.width_sbox2.get())

            thick_mtr = float(self.thick_sbox1.get())
            thick_cm = float(self.thick_sbox2.get())

            waste = float(self.waste_sbox.get())
            column = float(self.column_sbox.get())

            perimeter = length_mtr + (length_cm/100) + width_mtr + (width_cm/100)

            volume = (column * (length_mtr + (length_cm/100)) *
                      (width_mtr + (width_cm/100)) * (thick_mtr + (thick_cm/100)))
            volume = volume * (1 + waste/100)
            forms = column * perimeter * (thick_mtr + (thick_cm/100)) * 2

        self.volume_entry.delete('0', 'end')
        self.volume_entry.insert('end', format(volume, ".2f"))

        self.forms_entry.delete('0', 'end')
        self.forms_entry.insert('end', format(forms, ".2f"))

    def close(self):
        self.master.destroy()


class RoundColumnWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        #self.master.geometry("480x320+20+20")
        self.master.title("Round Column")
        self.master.resizable(False, False)
        if os.name == 'nt':
            self.master.iconbitmap("berderose.ico")
        self.setup_ui()
        self.grab_set()
        self.focus_set()

    def setup_ui(self):
        self.config(padx=10, pady=10)

        style = ttk.Style()
        style.configure("CB.TCombobox", foreground="blue")

        unit_system_frame = tk.LabelFrame(self, text="System Unit",
                                          padx=10, pady=10)
        unit_system_frame.pack(fill=tk.X)

        unit_system_lbl = tk.Label(unit_system_frame, text="System Unit",
                                   fg='blue', font="Times 12 bold")

        unit_system_frame.config(labelwidget=unit_system_lbl)

        self.unitvar_system = tk.StringVar()
        self.unitvar_system.set("FPS")

        self.unit_system_cb = ttk.Combobox(unit_system_frame, values=("MKS", "FPS"),
                                           textvariable=self.unitvar_system,
                                           style="CB.TCombobox")
        self.unit_system_cb.grid(row=0, column=0)
        self.unit_system_cb.bind("<<ComboboxSelected>>", self.setUnitEvent)

        column_qty_frame = tk.LabelFrame(self, text="No. of Column",
                                       padx=10, pady=10)
        column_qty_frame.pack(fill=tk.X)

        column_qty_lbl = tk.Label(column_qty_frame, text="No. of Column",
                                font="Times 12 bold", fg="blue")
        column_qty_frame.config(labelwidget=column_qty_lbl)

        self.column_sbox = tk.Spinbox(column_qty_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")
        self.column_sbox.grid(row=0, column=0)

        self.column_qty_unit = tk.Label(column_qty_frame, text="ea",
                                 font="Times 11 bold", fg="blue",
                                 anchor=tk.W)
        self.column_qty_unit.grid(row=0, column=1)

        column_diameter_frame = tk.LabelFrame(self, text="Diameter",
                                       padx=10, pady=10)
        column_diameter_frame.pack(fill=tk.X)

        column_diameter_lbl = tk.Label(column_diameter_frame, text="Diameter",
                                   font="Times 12 bold", fg="blue")
        column_diameter_frame.config(labelwidget=column_diameter_lbl)

        self.diameter_sbox1 = tk.Spinbox(column_diameter_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.diameter_sbox1.grid(row=0, column=0)

        self.diameter_unit1 = tk.Label(column_diameter_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.diameter_unit1.grid(row=0, column=1)

        self.diameter_sbox2 = tk.Spinbox(column_diameter_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.diameter_sbox2.grid(row=0, column=2)

        self.diameter_unit2 = tk.Label(column_diameter_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.diameter_unit2.grid(row=0, column=3)

        column_height_frame = tk.LabelFrame(self, text="Height",
                                       padx=10, pady=10)
        column_height_frame.pack(fill=tk.X)

        column_height_lbl = tk.Label(column_height_frame, text="Height",
                                   font="Times 12 bold", fg="blue")
        column_height_frame.config(labelwidget=column_height_lbl)

        self.height_sbox1 = tk.Spinbox(column_height_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.height_sbox1.grid(row=0, column=0)

        self.height_unit1 = tk.Label(column_height_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.height_unit1.grid(row=0, column=1)

        self.height_sbox2 = tk.Spinbox(column_height_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.height_sbox2.grid(row=0, column=2)

        self.height_unit2 = tk.Label(column_height_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.height_unit2.grid(row=0, column=3)

        waste_frame = tk.LabelFrame(self, text="Concrete Waste",
                                       padx=10, pady=10)
        waste_frame.pack(fill=tk.X)

        waste_lbl = tk.Label(waste_frame, text="Concrete Waste",
                                   font="Times 12 bold", fg="blue")
        waste_frame.config(labelwidget=waste_lbl)

        self.waste_sbox = tk.Spinbox(waste_frame, increment=0.50,
                                     from_=0.00, to=100.00, fg="blue",
                                     font="Times 12 normal",
                                     format="%.2f")

        self.waste_sbox.grid(row=0, column=0)

        waste_unit = tk.Label(waste_frame, text="%",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        waste_unit.grid(row=0, column=1)

        result_frame = tk.LabelFrame(self, text="Result",
                                     padx=10, pady=10)
        result_frame.pack(fill=tk.X)

        result_lbl = tk.Label(result_frame, text="Result",
                              font="Times 12 bold", fg="blue")
        result_frame.config(labelwidget=result_lbl)

        volume_lbl = tk.Label(result_frame, text="Volume: ", fg='blue',
                              font="Times 11 bold")
        volume_lbl.grid(row=0, column=0)
        
        self.volume_entry = tk.Entry(result_frame, font="Times 12 normal",
                                     fg='blue')
        self.volume_entry.grid(row=0, column=1)
        
        self.volume_lbl1 = tk.Label(result_frame, text="cyd", fg="blue",
                               font="Times 12 bold", width=6, anchor=tk.W)
        self.volume_lbl1.grid(row=0, column=2)

        self.calc_btn = tk.Button(self, text="Calculate", width=11,
                                  command=self.calculate, fg='white',
                                  font="Times 12 bold", bg='green')
        self.calc_btn.pack(padx=5, pady=5)

        # Hackfix for an error in Spinbox widget
        # displaying the max value instead of minimum value
        
        for child in self.winfo_children():
            for widget in child.winfo_children():
                if widget.winfo_class() == "Spinbox":
                    widget.delete('0', 'end')
                    widget.insert('end', "0")
                    widget.config(width=10)

        self.setUnit(self.unitvar_system.get())

    def setUnit(self, unit_system):
        if unit_system == "MKS":
            self.diameter_unit1.config(text="mtr")
            self.diameter_unit2.config(text="cm")
            self.height_unit1.config(text="mtr")
            self.height_unit2.config(text="cm")
            self.volume_lbl1.config(text="cum")
        else:
            self.diameter_unit1.config(text="ft")
            self.diameter_unit2.config(text="in")
            self.height_unit1.config(text="ft")
            self.height_unit2.config(text="in")
            self.volume_lbl1.config(text="cyd")

    def setUnitEvent(self, event):
        unit = self.unitvar_system.get()
        self.setUnit(unit)

    def calculate(self):
        unit = self.unitvar_system.get()
        if unit == "FPS":
            diameter_ft = float(self.diameter_sbox1.get())
            diameter_in = float(self.diameter_sbox2.get())
            
            height_ft = float(self.height_sbox1.get())
            height_in = float(self.height_sbox2.get())

            waste = float(self.waste_sbox.get())
            column = float(self.column_sbox.get())

            volume = (column * (math.pi * ((diameter_ft + (diameter_in/12))/2)**2 * (height_ft + (height_in/12))))/27
            volume = volume * (1 + waste/100)
        else:
            diameter_mtr = float(self.diameter_sbox1.get())
            diameter_cm = float(self.diameter_sbox2.get())

            height_mtr = float(self.height_sbox1.get())
            height_cm = float(self.height_sbox2.get())
            
            waste = float(self.waste_sbox.get())
            column = float(self.column_sbox.get())

            volume = (column * (math.pi * ((diameter_mtr + (diameter_cm/100))/2)**2 * (height_mtr + (height_cm/100))))
            volume = volume * (1 + waste/100)

        self.volume_entry.delete('0', 'end')
        self.volume_entry.insert('end', format(volume, ".2f"))

    def close(self):
        self.master.destroy()


class ConFootingWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        #self.master.geometry("480x320+20+20")
        self.master.title("Continous Footing")
        self.master.resizable(False, False)
        if os.name == 'nt':
            self.master.iconbitmap("berderose.ico")
        self.setup_ui()
        self.grab_set()
        self.focus_set()

    def setup_ui(self):
        self.config(padx=10, pady=10)

        style = ttk.Style()
        style.configure("CB.TCombobox", foreground="blue")

        unit_system_frame = tk.LabelFrame(self, text="System Unit",
                                          padx=10, pady=10)
        unit_system_frame.pack(fill=tk.X)

        unit_system_lbl = tk.Label(unit_system_frame, text="System Unit",
                                   fg='blue', font="Times 12 bold")

        unit_system_frame.config(labelwidget=unit_system_lbl)

        self.unitvar_system = tk.StringVar()
        self.unitvar_system.set("FPS")

        self.unit_system_cb = ttk.Combobox(unit_system_frame, values=("MKS", "FPS"),
                                           textvariable=self.unitvar_system,
                                           style="CB.TCombobox")
        self.unit_system_cb.grid(row=0, column=0)
        self.unit_system_cb.bind("<<ComboboxSelected>>", self.setUnitEvent)

        foot_length_frame = tk.LabelFrame(self, text="Length",
                                       padx=10, pady=10)
        foot_length_frame.pack(fill=tk.X)

        foot_length_lbl = tk.Label(foot_length_frame, text="Length",
                                   font="Times 12 bold", fg="blue")
        foot_length_frame.config(labelwidget=foot_length_lbl)

        self.length_sbox1 = tk.Spinbox(foot_length_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.length_sbox1.grid(row=0, column=0)

        self.length_unit1 = tk.Label(foot_length_frame, text="foot",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.length_unit1.grid(row=0, column=1)

        self.length_sbox2 = tk.Spinbox(foot_length_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.length_sbox2.grid(row=0, column=2)

        self.length_unit2 = tk.Label(foot_length_frame, text="inch",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.length_unit2.grid(row=0, column=3)

        foot_width_frame = tk.LabelFrame(self, text="Width",
                                       padx=10, pady=10)
        foot_width_frame.pack(fill=tk.X)

        foot_width_lbl = tk.Label(foot_width_frame, text="Width",
                                   font="Times 12 bold", fg="blue")
        foot_width_frame.config(labelwidget=foot_width_lbl)

        self.width_sbox1 = tk.Spinbox(foot_width_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.width_sbox1.grid(row=0, column=0)

        self.width_unit1 = tk.Label(foot_width_frame, text="foot",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.width_unit1.grid(row=0, column=1)

        self.width_sbox2 = tk.Spinbox(foot_width_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.width_sbox2.grid(row=0, column=2)

        self.width_unit2 = tk.Label(foot_width_frame, text="inch",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.width_unit2.grid(row=0, column=3)

        foot_thick_frame = tk.LabelFrame(self, text="Thickness",
                                       padx=10, pady=10)
        foot_thick_frame.pack(fill=tk.X)

        foot_thick_lbl = tk.Label(foot_thick_frame, text="Thickness",
                                   font="Times 12 bold", fg="blue")
        foot_thick_frame.config(labelwidget=foot_thick_lbl)

        self.thick_sbox1 = tk.Spinbox(foot_thick_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.thick_sbox1.grid(row=0, column=0)

        self.thick_unit1 = tk.Label(foot_thick_frame, text="foot",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.thick_unit1.grid(row=0, column=1)

        self.thick_sbox2 = tk.Spinbox(foot_thick_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.thick_sbox2.grid(row=0, column=2)

        self.thick_unit2 = tk.Label(foot_thick_frame, text="inch",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.thick_unit2.grid(row=0, column=3)

        waste_frame = tk.LabelFrame(self, text="Concrete Waste",
                                       padx=10, pady=10)
        waste_frame.pack(fill=tk.X)

        waste_lbl = tk.Label(waste_frame, text="Concrete Waste",
                                   font="Times 12 bold", fg="blue")
        waste_frame.config(labelwidget=waste_lbl)

        self.waste_sbox = tk.Spinbox(waste_frame, increment=0.50,
                                     from_=0.00, to=100.00, fg="blue",
                                     font="Times 12 normal",
                                     format="%.2f")

        self.waste_sbox.grid(row=0, column=0)

        waste_unit = tk.Label(waste_frame, text="%",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        waste_unit.grid(row=0, column=1)

        result_frame = tk.LabelFrame(self, text="Result",
                                     padx=10, pady=10)
        result_frame.pack(fill=tk.X)

        result_lbl = tk.Label(result_frame, text="Result",
                              font="Times 12 bold", fg="blue")
        result_frame.config(labelwidget=result_lbl)

        volume_lbl = tk.Label(result_frame, text="Volume: ", fg='blue',
                              font="Times 11 bold")
        volume_lbl.grid(row=0, column=0)
        
        self.volume_entry = tk.Entry(result_frame, font="Times 12 normal",
                                     fg='blue')
        self.volume_entry.grid(row=0, column=1)
        
        self.volume_lbl1 = tk.Label(result_frame, text="cubic yards", fg="blue",
                               font="Times 12 bold")
        self.volume_lbl1.grid(row=0, column=2)

        self.calc_btn = tk.Button(self, text="Calculate", width=11,
                                  command=self.calculate, fg='white',
                                  font="Times 12 bold", bg='green')
        self.calc_btn.pack(padx=5, pady=5)

        # Hackfix for an error in Spinbox widget
        # displaying the max value instead of minimum value
        
        for child in self.winfo_children():
            for widget in child.winfo_children():
                if widget.winfo_class() == "Spinbox":
                    widget.delete('0', 'end')
                    widget.insert('end', "0")
                    widget.config(width=10)

        self.setUnit(self.unitvar_system.get())

    def setUnit(self, unit_system):
        if unit_system == "MKS":
            self.length_unit1.config(text="mtr")
            self.length_unit2.config(text="cm")
            self.width_unit1.config(text="mtr")
            self.width_unit2.config(text="cm")
            self.thick_unit1.config(text="mtr")
            self.thick_unit2.config(text="cm")
            self.volume_lbl1.config(text="cum")
        else:
            self.length_unit1.config(text="ft")
            self.length_unit2.config(text="in")
            self.width_unit1.config(text="ft")
            self.width_unit2.config(text="in")
            self.thick_unit1.config(text="ft")
            self.thick_unit2.config(text="in")
            self.volume_lbl1.config(text="cyd")

    def setUnitEvent(self, event):
        unit = self.unitvar_system.get()
        self.setUnit(unit)

    def calculate(self):
        unit = self.unitvar_system.get()
        if unit == "FPS":
            length_ft = float(self.length_sbox1.get())
            length_in = float(self.length_sbox2.get())

            width_ft = float(self.width_sbox1.get())
            width_in = float(self.width_sbox2.get())

            thick_ft = float(self.thick_sbox1.get())
            thick_in = float(self.thick_sbox2.get())

            waste = float(self.waste_sbox.get())

            volume = ((length_ft + (length_in/12)) *
                      (width_ft + (width_in/12)) * (thick_ft + (thick_in/12)))/27
            volume = volume * (1 + waste/100)
        else:
            length_mtr = float(self.length_sbox1.get())
            length_cm = float(self.length_sbox2.get())

            width_mtr = float(self.width_sbox1.get())
            width_cm = float(self.width_sbox2.get())

            thick_mtr = float(self.thick_sbox1.get())
            thick_cm = float(self.thick_sbox2.get())

            waste = float(self.waste_sbox.get())

            volume = (length_mtr + (length_cm/100)) * (width_mtr + (width_cm/100)) * (thick_mtr + (thick_cm/100))
            volume = volume * (1 + waste/100)
            
        self.volume_entry.delete('0', 'end')
        self.volume_entry.insert('end', format(volume, ".2f"))

    def close(self):
        self.master.destroy()


class FoundationWallWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        #self.master.geometry("480x320+20+20")
        self.master.title("Foundation Wall")
        self.master.resizable(False, False)
        if os.name == 'nt':
            self.master.iconbitmap("berderose.ico")
        self.setup_ui()
        self.grab_set()
        self.focus_set()

    def setup_ui(self):
        self.config(padx=10, pady=10)

        style = ttk.Style()
        style.configure("CB.TCombobox", foreground="blue")

        unit_system_frame = tk.LabelFrame(self, text="System Unit",
                                          padx=10, pady=10)
        unit_system_frame.pack(fill=tk.X)

        unit_system_lbl = tk.Label(unit_system_frame, text="System Unit",
                                   fg='blue', font="Times 12 bold")

        unit_system_frame.config(labelwidget=unit_system_lbl)

        self.unitvar_system = tk.StringVar()
        self.unitvar_system.set("FPS")

        self.unit_system_cb = ttk.Combobox(unit_system_frame, values=("MKS", "FPS"),
                                           textvariable=self.unitvar_system,
                                           style="CB.TCombobox")
        self.unit_system_cb.grid(row=0, column=0)
        self.unit_system_cb.bind("<<ComboboxSelected>>", self.setUnitEvent)

        wall_length_frame = tk.LabelFrame(self, text="Length",
                                       padx=10, pady=10)
        wall_length_frame.pack(fill=tk.X)

        wall_length_lbl = tk.Label(wall_length_frame, text="Length",
                                   font="Times 12 bold", fg="blue")
        wall_length_frame.config(labelwidget=wall_length_lbl)

        self.length_sbox1 = tk.Spinbox(wall_length_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.length_sbox1.grid(row=0, column=0)

        self.length_unit1 = tk.Label(wall_length_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.length_unit1.grid(row=0, column=1)

        self.length_sbox2 = tk.Spinbox(wall_length_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.length_sbox2.grid(row=0, column=2)

        self.length_unit2 = tk.Label(wall_length_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.length_unit2.grid(row=0, column=3)

        wall_width_frame = tk.LabelFrame(self, text="Width",
                                       padx=10, pady=10)
        wall_width_frame.pack(fill=tk.X)

        wall_width_lbl = tk.Label(wall_width_frame, text="Width",
                                   font="Times 12 bold", fg="blue")
        wall_width_frame.config(labelwidget=wall_width_lbl)

        self.width_sbox1 = tk.Spinbox(wall_width_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.width_sbox1.grid(row=0, column=0)

        self.width_unit1 = tk.Label(wall_width_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.width_unit1.grid(row=0, column=1)

        self.width_sbox2 = tk.Spinbox(wall_width_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.width_sbox2.grid(row=0, column=2)

        self.width_unit2 = tk.Label(wall_width_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.width_unit2.grid(row=0, column=3)

        wall_height_frame = tk.LabelFrame(self, text="Thickness",
                                       padx=10, pady=10)
        wall_height_frame.pack(fill=tk.X)

        wall_height_lbl = tk.Label(wall_height_frame, text="Height",
                                   font="Times 12 bold", fg="blue")
        wall_height_frame.config(labelwidget=wall_height_lbl)

        self.height_sbox1 = tk.Spinbox(wall_height_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.height_sbox1.grid(row=0, column=0)

        self.height_unit1 = tk.Label(wall_height_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.height_unit1.grid(row=0, column=1)

        self.height_sbox2 = tk.Spinbox(wall_height_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.height_sbox2.grid(row=0, column=2)

        self.height_unit2 = tk.Label(wall_height_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.height_unit2.grid(row=0, column=3)

        waste_frame = tk.LabelFrame(self, text="Concrete Waste",
                                       padx=10, pady=10)
        waste_frame.pack(fill=tk.X)

        waste_lbl = tk.Label(waste_frame, text="Concrete Waste",
                                   font="Times 12 bold", fg="blue")
        waste_frame.config(labelwidget=waste_lbl)

        self.waste_sbox = tk.Spinbox(waste_frame, increment=0.50,
                                     from_=0.00, to=100.00, fg="blue",
                                     font="Times 12 normal",
                                     format="%.2f")

        self.waste_sbox.grid(row=0, column=0)

        waste_unit = tk.Label(waste_frame, text="%",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        waste_unit.grid(row=0, column=1)

        result_frame = tk.LabelFrame(self, text="Result",
                                     padx=10, pady=10)
        result_frame.pack(fill=tk.X)

        result_lbl = tk.Label(result_frame, text="Result",
                              font="Times 12 bold", fg="blue")
        result_frame.config(labelwidget=result_lbl)

        volume_lbl = tk.Label(result_frame, text="Volume: ", fg='blue',
                              font="Times 11 bold")
        volume_lbl.grid(row=0, column=0)
        
        self.volume_entry = tk.Entry(result_frame, font="Times 12 normal",
                                     fg='blue')
        self.volume_entry.grid(row=0, column=1)
        
        self.volume_lbl1 = tk.Label(result_frame, text="cyd", fg="blue",
                               font="Times 12 bold", width=6, anchor=tk.W)
        self.volume_lbl1.grid(row=0, column=2)

        self.calc_btn = tk.Button(self, text="Calculate", width=11,
                                  command=self.calculate, fg='white',
                                  font="Times 12 bold", bg='green')
        self.calc_btn.pack(padx=5, pady=5)

        # Hackfix for an error in Spinbox widget
        # displaying the max value instead of minimum value
        
        for child in self.winfo_children():
            for widget in child.winfo_children():
                if widget.winfo_class() == "Spinbox":
                    widget.delete('0', 'end')
                    widget.insert('end', "0")
                    widget.config(width=10)

        self.setUnit(self.unitvar_system.get())

    def setUnit(self, unit_system):
        if unit_system == "MKS":
            self.length_unit1.config(text="mtr")
            self.length_unit2.config(text="cm")
            self.width_unit1.config(text="mtr")
            self.width_unit2.config(text="cm")
            self.height_unit1.config(text="mtr")
            self.height_unit2.config(text="cm")
            self.volume_lbl1.config(text="cum")
        else:
            self.length_unit1.config(text="ft")
            self.length_unit2.config(text="in")
            self.width_unit1.config(text="ft")
            self.width_unit2.config(text="in")
            self.height_unit1.config(text="ft")
            self.height_unit2.config(text="in")
            self.volume_lbl1.config(text="cyd")

    def setUnitEvent(self, event):
        unit = self.unitvar_system.get()
        self.setUnit(unit)

    def calculate(self):
        unit = self.unitvar_system.get()
        if unit == "FPS":
            length_ft = float(self.length_sbox1.get())
            length_in = float(self.length_sbox2.get())/12

            width_ft = float(self.width_sbox1.get())
            width_in = float(self.width_sbox2.get())/12

            height_ft = float(self.height_sbox1.get())
            height_in = float(self.height_sbox2.get())/12

            waste = float(self.waste_sbox.get())

            volume = ((length_ft + length_in) * (width_ft + width_in) * (height_ft + height_in))/27
            volume = volume * (1 + waste/100)
        else:
            length_mtr = float(self.length_sbox1.get())
            length_cm = float(self.length_sbox2.get())/100

            width_mtr = float(self.width_sbox1.get())
            width_cm = float(self.width_sbox2.get())/100

            height_mtr = float(self.height_sbox1.get())
            height_cm = float(self.height_sbox2.get())/100

            waste = float(self.waste_sbox.get())

            volume = (length_mtr + length_cm) * (width_mtr + width_cm) * (height_mtr + height_cm)
            volume = volume * (1 + waste/100)

        self.volume_entry.delete('0', 'end')
        self.volume_entry.insert('end', format(volume, ".2f"))

    def close(self):
        self.master.destroy()


class SlabOnGradeWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.closeWindow)
        self.style = ttk.Style()
        self.setup_ui()

    def setup_ui(self):
        self.configure(padx=20, pady=20)

        length_lbl1 = ttk.Label(self, text="Slab Length", width=17)
        length_lbl1.grid(row=1, column=0)
        self.length_entry1 = tk.Spinbox(self, increment=1, from_=0,
                                        to=999999, font=("calibri", 12))
        self.length_entry1.grid(row=1, column=1)
        length_lbl2 = ttk.Label(self, text="ft", width=5)
        length_lbl2.grid(row=1, column=2)

        width_lbl1 = ttk.Label(self, text="Slab Width", width=17)
        width_lbl1.grid(row=2, column=0)
        self.width_entry1 = tk.Spinbox(self, increment=1, from_=0,
                                       to=999999, font=("calibri", 12))
        self.width_entry1.grid(row=2, column=1)
        width_lbl2 = ttk.Label(self, text="ft", width=5)
        width_lbl2.grid(row=2, column=2)

        thick_lbl1 = ttk.Label(self, text="Slab Thickness", width=17)
        thick_lbl1.grid(row=3, column=0)
        self.thick_entry1 = tk.Spinbox(self, increment=1, from_=0,
                                       to=999999, font=("calibri", 12))
        self.thick_entry1.grid(row=3, column=1)
        thick_lbl2 = ttk.Label(self, text="in", width=5)
        thick_lbl2.grid(row=3, column=2)

        waste_lbl1 = ttk.Label(self, text="Concrete Waste", width=17)
        waste_lbl1.grid(row=4, column=0)
        self.waste_entry = tk.Spinbox(self, increment=0.5, from_=0.0,
                                      to=100.0, font=("calibri", 12),
                                      format="%.1f")
        self.waste_entry.grid(row=4, column=1)
        waste_lbl2 = ttk.Label(self, text="%", width=5)
        waste_lbl2.grid(row=4, column=2)

        bar_space_lbl1 = ttk.Label(self, text="Bar Spacing", width=17)
        bar_space_lbl1.grid(row=5, column=0)
        self.bar_space_entry = tk.Spinbox(self, increment=1, from_=0,
                                          to=999999, font=("calibri", 12))
        self.bar_space_entry.grid(row=5, column=1)
        bar_space_lbl2 = ttk.Label(self, text="in", width=5)
        bar_space_lbl2.grid(row=5, column=2)

        bar_size_lbl1 = ttk.Label(self, text="Bar Size", width=17)
        bar_size_lbl1.grid(row=6, column=0)
        self.bar_size_entry = tk.Spinbox(self, increment=1, from_=0,
                                         to=999999, font=("calibri", 12))
        self.bar_size_entry.grid(row=6, column=1)
        bar_size_lbl2 = ttk.Label(self, text="#", width=5)
        bar_size_lbl2.grid(row=6, column=2)

        bar_length_lbl1 = ttk.Label(self, text="Bar Length", width=17)
        bar_length_lbl1.grid(row=7, column=0)
        self.bar_length_entry = tk.Spinbox(self, increment=1, from_=0,
                                           to=999999, font=("calibri", 12))
        self.bar_length_entry.grid(row=7, column=1)
        bar_length_lbl2 = ttk.Label(self, text="ft", width=5)
        bar_length_lbl2.grid(row=7, column=2)

        lap_lbl1 = ttk.Label(self, text="Lap", width=17)
        lap_lbl1.grid(row=8, column=0)
        self.lap_entry = tk.Spinbox(self, increment=1, from_=0,
                                    to=999999, font=("calibri", 12))
        self.lap_entry.grid(row=8, column=1)
        lap_lbl2 = ttk.Label(self, text="# of dia.", width=5)
        lap_lbl2.grid(row=8, column=2)

        area_lbl1 = ttk.Label(self, text="Area", width=17)
        area_lbl1.grid(row=9, column=0)
        self.area_entry = tk.Entry(self, font=("calibri", 12))
        self.area_entry.grid(row=9, column=1)
        area_lbl2 = ttk.Label(self, text="sft", width=5)
        area_lbl2.grid(row=9, column=2)

        volume_lbl1 = ttk.Label(self, text="Volume", width=17)
        volume_lbl1.grid(row=10, column=0)
        self.volume_entry = tk.Entry(self, font=("calibri", 12))
        self.volume_entry.grid(row=10, column=1)
        volume_lbl2 = ttk.Label(self, text="cyd", width=5)
        volume_lbl2.grid(row=10, column=2)

        no_bars_lbl1 = ttk.Label(self, text="No. of Bars", width=17)
        no_bars_lbl1.grid(row=11, column=0)
        self.no_bars_entry = tk.Entry(self, font=("calibri", 12))
        self.no_bars_entry.grid(row=11, column=1)
        no_bars_lbl2 = ttk.Label(self, text="each", width=5)
        no_bars_lbl2.grid(row=11, column=2)

        self.calc_btn = ttk.Button(self, text="Calculate")
        self.calc_btn.config(command=self.calculate)
        self.calc_btn.grid(row=12, column=0, columnspan=3, padx=10, pady=10)

        # Hackfix for an error in Spinbox widget
        # displaying the max value instead of minimum value
        for widget in self.winfo_children():
            if widget.winfo_class() == "Spinbox":
                widget.delete('0', tk.END)
                widget.insert(tk.END, "0")

    def calculate(self):
        length_ft = float(self.length_entry1.get())
        width_ft = float(self.width_entry1.get())
        thick_ft = float(self.thick_entry1.get())/12
        waste = float(self.waste_entry.get())
        bar_space = float(self.bar_space_entry.get())
        bar_size = float(self.bar_size_entry.get())
        bar_length_ft = float(self.bar_length_entry.get())
        lap = (float(self.lap_entry.get()) * (bar_size/8))/12

        area = length_ft * width_ft

        volume = (length_ft * width_ft * thick_ft)/27
        volume = volume * (1 + waste/100)

        no_length = math.ceil((length_ft/lap) + 1)
        no_width = math.ceil((width_ft/lap) + 1)

        total_length = (no_length * (width_ft)) + (no_width * (length_ft))

        no_bars = math.ceil(total_length/(bar_length_ft-lap))

        self.area_entry.delete('0', 'end')
        self.area_entry.insert('end', format(area, ",.0f"))

        self.no_bars_entry.delete('0', 'end')
        self.no_bars_entry.insert('end', format(no_bars, ","))

        self.volume_entry.delete('0', 'end')
        self.volume_entry.insert('end', format(volume, ".2f"))

    def closeWindow(self):
        self.master.destroy()


class ReSlabOnGradeWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.closeWindow)
        self.style = ttk.Style()
        self.setup_ui()

    def setup_ui(self):
        self.configure(padding=20)

        length_lbl1 = ttk.Label(self, text="Slab Length", width=17)
        length_lbl1.grid(row=1, column=0)
        self.length_entry1 = tk.Spinbox(self, increment=1, from_=0,
                                        to=999999, font=("calibri", 12))
        self.length_entry1.grid(row=1, column=1)
        length_lbl2 = ttk.Label(self, text="ft", width=5)
        length_lbl2.grid(row=1, column=2)

        width_lbl1 = ttk.Label(self, text="Slab Width", width=17)
        width_lbl1.grid(row=2, column=0)
        self.width_entry1 = tk.Spinbox(self, increment=1, from_=0,
                                       to=999999, font=("calibri", 12))
        self.width_entry1.grid(row=2, column=1)
        width_lbl2 = ttk.Label(self, text="ft", width=5)
        width_lbl2.grid(row=2, column=2)

        thick_lbl1 = ttk.Label(self, text="Slab Thickness", width=17)
        thick_lbl1.grid(row=3, column=0)
        self.thick_entry1 = tk.Spinbox(self, increment=1, from_=0,
                                       to=999999, font=("calibri", 12))
        self.thick_entry1.grid(row=3, column=1)
        thick_lbl2 = ttk.Label(self, text="in", width=5)
        thick_lbl2.grid(row=3, column=2)

        waste_lbl1 = ttk.Label(self, text="Concrete Waste", width=17)
        waste_lbl1.grid(row=4, column=0)
        self.waste_entry = tk.Spinbox(self, increment=0.5,
                                      from_=0.0, to=100.0,
                                      font=("calibri", 12), format="%.1f")
        self.waste_entry.grid(row=4, column=1)
        waste_lbl2 = ttk.Label(self, text="%", width=5)
        waste_lbl2.grid(row=4, column=2)

        mesh_length_lbl1 = ttk.Label(self, text="Wire Mesh Length", width=17)
        mesh_length_lbl1.grid(row=5, column=0)
        self.mesh_length_entry = tk.Spinbox(self, increment=1, from_=0,
                                            to=999999, font=("calibri", 12))
        self.mesh_length_entry.grid(row=5, column=1)
        mesh_length_lbl2 = ttk.Label(self, text="ft", width=5)
        mesh_length_lbl2.grid(row=5, column=2)

        mesh_width_lbl1 = ttk.Label(self, text="Wire Mesh Width", width=17)
        mesh_width_lbl1.grid(row=6, column=0)
        self.mesh_width_entry = tk.Spinbox(self, increment=1, from_=0,
                                           to=999999, font=("calibri", 12))
        self.mesh_width_entry.grid(row=6, column=1)
        mesh_width_lbl2 = ttk.Label(self, text="ft", width=5)
        mesh_width_lbl2.grid(row=6, column=2)

        mesh_lap_lbl1 = ttk.Label(self, text="Wire Mesh Lap", width=17)
        mesh_lap_lbl1.grid(row=7, column=0)
        self.mesh_lap_entry = tk.Spinbox(self, increment=1, from_=0,
                                         to=999999, font=("calibri", 12))
        self.mesh_lap_entry.grid(row=7, column=1)
        mesh_lap_lbl2 = ttk.Label(self, text="in", width=5)
        mesh_lap_lbl2.grid(row=7, column=2)

        area_lbl1 = ttk.Label(self, text="Area", width=17)
        area_lbl1.grid(row=8, column=0)
        self.area_entry = tk.Entry(self, font=("calibri", 12))
        self.area_entry.grid(row=8, column=1)
        area_lbl2 = ttk.Label(self, text="sft", width=5)
        area_lbl2.grid(row=8, column=2)

        volume_lbl1 = ttk.Label(self, text="Volume", width=17)
        volume_lbl1.grid(row=9, column=0)
        self.volume_entry = tk.Entry(self, font=("calibri", 12))
        self.volume_entry.grid(row=9, column=1)
        volume_lbl2 = ttk.Label(self, text="cyd", width=5)
        volume_lbl2.grid(row=9, column=2)

        mesh_qty_lbl1 = ttk.Label(self, text="Wire Mesh", width=17)
        mesh_qty_lbl1.grid(row=10, column=0)
        self.mesh_qty_entry = tk.Entry(self, font=("calibri", 12))
        self.mesh_qty_entry.grid(row=10, column=1)
        mesh_qty_lbl2 = ttk.Label(self, text="each", width=5)
        mesh_qty_lbl2.grid(row=10, column=2)

        self.calc_btn = ttk.Button(self, text="Calculate")
        self.calc_btn.config(command=self.calculate)
        self.calc_btn.grid(row=11, column=0, columnspan=3, padx=10, pady=10)

        # Hackfix for an error in Spinbox widget
        # displaying the max value instead of minimum value
        for widget in self.winfo_children():
            if widget.winfo_class() == "Spinbox":
                widget.delete('0', 'end')
                widget.insert('end', "0")

    def calculate(self):
        length_ft = float(self.length_entry1.get())
        width_ft = float(self.width_entry1.get())
        thick_ft = float(self.thick_entry1.get())/12
        waste = float(self.waste_entry.get())
        mesh_length = float(self.mesh_length_entry.get())
        mesh_width = float(self.mesh_width_entry.get())
        mesh_lap = float(self.mesh_lap_entry.get())/12

        area = length_ft * width_ft

        volume = (length_ft * width_ft * thick_ft)/27
        volume = volume * (1 + waste/100)

        rows = math.ceil(width_ft/(mesh_width-mesh_lap))
        print(rows)
        columns = round(length_ft/(mesh_length-mesh_lap), 2)
        print(columns)

        total_mesh_roll = math.ceil(rows * columns)
        print(total_mesh_roll)

        self.area_entry.delete('0', 'end')
        self.area_entry.insert('end', format(area, ",.0f"))

        self.mesh_qty_entry.delete('0', 'end')
        self.mesh_qty_entry.insert('end', format(total_mesh_roll, ",.0f"))

        self.volume_entry.delete('0', 'end')
        self.volume_entry.insert('end', format(volume, ".2f"))

    def closeWindow(self):
        self.master.destroy()


class SlabOnMetalWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.closeWindow)
        self.style = ttk.Style()
        self.setup_ui()

    def setup_ui(self):
        self.configure(padding=20)

        length_lbl1 = ttk.Label(self, text="Slab Length", width=17)
        length_lbl1.grid(row=1, column=0)
        self.length_entry1 = tk.Spinbox(self, increment=1, from_=0,
                                        to=999999, font=("calibri", 12))
        self.length_entry1.grid(row=1, column=1)
        length_lbl2 = ttk.Label(self, text="ft", width=5)
        length_lbl2.grid(row=1, column=2)

        width_lbl1 = ttk.Label(self, text="Slab Width", width=17)
        width_lbl1.grid(row=2, column=0)
        self.width_entry1 = tk.Spinbox(self, increment=1, from_=0,
                                       to=999999, font=("calibri", 12))
        self.width_entry1.grid(row=2, column=1)
        width_lbl2 = ttk.Label(self, text="ft", width=5)
        width_lbl2.grid(row=2, column=2)

        T_lbl1 = ttk.Label(self, text="T", width=17)
        T_lbl1.grid(row=3, column=0)
        self.T_entry = tk.Spinbox(self, increment=1, from_=0,
                                  to=999999, font=("calibri", 12))
        self.T_entry.grid(row=3, column=1)
        T_lbl2 = ttk.Label(self, text="in", width=5)
        T_lbl2.grid(row=3, column=2)

        D_lbl1 = ttk.Label(self, text="D", width=17)
        D_lbl1.grid(row=4, column=0)
        self.D_entry = tk.Spinbox(self, increment=1, from_=0,
                                  to=999999, font=("calibri", 12))
        self.D_entry.grid(row=4, column=1)
        D_lbl2 = ttk.Label(self, text="in", width=5)
        D_lbl2.grid(row=4, column=2)

        W1_lbl1 = ttk.Label(self, text="W1", width=17)
        W1_lbl1.grid(row=5, column=0)
        self.W1_entry = tk.Spinbox(self, increment=1, from_=0,
                                   to=999999, font=("calibri", 12))
        self.W1_entry.grid(row=5, column=1)
        W1_lbl2 = ttk.Label(self, text="in", width=5)
        W1_lbl2.grid(row=5, column=2)

        W2_lbl1 = ttk.Label(self, text="W2", width=17)
        W2_lbl1.grid(row=6, column=0)
        self.W2_entry = tk.Spinbox(self, increment=1, from_=0,
                                   to=999999, font=("calibri", 12))
        self.W2_entry.grid(row=6, column=1)
        W2_lbl2 = ttk.Label(self, text="in", width=5)
        W2_lbl2.grid(row=6, column=2)

        S_lbl1 = ttk.Label(self, text="S", width=17)
        S_lbl1.grid(row=7, column=0)
        self.S_entry = tk.Spinbox(self, increment=1, from_=0,
                                  to=999999, font=("calibri", 12))
        self.S_entry.grid(row=7, column=1)
        S_lbl2 = ttk.Label(self, text="in", width=5)
        S_lbl2.grid(row=7, column=2)

        waste_lbl1 = ttk.Label(self, text="Concrete Waste", width=17)
        waste_lbl1.grid(row=8, column=0)
        self.waste_entry = tk.Spinbox(self, increment=0.5, from_=0.0,
                                      to=100.0, font=("calibri", 12),
                                      format="%.1f")
        self.waste_entry.grid(row=8, column=1)
        waste_lbl2 = ttk.Label(self, text="%", width=5)
        waste_lbl2.grid(row=8, column=2)

        volume_lbl1 = ttk.Label(self, text="Volume", width=17)
        volume_lbl1.grid(row=9, column=0)
        self.volume_entry = tk.Entry(self, font=("calibri", 12))
        self.volume_entry.grid(row=9, column=1)
        volume_lbl2 = ttk.Label(self, text="cyd", width=5)
        volume_lbl2.grid(row=9, column=2)

        self.calc_btn = ttk.Button(self, text="Calculate")
        self.calc_btn.config(command=self.calculate)
        self.calc_btn.grid(row=11, column=0, columnspan=3, padx=10, pady=10)

        # Hackfix for an error in Spinbox widget
        # displaying the max value instead of minimum value
        for widget in self.winfo_children():
            if widget.winfo_class() == "Spinbox":
                widget.delete('0', 'end')
                widget.insert('end', "0")

    def calculate(self):
        length_ft = float(self.length_entry1.get())
        width_ft = float(self.width_entry1.get())
        T_ft = float(self.T_entry.get())/12
        D_ft = float(self.D_entry.get())/12
        W1_ft = float(self.W1_entry.get())/12
        W2_ft = float(self.W2_entry.get())/12
        S_ft = float(self.S_entry.get())/12
        waste = float(self.waste_entry.get())

        thickness = T_ft + (((W1_ft + W2_ft) * (D_ft))/(2 * S_ft))

        volume = (length_ft * width_ft * thickness)/27
        volume = volume * (1 + waste/100)

        self.volume_entry.delete('0', 'end')
        self.volume_entry.insert('end', format(volume, ".2f"))

    def closeWindow(self):
        self.master.destroy()


class StairsWindow(tk.Frame):

    def __init__(self, master=None, **kws):
        tk.Frame.__init__(self, master, **kws)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        #self.master.geometry("480x320+20+20")
        self.master.title("Stairs")
        self.master.resizable(False, False)
        if os.name == 'nt':
            self.master.iconbitmap("berderose.ico")
        self.setup_ui()
        self.grab_set()
        self.focus_set()

    def setup_ui(self):
        self.config(padx=10, pady=10)

        style = ttk.Style()
        style.configure("CB.TCombobox", foreground="blue")

        unit_system_frame = tk.LabelFrame(self, text="System Unit",
                                          padx=10, pady=10)
        unit_system_frame.pack(fill=tk.X)

        unit_system_lbl = tk.Label(unit_system_frame, text="System Unit",
                                   fg='blue', font="Times 12 bold")

        unit_system_frame.config(labelwidget=unit_system_lbl)

        self.unitvar_system = tk.StringVar()
        self.unitvar_system.set("FPS")

        self.unit_system_cb = ttk.Combobox(unit_system_frame, values=("MKS", "FPS"),
                                           textvariable=self.unitvar_system,
                                           style="CB.TCombobox")
        self.unit_system_cb.grid(row=0, column=0)
        self.unit_system_cb.bind("<<ComboboxSelected>>", self.setUnitEvent)

        parallel_frame = tk.LabelFrame(self, text="Parallelogram",
                                       padx=10, pady=10)
        parallel_frame.pack(fill=tk.X)

        parallel_lbl = tk.Label(parallel_frame, text="Parallelogram",
                                   font="Times 12 bold", fg="blue")
        parallel_frame.config(labelwidget=parallel_lbl)

        p_length_lbl = tk.Label(parallel_frame, text="Length:",
                                font="Times 11 bold", fg="blue")
        p_length_lbl.grid(row=0, column=0)

        self.p_length_sbox = tk.Spinbox(parallel_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")
        self.p_length_sbox.grid(row=0, column=1)

        self.p_length_unit = tk.Label(parallel_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.p_length_unit.grid(row=0, column=2)

        p_width_lbl = tk.Label(parallel_frame, text="Width:",
                                font="Times 11 bold", fg="blue")
        p_width_lbl.grid(row=1, column=0)

        self.p_width_sbox = tk.Spinbox(parallel_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")
        self.p_width_sbox.grid(row=1, column=1)

        self.p_width_unit = tk.Label(parallel_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.p_width_unit.grid(row=1, column=2)

        triangle_frame = tk.LabelFrame(self, text="Triangle",
                                       padx=10, pady=10)
        triangle_frame.pack(fill=tk.X)

        triangle_lbl = tk.Label(triangle_frame, text="Triangle",
                                   font="Times 12 bold", fg="blue")
        triangle_frame.config(labelwidget=triangle_lbl)

        t_base_lbl = tk.Label(triangle_frame, text="Base:",
                                font="Times 11 bold", fg="blue")
        t_base_lbl.grid(row=0, column=0)

        self.t_base_sbox = tk.Spinbox(triangle_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.t_base_sbox.grid(row=0, column=1)

        self.t_base_unit = tk.Label(triangle_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.t_base_unit.grid(row=0, column=2)

        t_height_lbl = tk.Label(triangle_frame, text="Height:",
                                font="Times 11 bold", fg="blue")
        t_height_lbl.grid(row=1, column=0)

        self.t_height_sbox = tk.Spinbox(triangle_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")
        self.t_height_sbox.grid(row=1, column=1)

        self.t_height_unit = tk.Label(triangle_frame, text="in",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.t_height_unit.grid(row=1, column=2)

        stairs_frame = tk.LabelFrame(self, text="Stairs",
                                       padx=10, pady=10)
        stairs_frame.pack(fill=tk.X)

        stairs_lbl = tk.Label(stairs_frame, text="Stairs",
                                   font="Times 12 bold", fg="blue")
        stairs_frame.config(labelwidget=stairs_lbl)

        num_steps_lbl = tk.Label(stairs_frame, text="No. of Steps:",
                                 font="Times 11 bold", fg="blue")
        num_steps_lbl.grid(row=0, column=0)

        self.num_steps_sbox = tk.Spinbox(stairs_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")
        self.num_steps_sbox.grid(row=0, column=1)

        self.num_steps_unit = tk.Label(stairs_frame, text="ea",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.num_steps_unit.grid(row=0, column=2)

        steps_width_lbl = tk.Label(stairs_frame, text="Stairs Width:",
                                 font="Times 11 bold", fg="blue")
        steps_width_lbl.grid(row=1, column=0)

        self.steps_width_sbox = tk.Spinbox(stairs_frame, increment=1,
                                       from_=0, to=999999, fg="blue",
                                       font="Times 12 normal")

        self.steps_width_sbox.grid(row=1, column=1)

        self.steps_width_unit = tk.Label(stairs_frame, text="ft",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        self.steps_width_unit.grid(row=1, column=2)

        waste_frame = tk.LabelFrame(self, text="Concrete Waste",
                                       padx=10, pady=10)
        waste_frame.pack(fill=tk.X)

        waste_lbl = tk.Label(waste_frame, text="Concrete Waste",
                                   font="Times 12 bold", fg="blue")
        waste_frame.config(labelwidget=waste_lbl)

        self.waste_sbox = tk.Spinbox(waste_frame, increment=0.50,
                                     from_=0.00, to=100.00, fg="blue",
                                     font="Times 12 normal",
                                     format="%.2f")

        self.waste_sbox.grid(row=0, column=0)

        waste_unit = tk.Label(waste_frame, text="%",
                                font="Times 11 bold", fg="blue",
                                width=6, anchor=tk.W)
        waste_unit.grid(row=0, column=1)

        result_frame = tk.LabelFrame(self, text="Result",
                                     padx=10, pady=10)
        result_frame.pack(fill=tk.X)

        result_lbl = tk.Label(result_frame, text="Result",
                              font="Times 12 bold", fg="blue")
        result_frame.config(labelwidget=result_lbl)

        area_lbl = tk.Label(result_frame, text="Area: ", fg='blue',
                             font="Times 11 bold")
        area_lbl.grid(row=0, column=0)

        self.area_entry = tk.Entry(result_frame, font="Times 12 normal",
                                    fg='blue')
        self.area_entry.grid(row=0, column=1)

        self.unitvar = tk.StringVar()

        area_unit = tk.Label(result_frame, textvariable=self.unitvar,
                              width=6, anchor=tk.W, fg='blue',
                              font="Times 12 bold")
        area_unit.grid(row=0, column=2)

        volume_lbl = tk.Label(result_frame, text="Volume: ", fg='blue',
                              font="Times 11 bold")
        volume_lbl.grid(row=1, column=0)
        self.volume_entry = tk.Entry(result_frame, font="Times 12 normal",
                                     fg='blue')
        self.volume_entry.grid(row=1, column=1)
        self.volume_lbl1 = tk.Label(result_frame, text="cyd", fg="blue",
                               font="Times 12 bold", width=6, anchor=tk.W)
        self.volume_lbl1.grid(row=1, column=2)

        self.calc_btn = tk.Button(self, text="Calculate", width=11,
                                  command=self.calculate, fg='white',
                                  font="Times 12 bold", bg='green')
        self.calc_btn.pack(padx=5, pady=5)

        # Hackfix for an error in Spinbox widget
        # displaying the max value instead of minimum value

        for child in self.winfo_children():
            for widget in child.winfo_children():
                if widget.winfo_class() == "Spinbox":
                    widget.delete('0', 'end')
                    widget.insert('end', "0")
                    widget.config(width=10)

        self.setUnit(self.unitvar_system.get())

    def setUnit(self, unit_system):
        if unit_system == "MKS":
            self.unitvar.set("sqm")
            self.p_length_unit.config(text="mtr")
            self.p_width_unit.config(text="cm")
            self.t_base_unit.config(text="cm")
            self.t_height_unit.config(text="cm")
            self.steps_width_unit.config(text="mtr")
            self.volume_lbl1.config(text="cum")
        else:
            self.unitvar.set("sft")
            self.p_length_unit.config(text="ft")
            self.p_width_unit.config(text="in")
            self.t_base_unit.config(text="in")
            self.t_height_unit.config(text="in")
            self.steps_width_unit.config(text="ft")
            self.volume_lbl1.config(text="cyd")

    def setUnitEvent(self, event):
        unit = self.unitvar_system.get()
        self.setUnit(unit)

    def calculate(self):
        unit = self.unitvar_system.get()
        if unit == "FPS":
            p_length = float(self.p_length_sbox.get())
            p_width = float(self.p_width_sbox.get())/12

            base = float(self.t_base_sbox.get())/12
            height = float(self.t_height_sbox.get())/12

            steps = float(self.num_steps_sbox.get())
            s_width = float(self.steps_width_sbox.get())
            waste = float(self.waste_sbox.get())

            p_area = p_length * p_width
            t_area = steps * ((base * height)/2)

            total_area = p_area + t_area

            volume = (total_area * s_width)/27
            volume = volume * (1 + waste/100)
        else:
            p_length = float(self.p_length_sbox.get())
            p_width = float(self.p_width_sbox.get())/100

            base = float(self.t_base_sbox.get())/100
            height = float(self.t_height_sbox.get())/100

            steps = float(self.num_steps_sbox.get())
            s_width = float(self.steps_width_sbox.get())
            waste = float(self.waste_sbox.get())

            p_area = p_length * p_width
            t_area = steps * ((base * height)/2)

            total_area = p_area + t_area

            volume = (total_area * s_width)
            volume = volume * (1 + waste/100)

        self.volume_entry.delete('0', 'end')
        self.volume_entry.insert('end', format(volume, ".2f"))

        self.area_entry.delete('0', 'end')
        self.area_entry.insert('end', format(total_area, ".2f"))

    def close(self):
        self.master.destroy()


class PDFReport(FPDF):

    def __init__(self, *args, **kwargs):
        FPDF.__init__(self, *args)
        self.report_title = None
        if 'title' in kwargs:
            self.report_title = kwargs['title']

    def header(self):
        # Logo
        self.image('images/berderose.png', 10, 8, 10)
        # Set font (Arial bold 15)
        self.set_font('Arial', 'B', 15)
        # Move to the right
        self.cell(80)
        # Title
        if self.report_title is not None:
            self.cell(20, 7, self.report_title, 0, 0, 'C')
        else:
            self.cell(20, 10, "Title Missing", 0, 0, 'C')
        # Line Break
        self.ln(10)

    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Set font (Arial italic 8)
        self.set_font('Arial', 'I', 8)
        # Insert page number.
        self.cell(0, 10, 'Page ' + str(self.page_no()) + ' of {nb}', 0, 0, 'C')


class CreateToolTip(object):

    def __init__(self, widget, message):
        self.message = message
        widget.bind("<Enter>", self.tooltip_show)
        widget.bind("<Leave>", self.tooltip_close)

    def tooltip_show(self, event):
        """ This function is used to display the tool tip. """
        x_coor = event.widget.winfo_rootx()+20
        y_coor = event.widget.winfo_rooty()+30

        time.sleep(0.2)
        self.tool_tip_win = tk.Toplevel(event.widget)
        self.tool_tip_win.overrideredirect(True)
        loc = "+" + str(x_coor) + "+" + str(y_coor)
        self.tool_tip_win.geometry(loc)
        msg = tk.Label(self.tool_tip_win, text=self.message,
                       fg='white', bg='#2596be')
        msg.pack(fill=tk.X, ipadx=5, ipady=5)
        msg.config(font="Helvetica 8 bold")

    def tooltip_close(self, event):
        """ This enables the tooltip to destroy itself when leave the widget """
        self.tool_tip_win.destroy()


class BidForm(tk.Frame):

    def __init__(self, master=None, **kwargs):
        """ Initialize the form for bidding"""
        tk.Frame.__init__(self, master, **kwargs)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.title("BidForm")
        self.setup_ui()

    def setup_ui(self):
        """ This will setup all other necessary widgets. """
        pass

    def close(self):
        """ This function is used to close the form. """
        self.master.destroy()

def main():
    """ This is the main function of the application. """
    if not os.path.isdir('config'):
        os.mkdir('config')
    app = tk.Tk()
    win = MainWindow(app)
    win.pack(expand=True, fill=tk.BOTH)
    app.mainloop()


if __name__ == "__main__":
    sys.exit(main())
