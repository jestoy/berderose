# Berderose Estimation Software
    version 1.0.0
---
Berderose Estimation Software is a simple estimation software which uses opensource software and third party modules. It was created as a hobby project.

## Installation

Installation is quite easy as you just need to extract the archive using your favorite archiver and place it any folder you have access to. Search for the *berderose.exe* file and start
from there.

## Usage

For basic usage and other information please click the help button on the app and an html file will be launch where teh user guide can be found. For any other concerns please feel
free to drop me a message at this [email](jestoy.olazo@gmail.com).

## Contributions

This was made by me and no other programmers work on this, but if you are interested please drop me a message anytime. I am open with just about anything.

## Credits

Free icons at [icons8.com](https://icons8.com)

## License

[MIT](https://choosealicense.com/licenses/mit/)
